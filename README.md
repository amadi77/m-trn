## Taraan trn component for moqui to overide overall styles and confs

Help:

persian-date(just for view based on timezone):
    js: new persianDate().format();
        convert gerogorian to persian:
            new persianDate(new Date(2011, 9, 16)).format();
        convert persian to gerogorian:
            new persianDate(new Date() | [1397,1,1]).toCalendar('gregorian').format();

    screen(client-side):
        convert gerogorian to persian:
            add this line into current screen or simply add the code too webroot screen:
                <render-mode><text type="html,vuet" location="component://trn/template/cssBasedDateUtils.html.ftl" template="true" encode="false" no-boundary-comment="true"/></render-mode>
            and also you need to add "date-time-view" or "date-view" class to your date holder element
    form:
        convert gerogorian to persian:
            add "date-time-view-form" or "date-view-form" customClass in form builder for client-side converting
        convert persian to gerogorian:
            ?

persian-date-picker:
    form:
        add "persian-datepicker" customClass in form builder
         TODO: add this feature to form engine as a component... not moq

    core-pages:
        just use the default tag for date picker in moqui...it'll be fine ;)

json-form-single:
    you can load forms which are created in taraan form builder like below:
        <json-form-single 
           name="testForm" 
           transition="testTransition" 
           require-authentication="anonymous-all" **optional: use this  when you need load form without auth
           json-src="'component://tasklist/screen/test.json'"
           redirect-url="."
           rest="http://url" ** ether this or transition attr you cant use the both... use'll this one if you need to send submission to custom rest url
           default-value="yourMapValue" **
           type="pv" ** For when we want to send the camunda infrastructure , just for send process variable to camunda
           token-name="basicToken" ** When we need to connect to the camunda with basicToken
           token-bearer="bearerToken"   ** not implemented completly ye
           token-value="Basic asfsah232lk=" ** When we need to connect to the camunda with a predefined user  
           />
        * if you want to send the form response to your custom rest url you can replace the "transition" attribute with "rest" attribute

***** IMPORTANT *****
but there are somethings that can't be override with this component and should change in the moqui-base-component

 1- Add Local fa_IR: as java standard Locale doesn't support fa_IR we need to open this file
    .../base-component/tools/screen/System/Security/UserAccount/UserAccountDetail.xml

    And add this line
    <script>localeStringList.add([locale:"fa_IR", name:"Persian - Iran"])</script>

    After this line in the file
    <iterate list="Locale.getAvailableLocales()" entry="lcl"><script>localeStringList.add([locale:lcl.toString(), name:lcl.getDisplayName(ec.user.locale)])</script></iterate>

 2- Config db: add below code to this file ../runtime/conf/MoquiDevConf.xml
        <datasource group-name="transactional" database-conf-name="postgres" schema-name="public" startup-add-missing="true" runtime-add-missing="false">
            <inline-jdbc><xa-properties user="postgres" password="123$%^789" serverName="192.168.1.22" portNumber="5432"
                                        databaseName="trn-moq-amir-dev"/></inline-jdbc>
        </datasource>
 3- there is something that you need change in moqui-base-component:
    Find this file
    ../base-component/webroot/screen/webroot/Login.xml

    And replace below script in the login transition action:
         if(ec.user.loginUser(username, password))
             ec.user.setPreference("basicToken",Base64.getEncoder().encodeToString((username + ":" + password).getBytes()))

