def mp = [:]
if(complainant.complainantRoleTypeEnumId == "cntEmployee"){
//complainant
    mp.dateNow = eblaghDate
    mp.caseFile = caseFile
    mp.complainantName = profile.person.firstName + " " + profile.person.lastName
    mp.nationalId = profile.person.ptidNationalCd
    if(complainant.complainantRelationEnumId != "cntOriginalPlaintiff") {
        mp.deadNationalId = complainant.complainantDead.deadNationalId
        mp.deadName = complainant.complainantDead.deadName + " " + complainant.complainantDead.deadLastName
    }else{
        mp.deadNationalId = "-"
        mp.deadName = "-"
    }
    mp.complainantfatherName = profile.person.fatherName
    mp.birthDate = profile.person.t_birthDate
    mp.jobTitle = complainant.jobTitle
    mp.workshopExperience = complainant.workshopExperience
    mp.complainantPhoneNumber = profile.person.personContactMech.mobileNumber
    mp.complainantProvince= profile.person.personContactMech.t_personProvince
    mp.complainantCity = profile.person.personContactMech.t_personCity
    mp.complainantAddress = profile.person.personContactMech.personAddress
    mp.complainantPostalCode = profile.person.personContactMech.personPostalCode
//representative
    mp.representiveName = (representativeAllData.profile.person) ? representativeAllData.profile.person.firstName + " " + representativeAllData.profile.person.lastName : '-'
    mp.representativeFatherName = (representativeAllData.profile.person) ? representativeAllData.profile.person.fatherName : '-'
    mp.representativeBirthDate =  (representativeAllData.profile.person) ? representativeAllData.profile.person.t_birthDate : '-'//mp.representativeNationalId = (representativeAllData.profile.person) ? representativeAllData.profile.person.ptidNationalCd : '-'//mp.representativeProvince = (representativeAllData.profile.person) ? representativeAllData.profile.person.personContactMech.t_personProvince : '-'
    mp.representativeCity = (representativeAllData.profile.person) ? representativeAllData.profile.person.personContactMech.t_personCity : '-'
    mp.representativeAddress = (representativeAllData.profile.person) ? representativeAllData.profile.person.personContactMech.personAddress :'-'
    mp.representativePostalCode = (representativeAllData.profile.person) ? representativeAllData.profile.person.personContactMech.personPostalCode : '-'
    mp.representativePhoneNumber = (representativeAllData.profile.person) ? representativeAllData.profile.person.personContactMech.mobileNumber : '-'
//complainant
    mp.complaintTitle = complaint.complaintTitle
    mp.complaintDesc = complaint.complaintDesc
    mp.theDate = meetingTime.t_theDate
    mp.mclsBranchAddr= mclsBranchAddr
//defendant
    def mpList = []
    def mpDefendant = [:]
    if(execution.hasVariable("defendantInfos")){
        for (int i=0; i < defendantInfos.size();i++){
            mpDefendant = [:]
            mpDefendant.defendantName = defendantInfos[i].ceoFirstName + " " + defendantInfos[i].ceoLastName
            mpDefendant.companyName = defendantInfos[i].companyName
            mpDefendant.defendantPhoneNumber = defendantInfos[i].workshopPhone
            mpDefendant.defendantProvince = (defendantInfos[i].t_defendantProvinceGeoId) ? defendantInfos[i].t_defendantProvinceGeoId : '?????'
            mpDefendant.defendantCity = (defendantInfos[i].t_defendantCityGeoId) ? defendantInfos[i].t_defendantCityGeoId : ''
            mpDefendant.defendantAddress = (defendantInfos[i].defendantAddress)  ? defendantInfos[i].defendantAddress : ''
            mpDefendant.defendantPostalCode = (defendantInfos[i].postalCode) ? defendantInfos[i].postalCode : '?????'
            mpList.add(mpDefendant)
        }

    }
    mp.mpList = mpList
}else{
//complainant
    mp.dateNow = eblaghDate
    mp.caseFile = caseFile
    mp.complainantName = profile.person.firstName + " " + profile.person.lastName
    mp.nationalId = profile.person.ptidNationalCd
    if(complainant.complainantRelationEnumId != "cntOriginalPlaintiff") {
        mp.deadNationalId = complainant.complainantDead.deadNationalId
        mp.deadName = complainant.complainantDead.deadName + " " + complainant.complainantDead.deadLastName
    }else{
        mp.deadNationalId = "-"
        mp.deadName = "-"
    }
    mp.complainantfatherName = profile.person.fatherName
    mp.birthDate = profile.person.t_birthDate
    mp.jobTitle= complainant.jobTitle
    mp.workshopExperience = complainant.workshopExperience
    mp.complainantPhoneNumber = profile.person.personContactMech.mobileNumber
    mp.complainantProvince= profile.person.personContactMech.t_personProvince
    mp.complainantCity = profile.person.personContactMech.t_personCity
    mp.complainantAddress = profile.person.personContactMech.personAddress
    mp.complainantPostalCode = profile.person.personContactMech.personPostalCode
//representative
    mp.representiveName = (representativeAllData.profile.person) ? representativeAllData.profile.person.firstName + " " + representativeAllData.profile.person.lastName : '-'
    mp.representativeFatherName = (representativeAllData.profile.person) ? representativeAllData.profile.person.fatherName : '-'
    mp.representativeBirthDate =  (representativeAllData.profile.person) ? representativeAllData.profile.person.t_birthDate : '-'
    mp.representativeNationalId = (representativeAllData.profile.person) ? representativeAllData.profile.person.ptidNationalCd : '-'
    mp.representativeProvince = (representativeAllData.profile.person) ? representativeAllData.profile.person.personContactMech.t_personProvince : '-'
    mp.representativeCity = (representativeAllData.profile.person) ? representativeAllData.profile.person.personContactMech.t_personCity : '-'
    mp.representativeAddress = (representativeAllData.profile.person) ? representativeAllDa0ta.profile.person.personContactMech.personAddress :'-'
    mp.representativePostalCode = (representativeAllData.profile.person) ? representativeAllData.profile.person.personContactMech.personPostalCode : '-'
    mp.representativePhoneNumber = (representativeAllData.profile.person) ? representativeAllData.profile.person.personContactMech.mobileNumber : '-'
//complainant
    mp.complaintTitle = complaint.complaintTitle
    mp.complaintDesc = complaint.complaintDesc
    mp.theDate = meetingTime.t_theDate
    mp.mclsBranchAddr= mclsBranchAddr
//defendant
    def mpList = []
    def mpDefendant = [:]
    if(execution.hasVariable("defendantInfos")){
        for (int i=0; i < defendantInfos.size();i++){
            mpDefendant = [:]
            mp.workerName = defendantInfos[i].workerName
            mp.workerLastName = defendantInfos[i].workerLastName
            mp.workerNationalId = defendantInfos[i].workerNationalId
            mp.workerPhone = defendantInfos[i].workerPhone
            mp.workerMobileNumber = defendantInfos[i].workerMobileNumber
            mp.defendantProvince = (defendantInfos[i]) ? defendantInfos[i].t_defendantProvinceGeoId: '?????'
            mp.defendantCity = (defendantInfos[i]) ? defendantInfos[i].t_defendantCityGeoId:''
            mp.defendantAddress = (defendantInfos[i]) ? defendantInfos[i].defendantAddress : ''
            mp.defendantPostalCode = (defendantInfos[i]) ? defendantInfos[i].postalCode : ''
            mpList.add(mpDefendant)
        }

    }

    mp.mpList  = mpList
}
mp