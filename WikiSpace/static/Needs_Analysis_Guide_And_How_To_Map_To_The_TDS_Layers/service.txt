 <service verb="send" noun="notification">
        <in-parameters>
            <parameter name="receivers" required="true" type="Boolean" />
            <parameter name="body" required="true" type="Map" />
            <parameter name="subject" required="true" type="Boolean" />
            <parameter name="identificationCode" type="Boolean"  />
            <parameter name="referenceNumber" type="intiger"  />
        </in-parameters>
        <out-parameters>
            <parameter name="res" />
        </out-parameters>
        <actions>
          ...
        </actions>
    </service>
