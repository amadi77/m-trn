 <............................................................................................ به نام خدا ...............................................................................................> 
*سرویس تامکت سرور مورد نظر را stop میکنیم.         
 ۱- از بخش Object Explorer کلیک راست بر روی Maintenance Plan و انتخاب Maintenace Plan Wizard.
![001](/trnwikistatic/Operation_And_Support/SQL Server Backup/001.png)
۲-گزینه ی Next در صفحه ی اول.
۳-برای Plan جدید یک نام تعریف میکنیم مابقی تنظیمات دستنخورده باقی میماند.گزینه ی change را انتخاب کرده.
![002](/trnwikistatic/Operation_And_Support/SQL Server Backup/002.png)
 ۴-در این صفحه از بخش Frequency دوره و تعداد دفعات بک اپ گیری را میتوانیم انتخاب کنیم(حالت (روزانه)Daily و روزانه یک بار).با کلیک رادیو باکس Occurs at Once میتوانیم زمان اجرای بک اپ را تنظیم کنیم .از بخش Duration زمان پایان را بر روی No End Date قرار میدهیم.
![003](/trnwikistatic/Operation_And_Support/SQL Server Backup/003.png)
۵-در این صفحه وظایف Plan را مشخص میکنیم (برای بک اپ کامل بر روی گزینه های Check Database Integrity و Shrink Database و Backup Database Full کلیک میکنیم). 
![004](/trnwikistatic/Operation_And_Support/SQL Server Backup/004.png)
۶-در این صفحه ترتیب وظایف Plan خود را مشخص میکنیم([3-integrity-1][shrink-2][FullBackup])
![005](/trnwikistatic/Operation_And_Support/SQL Server Backup/005.png)
۷-در این صفحه از بخش Databases باید به ازای هر تسک , دیتابیس هایی را که میخوایم از ان بک اپ بگیریم مشخص کنیم(یک بار برای integrity یک بار برای Shrink).
![006](/trnwikistatic/Operation_And_Support/SQL Server Backup/006.png)
۸- برای مشخص کردن وظیفه Full Backup مراحل زیر رادنبال کنید:
*از تب General دیتابیس ها مشخص میشوند.
![007](/trnwikistatic/Operation_And_Support/SQL Server Backup/007.png)
![008](/trnwikistatic/Operation_And_Support/SQL Server Backup/008.png)
*از تب Destination تیک باکس Create a sup-directory For Each Backup زده شود و از قسمت Folder ادرس فایلی که میخواهیم در ان بک اپ گرفته شود میدهیم(ترجیحا در درایو محل نصب سیستم عامل قرار نگیرد)
![009](/trnwikistatic/Operation_And_Support/SQL Server Backup/009.png)
*از تب Option تیک باکس Backup Will Expire (برای جلوگیری از انباشته شدن حجم بک ها) زده شود و با تیک رادیو باکس After میتوانیم مشخص کنیم بک اپ بعد از چند روز پاک شود(معمولا ۷ روز).تیک باکس Verify Backup Integrity نیز باید زده شود.
![010](/trnwikistatic/Operation_And_Support/SQL Server Backup/010.png)
۹-سپس صبر میکنیم تا Plan ما ساخته شود(در صورتی که با پیغام Success مواجه شویم برای اطمینان از صحت Plan ساخته شده به فایلی که ادرس ان را داده ایم تا بک اپ ما ساخته شود مراجعه میکنیم تا فایل های بک اپ گرفته شده را بررسی کنیم).
![011](/trnwikistatic/Operation_And_Support/SQL Server Backup/011.png)
۱۰-روی Plan Backup که ساخته ایم کلیک راست کرده و Execute را انتخاب میکنیم در صورت مشاهده ی پیغام زیر مراحلی که گفته میشود را طی میکنیم:
![012](/trnwikistatic/Operation_And_Support/SQL Server Backup/012.png)
![013](/trnwikistatic/Operation_And_Support/SQL Server Backup/013.png)
*ارور بالا به این معناست که SQL Server Agent Service به صورت پیشفرض اجرا نشده و ما باید ان را اجرا کنیم:
![014](/trnwikistatic/Operation_And_Support/SQL Server Backup/014.png)
![015](/trnwikistatic/Operation_And_Support/SQL Server Backup/015.png)
*سپس دوباره Plan مورد نظر را Execute میکنیم در صورت برطرف شدن مشکل با پیغام زیر مواجه میشویم:
![017](/trnwikistatic/Operation_And_Support/SQL Server Backup/017.png)
۱۱-برای تحت کنترل در اوردن errorlog ها مراحل زیر را طی میکنیم:
*کلیک راست روی SQL Server Logs و گزینه ی Configure را انتخاب کرده.در صفحه ی باز تیک  ‌Limit the Number of Error Log را زده و مقدار Muximum Number of Error Log Files را روی ۶ قرار داده و ok را کلیک میکنیم.
![016](/trnwikistatic/Operation_And_Support/SQL Server Backup/016.png)
*از تب ابزار Query را انتخاب کرده و دستور ‌sp_cycle_errorlog را چند بار اجرا میکنیم.
اقدامات بالا سبب محدود شدن حجم و تعداد log ها میشود.(برای اطمینان از صحت اقدامات شماره ی ۱۱ به ادرس MSSQL\LOG مراجعه کرده تا از تعداد و حجم log ها اطمینان حاصل شود)
*سرویس تامکت سرور مورد نظر را start کرده سپس دستور tail را اجرا میکنیم.
۱۲-برای ایجاد ‌Plan برای بک اپ گیری از Differential دیتابیس ها همانند مراحل بالا عمل میکنیم به غیر از مواردی که ذکر میشود:
*در این صفحه از قسمت Frequency حالت Occurs را برروی Daily و Recurs every را روزانه ۱ بار مشخص میکنیم.سپس از قسمت Daily Frequency چک باکس Occurs Every را انتخاب کرده سپس بر روی هر ۴ ساعت یک بار ان را تعیین کرده Starting At را بر روی 8Am و Ending AT را بر روی 12PM قرار میدهیم(در این قسمت باید دقت کنیم که زمان بک اپ گیری ‌FullBackup ما با زمان بک اپ گیری Differential ما تداخل نکند).  
![018](/trnwikistatic/Operation_And_Support/SQL Server Backup/018.png)
*در این تب فقط گزینه ی Backup Database Differential انتخاب شود.
![004](/trnwikistatic/Operation_And_Support/SQL Server Backup/004.png)
*مابقی مراحل بالا باید طی شود.
<== انتقال فایل های دیتابیس از یک درایو به درایو دیگر ==>
۱-ابتدا از طریق putty سرویس Tomcat سرور مورد نظر را stop کرده.
۲-دیتابیس مورد نظر را Detach کرده:
![019](/trnwikistatic/Operation_And_Support/SQL Server Backup/019.png)
  ۳- در صفحه ی باز شده تیک باکس های Drop All Connections و Update انتخاب شود:
![020](/trnwikistatic/Operation_And_Support/SQL Server Backup/020.png)
۴-به فایلی که دیتابیس ما در ان قرار دارد رفته و فایل دیتا بیس همراه با log ان(MDFوLDF به عنوان مثال در اینجا model.mdf و modellog.ldf) را به مکان مورد نظر انتقال میدهیم:
![021](/trnwikistatic/Operation_And_Support/SQL Server Backup/021.png)
۵-سپس بر روی Database کلیک راست کرده و Attach را انتخاب میکنیم:
![022](/trnwikistatic/Operation_And_Support/SQL Server Backup/022.png)
۶-در این صفحه ابتدا با استفاده از Add به تب attach کردن دیتابیس رفته و به ادرس جدید دیتابیس و log ان اشاره میکنیم تا دیتابیس مورد نظر attach شود:
![023](/trnwikistatic/Operation_And_Support/SQL Server Backup/023.png)
۷-سرویس tomcat را start کرده  سپس دستور tail را اجرا میکنیم.