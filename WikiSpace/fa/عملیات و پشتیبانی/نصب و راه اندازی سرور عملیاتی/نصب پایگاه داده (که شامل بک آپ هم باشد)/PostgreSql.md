## راهنمای نصب PostgreSQL روی سرور CentOS :



### ۱- دانلود و نصب PostgreSQL:

ابتدا باید با کاربری که برای db-server ساخته‌ایم(کاربر trn)؛ لاگین کنیم. سپس در ترمینال db-server دستور زیر را وارد می‌کنیم تا PostgreSQL را دانلود و نصب کند.(توجه داشته باشید  که باید ابتدا بررسی کنیم که سیستم db-server به اینترنت متصل هست یا نه. برای این کار می‌توانید ping  بگیرید).

    $ sudo rpm -Uvh https://yum.postgresql.org/11/redhat/rhel-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm

اگر سیستم db-server به اینترنت متصل نبود؛ یک فایل  zip در لینک زیر قرار داده شده که شما میتوانید با مراجعه به آن، موارد لازم برای نصب PostgreSQL را دانلود کرده و سپس با وارد کردن دستور زیر تمام آن‌ها را نصب کنید:

لینککککککک

    $ sudo rpm -Uvh postgresql11-*.rpm‌
    
سپس دیتابیس را بااستفاده از دستور زیر، Initialize(مقداردهی اولیه) می‌کنیم.

    $ sudo /usr/psql-11/bin/postgresql-11-setup initdb
    
برای فعال کردن سرویس PgAdmin دستور زیر را وارد کنید:

    $ sudo systemctl enable postgresql-11.service
    
   برای شروع کردن سرویس PgAdmin هم دستور زیر را وارد کنید:
    
    $ sudo systemctl enable postgresql-11.service
    
و با دستور زیر بررسی کنید که سرویس start شده باشد:

    $ sudo systemctl start postgresql-11.service
    
### ۲- ایجاد دیتابیس‌ها داخل PostgreSQL:

با استفاده از دستور زیر، می‌توانیم از کاربر فعلی به کاربر root تبدیل شویم:

    $ su

گذرواژه کاربر root در اینجا همان گذرواژه کاربر root سیستم db-server است. و شما با وارد کردن این گذرواژه می‌توانید به کاربر root، تبدیل شوید(switch کنید). سپس با وارد کردن دستور زیر از کاربر rootبه کاربر postgres، سوئیچ کنید.دلیل اینکه به طور مستقیم از کاربر فعلی به کاربر postgres سوئیچ نکردیم این است که در حالت اول نیازی به وارد کردن گذرواژه کاربر postgres نیست اما در حالت دوم این گذرواژه از شما درخواست می‌شود. پس  اگر کاربر فعلی ما root بود؛ داریم:

    $ su postgres 

ولی اگر کاربر فعلی کاربر غیر root(کاربر trn) بود؛ داریم:

    $ sudo su postgres

و گذرواژه کاربر trn (که *@*@Taraan است) را وارد می‌کنیم.

با استفاده از دستور زیر شما وارد postgresql می‌شوید:

    $ psql

و اگر دستور زیر را وارد کنید؛ می‌توانید از راهنمای PostgreSQL بهره ببرید:

    $ help

همیشه برای دیدن لیست دیتابیس‌های موجود؛ دستور زیر را وارد می‌کنیم:

    $ \l

با استفاده از دستورات زیر، سه عدد ROLE و سه عدد دیتابیس ایجاد می‌کنیم. (برای هر ROLE، دیتابیس هم نام خودش رایجاد می‌کنیم و در واقع این ROLE ها را باید به عنوان همان OWNER دیتابیس های متناظرشان قرار دهیم). داریم:

    $ CREATE ROLE core WITH PASSWORD 'Taraan@*@*' LOGIN;
    $ CREATE ROLE bpm WITH PASSWORD 'Taraan@*@*' LOGIN;
    $ CREATE ROLE dashboard WITH PASSWORD 'Taraan@*@*' LOGIN;
    
    $ CREATE DATABASE core ENCODING 'UTF8' OWNER core;
    $ CREATE DATABASE bpm ENCODING 'UTF8' OWNER bpm;
    $ CREATE DATABASE dashboard ENCODING 'UTF8' OWNER dashboard;

حال اگر دوباره دستور زیر را وارد کنیم؛ می‌توانیم اضافه شدن دیتابیس‌هایی که ساختیم به داخل لیست دیتابیس‌ها را ببینیم:

    $ \l

برای خروج از محیط bash نرم‌افزار PostgreSQL دو دستور زیر را به ترتیب وارد می‌کنیم:

    $ quit
    
    $ \q
    
اگر با دستور «quit $» موفقیت آمیز نبود می‌توانید دکمه‌های ctrl و c را همزمان بفشارید و سپس دستور «q\ $» را وارد کنید.

### ۳- انجام تنظیمات PostgreSQL در فایل postgresql.conf:

به صورت پیش فرض PostgreSQL طوری تنظیم شده که به localhost وصل(bound) شود.
 اگر دستور زیر را وارد کنید می‌توانید مطابق شکل زیر، پورت‌ها و IP هایی را که برای اپلیکیشن‌های سیستم فعال هستند و اصطلاحا در حال LISTEN هستند را ببینید(همانند شکل زیر).
 
    $ netstat -nlt

![شکل شماره ۱](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/DB_Installation_Which_Includes_Back_Up/0001.png)

همانطور که در شکل بالا می‌بینید؛ پورت 5432 به آدرس localhost وصل است که به این معنی می‌باشد که هر گونه اتصالی از خارج به PostgreSQL ناموفق خواهد بود. برای آزمایش می‌توانید با وارد کردن دستور telnet که در زیر مشاهده می‌کنید؛ روی app-server بررسی کنید که آیا می‌توان از app-server به PostgreSQL که روی db-server قرار دارد وصل شد یا نه: 

  ![شکل شماره ۲](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/DB_Installation_Which_Includes_Back_Up/0002.png)
  
نکته: اگر با وارد کردن دستور فوق، پیامی مبنی بر نبود یا نصب نشدن telnet دیدیم باید با دستور زیر، آن را نصب کنیم:

    $ sudo yum install telnet
    
و سپس دوباره دستور قبل را وارد می‌کنیم.
  
همانطور که در شکل فوق مشاهده کردید؛ شما از طریق آدرس‌های خارجی نظیر app-sever امکان وصل شدن به PostgreSQL سیستم db-server را ندارید. برای حل این مشکل ابتدا باید فایل postgresql.conf را باید بیابیم:

    $ sudo find / -name postgresql.conf
    
پس از یافتن فایل، به محلی که در آنجا قرار دارد رفته و با استفاده از دستور زیر، فایل را باز می‌کنیم و روی دکمه insert صفحه کلید می‌فشاریم تا بتوانیم تغییراتی که مد نظر داریم داخل فایل ایجاد کنیم.

     $ cd postgresql.confمسیر‌و‌نام‌پوشه‌والد‌فایل‌
    مسیر‌و‌نام‌فایل‌به‌همراه‌پسوند vi $     

نکته: اگر در مسیر امکان یافتن فایل یا رفتن به مسیر مورد نظر را نداشتیم؛ احتمالا به این دلیل است که به آن پوشه یا فایل دسترسی نداریم. پس با دستور chown و chgrp دسترسی لازم را به کاربر trn روی فایل مورد نظر می‌دهیم:

    $  sudo chgrp trn به‌همراه‌مسیرشpostgresql.conf
    $  sudo chown trn به‌همراه‌مسیرشpostgresql.conf

در یکی از خطوط داریم:

    listen_addresses = 'localhost'
    
به جای کلمه localhost کاراکتر * را قرار می‌دهیم و علامت # کنار خط را پاک می‌کنیم. سپس دکمه ctrl و c روی صفخه کلید را با هم می‌فشاریم تا فایل بسته شود.

برای اینکه قبل از بسته شدن فایل، تغییرات ما ذخیره شوند؛ دستور زیر را وارد می‌کنیم: 

    $ wq!

اما اگر قصد ذخیره شدن تغییرات را نداشته باشیم باید دستور زیر را وارد کنیم:

    $ q!
    
سپس با دستور زیر، سرویس PostgreSQL را start می‌کنیم:

    $ sudo systemctl start postgresql-11
    
با دستور زیر وضعیت سرویس PostgreSQL را بررسی می‌کنیم که آیا start شده یا نه:

    $ sudo systemctl status postgresql-11
    
با استفاده از دستور زیر هم می‌توانید لاگ سرویس را مشاهده کنید: 

    $ journalctl -xe
    
دوباره دستور  «netstat -nlt» را وارد می‌کنیم.

 ![شکل شماره ۳](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/DB_Installation_Which_Includes_Back_Up/0003.png)
 
 همانطور که در تصویر فوق مشاهده می‌کنید؛ آدرس Local Address که پورت 5432 را listen می‌کند؛ از «localhost» به «0.0.0.0» تغییر کرده است.
 
 ### ۴- انجام تنظیمات PostgreSQL در فایل pg_hba.conf:
 
 برای انجام این کار فایل pg_hba.conf را همانند کاری که برای یافتن وتغییر دادن محتوای فایل postgresql.conf انجام دادیم ؛ یافته و دو خط  زیر را به انتهای محتویات فایل، اضافه کرده و سپس ذخیره می‌کنیم:
 
 ![شکل شماره ۴](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/DB_Installation_Which_Includes_Back_Up/0004.png)
 
 و دوباره سرویس PostgreSQL را stop و پس از آن start می‌کنیم.
 اگر هنگام restart کردن سرویس PostgreSQL به مشکلی برخوردیم و با گرفتن status مشاهده کردیم که هنوز سرویس deactive است دستور زیر را وارد می‌کنیم تا به کاربر postgres روی پوشه pgsql دسترسی می‌دهیم تا بتواند سرویس را start کند.
 
    $ sudo chgrp -R postgres /var/lib/pgsql
    $ sudo chown -R postgres /var/lib/pgsql
    
حالا سرویس را start می‌کنیم. بعد از start شدن، با دستور زیر روی db-server از وصل شدن db-server به PostgreSQL مطمئن می‌شویم:

    $ telnet localhost 5432
    
همینطور روی app-server هم دستور زیر را وارد می‌کنیم:

    $ telnet db-serverآدرسIP 5432
    
تا ببینیم آیا app-server به PostgreSQL روی db-server متصل می‌شود یا نه.


## راهنمای نصب PostgreSQL روی سرور Windows :

ابتدا با کلیک روی لینک زیر، فایل نصبی نرم‌افزار postgresql را دانلود کرده و مراحل نصب را به ترتیب زیر دنبال کنید:
۱- روی فایل دانلود شده راست کلیک کرده و گزینه Run as administrator را انتخاب کنید.

![شکل شماره ۵](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/DB_Installation_Which_Includes_Back_Up/0005.png)

در پنجره باز شده، روی Yes کلیک کنید.

![شکل شماره ۶](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/DB_Installation_Which_Includes_Back_Up/0006.png)

 و در پنجره بعدی روی Next کلیک کنید.

![شکل شماره ۷](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/DB_Installation_Which_Includes_Back_Up/0007.png)

محل نصب نرم‌افزار را تعیین کنید(نیازی به تغییر مسیر ندارید) و روی Next کلیک کنید.

![شکل شماره ۸](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/DB_Installation_Which_Includes_Back_Up/0008.png)

یک پوشه را برای ذخیره شدن داده‌ها در آن، انتخاب کنید(نیازی به تغییر این فیلد نیست) و روی Next کلیک کنید.  

![شکل شماره ۹](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/DB_Installation_Which_Includes_Back_Up/0009.png)

در پنجره باز شده، دو فیلد برای وارد کردن گذرواژه برای Super User(که همان کاربر postgres می‌باشد.) وجود دارد. در هر دو فیلد مقدار taraan را قرار داده و روی Next کلیک کنید.
 **نکته:** این گذرواژه بعد از نصب نرم افزار برای وصل شدن به پایگاه داده، مورد نیاز خواهد بود.

![شکل شماره ۱۰](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/DB_Installation_Which_Includes_Back_Up/0010.png)

حالا در پنجره باز شده، فیلد local را تغییر ندهید(باید مقدارش برابر با Default local باشد). سپس روی Next کلیک کنید.

![شکل شماره ۱۱](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/DB_Installation_Which_Includes_Back_Up/0011.png)

پورت PostgreSQL به صورت پیش فرض 5432 است. آن را تغییر نداده و روی next کلیک کنید.

![شکل شماره ۱۲](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/DB_Installation_Which_Includes_Back_Up/0012.png)
![شکل شماره ۱۳](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/DB_Installation_Which_Includes_Back_Up/0013.png)
![شکل شماره ۱۴](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/DB_Installation_Which_Includes_Back_Up/0014.png)

تیک چک باکس را برداشته و روی Finish کلیک کنید. این کار برای صرفنظر کردن از دانلود ابزارها و ... می‌باشد.

![شکل شماره ۱۵](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/DB_Installation_Which_Includes_Back_Up/0015.png)

سپس به منوی start رفته و pgAdmin را یافته و روی آن کلیک می‌کنیم تا اجرا شود.

![شکل شماره ۱۶](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/DB_Installation_Which_Includes_Back_Up/0016.png)

حالا روی پایگاه داده راست کلیک کرده و روی connect کلیک می‌کنیم.

![شکل شماره ۱۷](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/DB_Installation_Which_Includes_Back_Up/0017.png)

گذرواژه‌ای که در هنگام نصب نرم‌افزار تعیین کرده بودیم را در این قسمت وارد می‌کنیم.

![شکل شماره ۱۸](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/DB_Installation_Which_Includes_Back_Up/0018.png)

و نهایتا اتصال پایگاه داده برقرار می‌شود.

![شکل شماره ۱۹](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/DB_Installation_Which_Includes_Back_Up/0019.png)

### ساخت Roleها در PostgreSQL:

برای ایجاد یک Role روی قسمت Login/Group Roles راست کلیک کرده و روی Create کلیک می‌کنیم. در ادامه پنجره‌ای باز می‌شود که در قسمت general نام Role مورد نظر ، در قسمت Definition گذرواژه مخصوص آن Role و در قسمت privilegs هم گزینه های «can login»،«Inherit right from the parent roles»و«can initiate streaming replication and backup» را تیک زده و نهایتا روی Save کلیک می‌کنیم.

### ساخت دیتابیس‌ها در PostgreSQL:

روی Databases راست کلیک کرده و روی Create Database کلیک کنید. در فیلد Database نام دیتابیس را وارد کرده و در قسمت Owner هم کاربر(Role) متناظر با آن دیتابیس را انتخاب کنید. سپس به قسمت SQL رفته و Query را هم بررسی کنید. پس از اطمینان یافتن از اینکه موارد درست لحاظ شده‌اند؛ روی Save کلیک کنید.
**نکته:** به یاد داشته باشید که سه کاربر(Role)با نام‌های«core»،«bpm»و«dashboard» و سه عدد دیتابیس با همین نام‌ها باید داشته باشیم.

### تنظیمات PostgreSQL:

**فایل postgresql.conf:** این فایل را یافته و بررسی می‌کنیم که در محتویات فایل در قسمت زیر به جای عبارت localhost،علامت * قرار گرفته باشد. اگر چنین نبود؛ به صورت دستی آن را تغییر می‌دهیم.

Listen_address="localhost"

سپس تغییرات را ذخیره کرده و می‌بندیم.

**فایل pg_hba.config:** این فایل را هم یافته و به انتهای فایل رفته و دو خط زیر را به آن اضافه می‌کنیم:

    host        all     all     0.0.0.0/0
    host        all     all     ::/0

سپس تغییرات را ذخیره کرده و فایل را می‌بندیم.

**نکته:** بعد از انجام تنظیمات فایل‌های postgresql.conf و pg_hba.conf ؛ لازم است که نرم‌افزار PostgreSQL را یک بار restart کنیم. برای این کار در منوی start ویندوز، عبارت services را جستجو می‌کنیم و بعد از یافتن روی آن کلیک می‌کنیم. در پنجره باز شده تمام سرویس‌های سیستم را می‌توانید ببینید. از درون لیست postgresql را یافته و آن را انتخاب می‌کنیم. سپس در قسمت سمت چپ پنجره، سه کلمه «start»،«stop»و«restart» را می‌توانید ببینید. روی restart کلیک می‌کنیم تا اپلیکیشن Postgresql دوباره راه‌اندازی شود. 

### بررسی اتصال db-server و app-server به دیتابیس:

برای بررسی اتصال سیستم db-server به PostgreSQL می‌توانید در منوی start ویندوز عبارت cmd را جستجو کنید و از دستور telnet استفاده کنید:
 
    $ telnet  db-serverIpAddress  5432
 
 اگر این نرم افزار در سیستم عامل نصب نشده باشد، به منوی start ویندوز رفته و عبارت cmd را جستجو کرده و روی نرم‌افزار cmd راست کلیک کنید و روی run as administrator کلیک کنید تا محیط cmd به شما نمایش داده شود. در ادامه دستور زیر را وارد کنید:
 
    $ dism /online /Enable-Feature /FeatureName:TelnetClient
    
بعد از نصب شدن telnet؛ دستور telnet و سپس دستور حرف O انگلیسی را وارد می‌کنیم. ازما درخواست IP هاست می‌شود. با وارد کردن آدرس IP سیستم db-server  و پورت 5432 اگر به ما پیغامی مبنی بر اتصال موفق را نمایش داد؛ به این معنی است که db-server با PostgreSQL ارتباط دارد. در غیر این صورت ارتباط برقرار نیست. همین دستور telnet را روی app-server البته با وارد کردن آدرس db-server در آن، بررسی می‌کنیم تا از ارتباط بین app-server و PostgreSQL(که روی db-server قرار دارد) اطمینان حاصل کنیم. 
 
    $ telnet  db-serverIpAddress  5432
    
**نکته:** اگر telnet روی app-server نصب نشده بود، با روش گفته شده آن را نصب می‌کنیم و سپس دستور telnet را وارد می‌کنیم.