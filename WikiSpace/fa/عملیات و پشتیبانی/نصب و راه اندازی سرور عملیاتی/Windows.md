 باید قبل از انجام هر کاری روی سیستم app-server، یک درایو خالی برای اپلیکیشن trad با فضای حداقل ۲۰۰ گیگابایت در اختیار داشته باشیم.
 
 ### ۱- دانلود نرم افزار PostgreSQL، انتقال فایل نصب به db-server و نصب آن: 

با کلیک روی لینک زیر، فایل نصبی متناسب با نسخه سیستم عامل db-server را یافته و آن را دانلود کنید. سپس از سیستم local به db-server انتقال دهید. در ادامه برای آموزش نحوه نصب اپلیکیشن PostgreSQL به لینک زیر مراجعه کنید:
 
 [لینک](/apps/wikiSpaces/wiki/dfg/عملیات+و+پشتیبانی/نصب+و+راه+اندازی+سرور+عملیاتی/نصب+پایگاه+داده+%28که+شامل+بک+آپ+هم+باشد%29/PostgreSql)
 
### ۲- دانلود جاوا ۸، انتقال فایل JDK به app-sever و نصب آن: 

 ابتدا متناسب با نسخه ویندوز(اینکه ۳۲ بیتی است یا ۶۴ بیتی) فایل jdk 8(با پسوند rpm را از releas-server دانلود می‌کنیم). برای دسترسی به release-server روی لینک زیر کلیک کنید.
 
[لینک](http://my.taraan.com/release/)

ابتدا در سیستم عامل ویندوز، دکمه‌های ترکیبی Windows + R را نگه دارید. در پنجره ظاهر شده عبارت CMD را تایپ کنید.

![شکل شماره ۴](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/0004.jpg)

 بعد از کلیک روی دکمه OK وارد پنجره Command Prompt می شوید. در این پنجره دستور زیر را تایپ کنید.
 
    java -version
    
ابتدا کلمه java را نوشته و بعد یک فضای خالی (space) ایجاد کنید و بعد یک علامت – (خط فاصله) و بعد از آن بدون وارد کردن space کلمه version را تایپ کنید و بعد دکمه ENTER را فشار دهید. توجه داشته باشید که عبارت version را با حروف کوچک تایپ کنید. (با تصویر زیر مواجه می‌شوید):

![شکل شماره ۵](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/0005.jpg)

روی فایل JDK راست کلیک کرده و بعد گزینه Run as Administrator را انتخاب کنید تا مراحل نصب آغاز شود.

![شکل شماره ۶](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/0006.jpg)

نصب JDK بسیار راحت است و فقط کافی است چند بار روی دکمه Next کلیک کنید. پیشنهاد می‌کنیم به هیچ عنوان تنظیمات پیش فرض مراحل نصب را تغییر ندهید. همانطور که در عکس بالا مشاهده می‌کنید روی دکمه Next کلیک کنید تا با عکس زیر مواجه شوید.

![شکل شماره ۷](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/0007.jpg)

در این پنجره می‌توانید مسیر نصب JDK را مشخص کنید.(لطفا تغییر ندهید تا با آموزش هماهنگ باشید). در این مرحله هم روی دکمه‌ی Next کلیک کنید.

![شکل شماره ۸](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/0008.jpg)

همانطور که در تصویر بالا مشاهده می‌کنید، منتظر بمانید تا مراحل نصب انجام شود. بعد از پایان این مرحله شما باید محل نصب JRE را انتخاب کنید که باز هم لطفا مانند قبل تغییری ایجاد نکنید و روی دکمه‌ی  Next کلیک کنید.

![شکل شماره ۹](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/0009.jpg)

دوباره منتظر بمانید تا عملیات نصب انجام شود.

![شکل شماره ۱۰](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/0010.jpg)

بعد از پایان عملیات نصب با پنجره زیر مواجه می‌شوید. روی دکمه Close کلیک کنید.

![شکل شماره ۱۱](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/0011.jpg)

تا اینجای کار ما جاوا را نصب کرده‌ایم. حال باید جاوا را به سیستم معرفی کنیم. برای این کار، ابتدا وارد کنترل پنل شوید و بعد بر روی گزینه System کلیک کنید. (تصویر زیر، دایره قرمز رنگ).

![شکل شماره ۱۲](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/0012.jpg)

بعد از کلیک کردن بر روی System وارد پنجره زیر می‌شوید. و بعد روی گزینه Advanced system settings کلیک کنید. (عکس زیر، دایره قرمز رنگ).

![شکل شماره ۱۳](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/0013.jpg)

بعد از کلیک کردن روی گزینه Advanced system settings وارد پنجره زیر می‌شوید. که طبق عکس زیر روی گزینه مشخص شده کلیک کنید.

![شکل شماره ۱۴](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/0014.jpg)

بعد از کلیک کردن روی گزینه Environment Variables وارد پنجره جدیدی می‌شوید همانند عکس زیر. در قسمت system variables روی دکمه New کلیک کنید.

![شکل شماره ۱۵](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/0015.jpg)

بعد از کلیک روی دکمه New پنجره‌ای باز می‌شود که ما می‌توانیم یک متغیر جدید بسازیم. برای نام متغیر، دقیقا جمله زیر را بنویسید(حساس به حروف بزرگ و کوچک):

    JAVA_HOME
    
و برای مقداره متغیر (Variable value) مسیر نصب JDK را قرار دهید. (اگر تنظیمات پیش فرض نصب JDK را تغییر نداده باشید، JDK در مسیر: **C:\Program Files\Java\jdk1.8.0_60** نصب شده است. بنابراین برای مقدار value مسیر JDK را کپی کنید و در قسمت value پیست(paste) کنید و بعد  روی دکمه OK کلیک کنید (همانند تصویر زیر).

![شکل شماره ۱۶](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/0016.jpg)

بعد از این مرحله باید مسیر دایرکتوری bin را در متغیر سیستمی PATH قرار دهیم. برای این کار دوباره در قسمت system variables به دنبال متغیر path بگردید.( همانند تصویر زیر).

![شکل شماره ۱۷](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/0017.jpg)

بعد از انتخاب متغیر path (کلیک روی path)، روی دکمه Edit… کلیک کنید تا پنجره زیر ظاهر شود. در قسمت value به آخر خط رفته (می توان با فشار دادن دکمه End روی کیبورد به آخر خط رفته) و سپس یک سِمی کالِن قرار دهید.
نکته: برای نوشتن سمی کالن، ابتدا زبان کیبورد سیستم را انگلیسی کرده و بعد دکمه دو نقطه را فشار دهید(همانند تصویر زیر):
 
![شکل شماره ۱۸](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/0018.jpg)

بعد از قرار دادن سمی کالن، نشانگر ماوس خود را بعد از سمی کالن قرار دهید و بعد مسیر دایرکتوری bin را در این قسمت paste کنید.مسیر دایرکتوری: **C:\Program Files\Java\jdk1.8.0_60\bin**. (همانند عکس زیر):

![شکل شماره ۱۹](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/0019.jpg)

سپس در قسمت system variables دوباره روی دکمه **...New** کلیک می‌کنیم تا یک متغیر دیگر هم به متغیرهای سیستم اضافه کنیم. در فیلد Variable name  مقدار **JAVA_TOOL_OPTIONS** و در فیلد Variable value مقدار **Dfile.encoding=UTF8-** را قرار داده سپس روی OK کلیک می‌کنیم.

بعد از این کار تمام پنجره‌های باز را OK کرده و ببندید. در آخر دوباره وارد پنجره Command Prompt شوید و دوباره دستور زیر را وارد کنید:

    java -version

![شکل شماره ۲۰](/trnwikistatic/Operation_And_Support/Installation_And_Operation_Of_The_Server/0020.jpg)


### ۳- گرفتن پوشه‌ها و فایل‌های لازم از release-server:

برای انجام این کار باید در ابتدا آدرس «my.taraan.com» را در نوار آدرس مرورگر خود وارد کنید و پروژه trad و کامپوننت های زیر را از آن دانلود کنید:

m-trn
m-camunda
mantle-udm
mantle-usl
simplescreens
hivemind
m-erp
moqui-elasticsearch
    
 در ادامه از منبع ذکر شده، base-component و trad را هم دانلود کرده و سپس همه آن‌ها را extract می‌کنیم و نهایتا base-component را در مسیر /trad/runtime قرار داده و کامپوننت‌ها را به داخل پوشه /trad/runtime/component انتقال می‌دهیم. سپس تنظیمات trad و پایگاه‌های داده(که روی db-server ایجاد شده‌اند) و همچنین کامنت کردن پلاگین را داخل فایل‌های زیر انجام می‌دهیم.
 
 دقت داشته باشید که اگر سیستم local ما لینوکسی باشد؛ باید با کاربر trn(که روی app-server ساختیم) این کار را انجام بدهیم. برای unzip کردن فایل‌ها از دستور زیر استفاده می‌کنیم(در صورت لزوم در ابتدای دستور کلمه sudo را هم اضافه می‌کنیم)؛ اما اگر سیستم local ویندوز بود؛ با اپلیکیشن‌هایی مانند winzip فایل‌های دانلود شده را استخراج می‌کنیم 
 
    $ unzip مسیر‌فایل‌و‌نام‌فایل‌به‌همراه‌پسوند
 
 و نهایتا base-component را در مسیر /trad/runtime قرار داده و کامپوننت‌ها را به داخل پوشه /trad/runtime/component انتقال می‌دهیم
  
### ۴- تنظیمات فایل‌های bpm-platform-prod.xml و moqui-production-conf.xml :
  
  سپس تنظیمات trad و پایگاه‌های داده(که روی db-server ایجاد شده‌اند) و همچنین کامنت کردن پلاگین را داخل فایل‌های زیر انجام می‌دهیم:
 
     trad/runtime/conf/bpm-platform-prod.xml
     
     trad/runtime/conf/moqui-production-conf.xml
     
نکته: هنگام انجام تنظیمات پایگاه داده در فایل‌های مذکور؛ منظور از jdbcusername همان Role دیتابیس‌هایی است که در db-server ایجاد کرده بودیم و jdbcpassword هم password همان دیتابیس‌‌ها است(که ما این اطلاعات را زمان ایجاد دیتابیس‌ها در db-sever وارد کرده بودیم).
     
نکته: پلاگین مذکور در انتهای فایل bpm-platform-prod.xml قرار دارد.  
            
            ``<plugin>
                <class>com.taraan.bpm.identity.http.plugin.MoquiIdentityProviderPlugin</class>
                <properties>
                    <property name="managerUser">trn</property>
                    <property name="managerPassword">Taraan28@*</property>
                </properties>
            </plugin>``

همانطور که در تصویر بالا قابل مشاهده است؛ دقت کنید که در مقدار managerUser برابر با trn مقدار managerPassword برابر با *@Taraan28 باشد.
توجه: در داخل فایل‌های  bpm-platform-prod.xml و moqui-production-conf.xml در قسمت تنظیمات دیتابیس، منظور از jdbcusername همان role دیتابیس و در قسمت jdbcPassword هم password همان دیتابیس باید قرار بگیرد. مورد دیگری که باید در فایل bpm-platform-prod.xml رعایت شود؛ در صورت نبود، اضافه کردن دو attribute دیگر به تنظیمات دیتابیس است که دقیقا بعد از ""=schema-name و قبل از بسته شدن تگ قرار می‌گیرد.

    runtime-add-missing="false"
    startup-add-missing="true"
    
    
در فایل MoquiProductionConf.xml خطی مطابق شکل زیر وجود دارد. این خط برای اتصال سیستم‌های دیگر به app-server به منظور استفاده از اپلیکیشن BPMS مورد استفاده قرار می‌گیرد. توجه داشته باشید که برای اتصال سیستم‌های مذکور به app-server، باید در تگ زیر، به جای عبارت localhost، آدرس IP سیستم app-server را قرار بدهیم.

    <default-property name="BPMS_remoteBpmsContextPath" value="http://localhost:8080"/>  
    
###  ۵- تنظیمات فایل MoquiInit.properties: 

این فایل داخل پوشه opt/trad/tomcat/webapps/ROOT/WEB-INF/classes/ قرار دارد. با توجه به محل قرار گیری trad داخل app-server بررسی کنید که آیا مسیرهایی که در داخل این فایل نوشته شده است؛ صحیح است یا نه. اگر نیاز به تغییر بود آن‌ها را تغییر دهید.
نوجه: در این فایل حتما دو مورد زیر را بررسی کنید که مسیر درست برای این موارد قرار گرفته باشد:

moqui.runtime=/opt/trad/runtime

moqui.conf=/opt/trad/runtime/conf/MoquiProductionConf.xml 


سپس تغییرات را ذخیره و فایل را ببندید.


###  ۶- انتقال پوشه trad به داخل درایو تخصیص داده شده به اپلیکیشن :

حالا باید پوشه trad را zip کرده و داخل درایو مذکور در app-server قرار دهیم.
 
 
### ۷- تنظیمات فایل trad.service:

فایل را باز کرده و  مسیر فایل‌ها را چک می‌کنیم که آیا مسیرها درست هستند یا نه در صورت لزوم اصلاح می‌کنیم.
در یکی از خطوط داریم:

    user=admin
    
به جای admin کلمه trn را قرار می‌دهیم زیرا نام کاربر app-server ما trn است.

سپس فایل را ذخیره کرده و می‌بندیم و نهایتا آن را با استفاده از دستور زیر در پوشه system کپی می‌کنیم:

    $ sudo cp مسیر‌و‌نام‌و‌پسوند‌فایل‌مبدا /etc/systemd/system/

نکته: اگر خطای دسترسی نمایش داده شد، به کاربر trn دسترسی لازم را به پوشه system می‌دهیم:
 
     $ sudo chown -R  trn /etc/systemd/system
 
     $ sudo chgrp -R trn /etc/systemd/system
 
 و بعد عمل کپی کردن فایل انجام می‌دهیم.

    
### ۸- بستن نرم افزارهای در حال اجرا:

در صفحه دسکتاپ ویندوز اگر دکمه های Ctrl و Shift و Esc را با هم بفشاریم؛ پنجره task manager برای ما نمایش داده می‌شود. داخل این پنجره اپلیکیشن‌های در حال اجرا قابل مشاهده هستند. اگر اپلیکیشن‌های جاوا و tomcat در لیست برنامه‌های در حال اجرا بودند؛ آنها را با دکمه End Task (یا با راست کلیک کردن روی هر کدام از آنها و انتخاب گزینه End task) می‌بندیم.


### ۹- متوقف و غیرفعال کردن فایروال‌ها:

در این مرحله باید تمام فایروال‌های ویندوز را در حالت غیرفعال قرار دهیم. برای انجام این کار داخل control panel ویندوز وارد قسمت windows firewalls شده و تمام فایروال‌ها را غیر فعال می‌کنیم.

### ۱۰- اجرای اولیه trad:

برای اجرای اپلیکیشن کافی است به پوشه درایو اپلیکیشن رفته و در مسیر trad\runtime\tomcat\bin یک فایل به نام startup.bat وجود دارد با دابل کلیک ردن روی این فایل، اپلیکیشن شروع به کار می‌کند. برای متوقف کردن اپلیکیشن هم کافی است روی shutdown دابل کلیک کنید.
**نکته:** اگر در منوی استارت عبارت services را جستجو کنید؛ برایتان قسمت services ویندوز نمایش داده می‌شود که با کلیک کردن روی آن،پنجره‌ای برایتان نمایش داده می‌شود که در آن هم می‌توانید سرویس‌ها و وضعیت آنها را بررسی کنید و هم می‌توانید وضعیت هر کدام را (مثلا از وضعیت stop به حالت start یا بالعکس) تغییر بدهید.
نام سرویس  مورد نظرتان (به عنوان مثال سرویس tRad)را یافته و آن را فعال و شروع(start) کرد.
دستور زیر برای بررسی وضعیت اپلیکیشن ما می‌باشد:

هنگام start کردن یا حتی stop کردن اپلیکیشن؛ حتما لاگ‌های سیستم را در مسیر trad/tomcat/logs بررسی کنید که نرم افزار بدون خطا اجرا شده باشد.

بعد از start شدن اپلیکیشن؛ مرورگر(chrome یا firefox) را روی سیستم local باز می‌کنیم و در نوار آدرس مقدار زیر را وارد می‌کنیم:

  8080:آدرس IP app-server      

صفحه‌ای برای ما باز می‌شود که باید داخل فیلدها را با مقادیر، مطابق زیر پر کنیم:

    username(نام کاربری) = trn
    password(گذرواژه) = Taraan28@*
    confirm password(گذرواژه) = Taraan28@*
    full name(نام و نام خانوادگی) = taraan admin
    email (آدرس ایمیل) = support@taraan.com

سپس روی دکمه Create Initial Admin Account کلیک می‌کنیم.

بعد از لاگین شدن باید logout کنیم و در نوار آدرس بالای مرورگر، به انتهای آدرس مقدار زیر را اضافه کنیم:

    /bpms/app/

سپس روی دکمه enter صفحه کلید می‌فشاریم تا صفحه ثبت نام camunda به ما نمایش داده شود و بعد از آن، فیلدها را به شکل زیر مقدار دهی می‌کنیم:

    username(نام کاربری) = trn
    password(گذرواژه) = Taraan28@*
    confirm password(گذرواژه) = Taraan28@*
    first name(نام و نام خانوادگی) = taraan
    last name(نام و نام خانوادگی) = admin
    
مورد ایمیل اختیاری است.

در نهایت روی دکمه enter صفحه کلید می‌فشاریم تا صفحه لاگین کموندا برای ما نمایش داده شود.

سپس در app-server مانند با دابل کلیک روی shutdown.bat، اپلیکیشن trad را stop می‌کنیم.

و سپس فایل bpm-platform-prod.xml را باز کرده و پلاگین را از حالت کامنت خارج کرده و ذخیره می‌کنیم. سپس با دابل کلیک روی فایل startup.bat، اپلیکیشن trad را دوباره start می‌کنیم.
    
بعد از start شدن اپلیکیشن؛ لاگین کرده و دسترسی‌های Camunda Admin و Full Access Tasklist را به کاربر trn می‌دهیم. در ادامه، در دسترسی‌ها در قسمت local مقدار fa-IR را انتخاب کرده و روی update(به روز رسانی) کلیک می‌کنیم.

نهایتا از قسمت «پنجره واحد خدمات» وارد قسمت «کارپوشه» شده و روی علامت + کلیک می‌کنیم تا کارپوشه بسازیم. دو تا کارپوشه به شکل زیر می‌سازیم:

الف) تمام وظایف : فیلد «کلید» در قسمت شاخص  را برابر با مقدار «فعال» و در فیلد «مقدار»، عبارت true را قرار می‌دهیم و ذخیره می‌کنیم.
ب) وظایف من: فیلد «کلید» در قسمت شاخص  را برابر با مقدار «انجام دهنده» و در فیلد «مقدار»، عبارت {()currentUser}$ را قرار می‌دهیم و در قسمت «اجازه‌های دسترسی»، گزینه‌ی «برای همه» را فعال کرده ذخیره می‌کنیم.