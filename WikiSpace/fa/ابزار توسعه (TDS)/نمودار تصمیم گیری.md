### **نمودار تصمیم گیری**

Decision Model and Notation (DMN) is a standard for Business Decision Management.

Currently the Camunda DMN engine partially supports DMN 1.1, including Decision Tables, Decision Literal Expressions, Decision Requirements Graphs and the Friendly Enough Expression Language (FEEL).

[**Decision Table**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/نمودار+تصمیم+گیری/decision-table.md)
Specify Decision Logic as a Table
[**Decision Literal Expression**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/نمودار+تصمیم+گیری/decision-literal-expression.md)
Specify Decision Logic as an Expression
[**Decision Requirements Graph**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/نمودار+تصمیم+گیری/drg.md)
Models Dependencies between Decisions
[**FEEL**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/نمودار+تصمیم+گیری/feel.md)
DMN’s default Expression Language
[**Extension Reference**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/نمودار+تصمیم+گیری/custom-extensions.md)
Custom Extensions supported by Camunda