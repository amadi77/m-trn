
![input](/trnwikistatic/Development_Tools_TDS/dmn/decision-table/input.png)

A decision table can have one or more inputs, also called input clauses. An
input clause defines the id, label, expression and type of a decision table
input.

An input clause is represented by an `input` element inside a `decisionTable`
XML element.


    <definitions xmlns="http://www.omg.org/spec/DMN/20151101/dmn.xsd" id="definitions" name="definitions" namespace="http://camunda.org/schema/1.0/dmn">
      <decision id="dish" name="Dish">
        <decisionTable id="decisionTable">
          <input id="input1" label="Season">
            <inputExpression id="inputExpression1" typeRef="string">
              <text>season</text>
            </inputExpression>
          </input>
          <!-- ... -->
        </decisionTable>
      </decision>
    </definitions>


# Input Id

The input id is a unique identifier of the decision table input. It is used by
the Camunda BPMN platform to reference the input in the history of evaluated
decisions. Therefore, it is required by the Camunda DMN engine. It is set as
the `id` attribute of the `input` XML element.


    <input id="input1" label="Season">
      <inputExpression id="inputExpression1" typeRef="string">
        <text>season</text>
      </inputExpression>
    </input>


# Input Label

![input-label](/trnwikistatic/Development_Tools_TDS/dmn/decision-table/input-label.png)

An input label is a short description of the input. It is set on the `input`
XML element in the `label` attribute. Note that the label is not required but
recommended, since it helps to understand the decision.


    <input id="input1" label="Season">
      <inputExpression id="inputExpression1" typeRef="string">
        <text>season</text>
      </inputExpression>
    </input>


# Input Expression

![input-expression](/trnwikistatic/Development_Tools_TDS/dmn/decision-table/input-expression.png)

An input expression specifies how the value of the input clause is generated.
It is an expression which will be evaluated by the DMN engine. It is usually
simple and references a variable which is available during the evaluation. The
expression is set inside a `text` element that is a child of the
`inputExpression` XML element.


    <input id="input1" label="Season">
      <inputExpression id="inputExpression1" typeRef="string">
        <text>season</text>
      </inputExpression>
    </input>


# Input Type Definition

![input-type-definition](/trnwikistatic/Development_Tools_TDS/dmn/decision-table/input-type-definition.png)

The type of the input clause can be specified by the `typeRef` attribute on the
`inputExpression` XML element. After the input expression is evaluated by the
DMN engine, it converts the result to the specified type. The supported types
are listed in the [User Guide](https://docs.camunda.org/manual/7.10/user-guide/dmn-engine/data-types/#supported-data-types).


    <input id="input1" label="Season">
      <inputExpression id="inputExpression1" typeRef="string">
        <text>season</text>
      </inputExpression>
    </input>


Note that the type is not required but recommended, since it helps to understand
the possible input values and provides a type safety to be aware of unexpected
input values.

# Input Expression Language

The expression language of the input expression can be specified by the
`expressionLanguage` attribute on the `inputExpression` XML element. The
supported expression languages are listed in the [User Guide](https://docs.camunda.org/manual/7.10/user-guide/dmn-engine/expressions-and-scripts/#supported-expression-languages).


    <input id="input1" label="Season">
      <inputExpression id="inputExpression1" typeRef="string" expressionLanguage="groovy">
        <text>season</text>
      </inputExpression>
    </input>


If no expression language is set then the global expression 
language, which is set on the `definitions` XML element, is used.


    <definitions id="definitions"
                 name="definitions"
                 xmlns="http://www.omg.org/spec/DMN/20151101/dmn.xsd"
                 expressionLanguage="groovy"
                 namespace="http://camunda.org/schema/1.0/dmn">
      <!-- ... -->
    </definitions>


In case no global expression language is set, the default expression language
is used instead. The default expression language for input expressions is JUEL.
Please refer to the [User Guide](https://docs.camunda.org/manual/7.10/user-guide/dmn-engine/expressions-and-scripts/#default-expression-languages) to read more about expression
languages.

# Input Variable Name

When the input expression is evaluated then the return value is stored in a variable.
The name of the variable can be specified by the `camunda:inputVariable`
[extension attribute](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/نمودار+تصمیم+گیری/custom-extensions/camunda-attributes.md#inputvariable) on the `input` element. By default, the
name is `cellInput`.

To use the attribute you have to define the Camunda DMN namespace
`xmlns:camunda="http://camunda.org/schema/1.0/dmn` in the XML.


    <definitions id="definitions"
                 name="definitions"
                 xmlns="http://www.omg.org/spec/DMN/20151101/dmn.xsd"
                 xmlns:camunda="http://camunda.org/schema/1.0/dmn"
                 namespace="http://camunda.org/schema/1.0/dmn">
      <decision id="dish" name="Dish">
        <decisionTable id="decisionTable">
          <input id="input1"
                 label="Season"
                 camunda:inputVariable="currentSeason">
            <!-- ... -->
          </input>
          <!-- ... -->
        </decisionTable>
      </decision>
    </definitions>


The variable can be used in an expression of an [input entry](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/نمودار+تصمیم+گیری/decision-table/rule.md#input-entry-condition). For example, the
JUEL expression `currentSeason != "Fall"` checks if the season input is not
`"Fall"`.
