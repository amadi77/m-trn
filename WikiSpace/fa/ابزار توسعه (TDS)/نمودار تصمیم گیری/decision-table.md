![decision-literal-expression](/trnwikistatic/Development_Tools_TDS/dmn/decision-table/dish-table.png)

A decision table represents decision logic which can be depicted as a table in DMN 1.1. It consists of inputs, outputs and rules.

A decision table is represented by a decisionTable element inside a decision XML element.

    <definitions xmlns="http://www.omg.org/spec/DMN/20151101/dmn.xsd" id="definitions" name="definitions" namespace="http://camunda.org/schema/1.0/dmn">
      <decision id="dish" name="Dish">
        <decisionTable id="decisionTable">
        <!-- ... -->
        </decisionTable>
      </decision>
    </definitions>
    
### **Decision Name**

![decision-literal-expression](/trnwikistatic/Development_Tools_TDS/dmn/decision-table/decision-name.png)

The name describes the decision for which the decision table provides the decision logic. It is set as the name attribute on the decision element.

    <decision id="dish" name="Dish">
      <decisionTable id="decisionTable">
      <!-- ... -->
      </decisionTable>
    </decision>

### **Decision Id**

![decision-literal-expression](/trnwikistatic/Development_Tools_TDS/dmn/decision-table/dish-table.png)

The id is the technical identifier of the decision. It is set in the id attribute on the decision element.

Each decision should have an unique id when it is deployed to the Camunda BPM platform. The engine uses the id as the decision key of the deployed DecisionDefinition.

    <decision id="dish" name="Dish">
      <decisionTable id="decisionTable">
      <!-- ... -->
      </decisionTable>
    </decision>
