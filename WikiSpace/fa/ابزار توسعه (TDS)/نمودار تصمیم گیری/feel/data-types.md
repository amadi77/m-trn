
The Camunda DMN engine supports the following FEEL data types.

# String

![feel](/trnwikistatic/Development_Tools_TDS/dmn/feel/string-type.png)

FEEL supports Strings. They must be encapsulated in double quotes. They
support only the equal [**comparison**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/نمودار+تصمیم+گیری/feel/language-elements.md#comparison) operator.

# Numeric Types

![feel](/trnwikistatic/Development_Tools_TDS/dmn/feel/integer-type.png)

FEEL supports numeric types like integer. In the Camunda DMN engine the
following numeric types are available:

- integer
- long
- double

Numeric types support all [**comparison**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/نمودار+تصمیم+گیری/feel/language-elements.md#comparison) operators and [**range**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/نمودار+تصمیم+گیری/feel/language-elements.md#range).

# Boolean

![feel](/trnwikistatic/Development_Tools_TDS/dmn/feel/boolean-type.png)

FEEL supports the boolean value `true` and `false`. The boolean type only
supports the equal [**comparison**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/نمودار+تصمیم+گیری/feel/language-elements.md#comparison) operator.

# Date

![feel](/trnwikistatic/Development_Tools_TDS/dmn/feel/date-type.png)

FEEL supports date types. In the Camunda DMN engine the following date types
are available:

- date and time

To create a date and time value, the function `date and time` has to be used
with a single String parameter. The parameter specifies the date and time in
the format `yyyy-MM-dd'T'HH:mm:ss`.

Date types support all [**comparison**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/نمودار+تصمیم+گیری/feel/language-elements.md#comparison) operators and [**range**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/نمودار+تصمیم+گیری/feel/language-elements.md#range).

