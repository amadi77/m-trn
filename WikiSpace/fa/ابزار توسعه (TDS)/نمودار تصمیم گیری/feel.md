Decision Model and Notation (DMN) defines a Friendly Enough Expression Language (FEEL). It can be used to evaluate expressions in a decision table.

The Camunda DMN engine only supports FEEL for [**input entries**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/نمودار+تصمیم+گیری/decision-table/rule.md/#input-entry-condition) of a decision table. This corresponds to FEEL simple unary tests.

[**Data Types**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/نمودار+تصمیم+گیری/feel/data-types.md)
Supported Data Types in FEEL
[**Language Elements**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/نمودار+تصمیم+گیری/feel/language-elements.md)
Supported FEEL Language Elements