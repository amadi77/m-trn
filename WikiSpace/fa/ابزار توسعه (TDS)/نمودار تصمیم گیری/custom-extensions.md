Camunda extends DMN with custom Extension Elements and Attributes defined in the http://camunda.org/schema/1.0/dmn namespace.

[**Extension Attributes**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/نمودار+تصمیم+گیری/custom-extensions/camunda-attributes.md)
Reference of Camunda Extension Attributes for DMN.