Gateways control token flow in a process. They allow modeling decisions based on data and events as well as fork / join concurrency.

[**Data-based Exclusive Gateway(XOR)**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/Gateways/exclusive-gateway.md)   
Model decisions based on data.
[**Conditional and Default Sequence Flows**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/Gateways/sequence-flow.md)
Concurrency and decisions without Gateways.
[**Parallel Gateway**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/Gateways/parallel-gateway.md)
Model fork / join concurrency.
[**Inclusive Gateway**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/Gatewaysinclusive-gateway.md)
Model conditional fork / join concurrency.
[**Event-based Gateway**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/Gateways/event-based-gateway.md)
Model decisions based on events.