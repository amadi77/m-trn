Camunda extends BPMN with custom Extension Elements and Attributes.

[**Extension Elements**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/custom-extensions/extension-elements.md)
Reference of Camunda Extension Attributes for BPMN.
[**Extension Attributes**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/custom-extensions/extension-attributes.md)
Reference of Camunda Extension Attributes for BPMN.