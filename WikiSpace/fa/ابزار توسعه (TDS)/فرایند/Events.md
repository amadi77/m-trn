BPMN defines different Event types. The following are supported by Camunda.

[**Start Events**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/Events/start-events.md)
Start Events define where a Process or Sub Process starts.
[**None Events**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/Events/none-events.md)
Events without a specified trigger and no specified behavior.
[**Message Events**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/Events/message-events.md)
Events catching / throwing messages.
[**Timer Events**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/Events/timer-events.md)
Events waiting on a timer condition.
[**Error Events**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/Events/error-events.md)
Events catching / throwing errors.
[**Escalation Events**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/Events/escalation-events.md)
Events catching / throwing escalations.
[**Signal Events**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/Events/signal-events.md)
Events catching / throwing signals.
[**Cancel and Compensation Events**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/Events/cancel-and-compensation-events.md)
Events throwing / catching compensation and cancel transaction events.
[**Conditional Events**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/Events/conditional-events.md)
Events catching conditional events.
[**Link Events**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/Events/link-events.md)
Bridge very long sequence flows.
[**Terminate Events**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/Events/terminate-event.md)
End a scope.