Tasks allow modeling the actual work being performed in the process. Different types of tasks are supported.
[**Service Task**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/tasks/service-task.md)
Invoke or execute business logic.
[**Send Task**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/tasks/send-task.md)
Send a message.
[**User Task**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/tasks/user-task.md)
A task performed by a human participant.
[**Business Rule Task**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/tasks/business-rule-task.md)
Execute an automated business decision.
[**Script Task**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/tasks/script-task.md)
Execute a Script.
[**Receive Task**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/tasks/receive-task.md)
Wait for a message to arrive.
[**Manual Task**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/tasks/manual-task.md)
A task which is performed externally.
[**Task Markers**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/tasks/task-markers.md)
Markers control operational aspects like repetition.