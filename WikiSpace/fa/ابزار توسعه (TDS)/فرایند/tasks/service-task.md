**افزودن service task به فرآیند به منظور ذخیره اطلاعات درون Data base : **

درون فرآیند مورد نظر خود نیاز است که فرم های تکمیل شده درون دیتا بیس نیز ذخیره شوند و نه اینکه تنها در روند فرآیند ذخیره شوند.
به این منظور نیاز است که در ابتدای فرآیند خود ، فایل system . groovy مربوطه را معرفی کنیم تا core local host قابل شناسایی باشد.
توجه به این نکته ضروری است که فایل groovy مورد نظر باید حتما درون پوشه src/main         resources/ test – system . grcovy قرار داشته باشد.


حال به منظور آدرس دهی به فایل groovy ، درون فرآیند خود ، در مراحل ابتدایی فرآیند خود ، یک script task را با فرمت groovy قرار می د هیم و در بخش Resource خود ، آدرس ( عنوان ) فایل groovy خود را قرار می دهیم.
حال پس از فرم مربوطه خود یک service task با implementation=connector قرار می دهیم .
حال در بخش connector ، input و  output های زیر را وارد می کنیم :

**1. input ها :**

*** url : script**

که در بخش script آن مقدار " آدرس سرویس درون swagger " + core host local را وارد می کنیم 

*** method : text**

که متد برای ذخیره باید از نوع post باشد.

*** Payload : script**

که فرمت آن groovy است و در بخش script آن ، API Name مربوط به container فرم و یا تمامی فیلد های درون فرم را درون یک مغیر با نام دلخواه می ریزیم سپس با استفاده از کد زیر اطلاعات را به صورت string ذخیره می کنیم :

S( نام متغر مربوطه , org.camunda.spin.DataFormats.json()).toString();

مثال :

def myTest = [:];
myTest.نام فیلد اول = نام فیلد اول;
myTest.نام فیلد دوم = نام فیلد دوم;
...
....
......
S(myTest , org.camunda.spin.DataFormats.json()).toString();

*** Headers : map**

Content_Type : application/json
Authorization : ${basicToken}
Accept : application/json


**2.out put ها **

یک متغیر خروجی با نام دلخواه از نوع script با فرمت groovy که درون بخش script آن کد زیر را وارد می کنیم :

S(response , org.camunda.spin.DataFormats.json()).mapTo("java.util.HashMap").نام یک فیلد از entity مورد نظر که pk باشد




A Service Task is used to invoke services. In Camunda this is done by calling Java code or providing a work item for an external worker to complete asynchronously.

![شکل شماره ۱](/trnwikistatic/Development_Tools_TDS/bpmn/tasks/00001.png)


# Calling Java Code

There are four ways of declaring how to invoke Java logic:

* Specifying a class that implements a JavaDelegate or ActivityBehavior
* Evaluating an expression that resolves to a delegation object
* Invoking a method expression
* Evaluating a value expression

To specify a class that is called during process execution, the fully qualified classname needs to be provided by the `camunda:class` attribute.


    <serviceTask id="javaService"
                 name="My Java Service Task"
                 camunda:class="org.camunda.bpm.MyJavaDelegate" />


Please refer to the [Java Delegate](https://docs.camunda.org/manual/7.10/user-guide/process-engine/delegation-code/#java-delegate) section of the [User Guide](https://docs.camunda.org/manual/7.10/user-guide/) for details on how to implement a Java Delegate.

It is also possible to use an expression that resolves to an object. This object must follow the
same rules as objects that are created when the `camunda:class` attribute is used.


    <serviceTask id="beanService"
                 name="My Bean Service Task"
                 camunda:delegateExpression="${myDelegateBean}" />


Or an expression which calls a method or resolves to a value.


    <serviceTask id="expressionService"
                 name="My Expression Service Task"
                 camunda:expression="${myBean.doWork()}" />


For more information about expression language as delegation code, please see the corresponding
[section](https://docs.camunda.org/manual/7.10/user-guide/process-engine/expression-language/#use-expression-language-as-delegation-code)
of the [User Guide](https://docs.camunda.org/manual/7.10/user-guide/).


## Generic Java Delegates & Field Injection

You can easily write generic Java Delegate classes which can be configured later on via the BPMN 2.0 XML in the Service Task. Please refer to the [Field Injection](https://docs.camunda.org/manual/7.10/user-guide/process-engine/delegation-code/#field-injection) section of the [User Guide](https://docs.camunda.org/manual/7.10/user-guide/) for details.


## Service Task Results

The return value of a service execution (for a Service Task exclusively using expressions) can be assigned to an already existing or to a new process variable by specifying the process variable name as a literal value for the `camunda:resultVariable` attribute of a Service Task definition. Any existing value for a specific process variable will be overwritten by the result value of the service execution. When not specifying a result variable name, the service execution result value is ignored.


    <serviceTask id="aMethodExpressionServiceTask"
               camunda:expression="#{myService.doSomething()}"
               camunda:resultVariable="myVar" />


In the example above, the result of the service execution (the return value of the `doSomething()` method invocation on object `myService`) is set to the process variable named `myVar` after the service execution completes.

![شکل شماره ۲](/trnwikistatic/Development_Tools_TDS/bpmn/tasks/00002.png)

# External Tasks

In contrast to calling Java code, where the process engine synchronously invokes Java logic, it is possible to implement a Service Task outside of the process engine's boundaries in the form of an external task. When a Service Task is declared external, the process engine offers a work item to workers that independently poll the engine for work to do. This decouples the implementation of tasks from the process engine and allows to cross system and technology boundaries. See the [user guide on external tasks](https://docs.camunda.org/manual/7.10/user-guide/process-engine/external-tasks/) for details on the concept and the relevant API.

To declare a Service Task to be handled externally, the attribute `camunda:type` can be set to `external` and the attribute `camunda:topic` specifies the external task's topic. For example, the following XML snippet defines an external Service Task with topic `ShipmentProcessing`:


    <serviceTask id="anExternalServiceTask"
               camunda:type="external"
               camunda:topic="ShipmentProcessing" />


# Camunda Extensions

<table class="table table-striped">
  <tr>
    <th>Attributes</th>
    <td>
    [camunda:asyncBefore](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/custom-extensions/extension-attributes.md#asyncbefore),
    [camunda:asyncAfter](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/custom-extensions/extension-attributes.md#asyncafter),
    [camunda:class](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/custom-extensions/extension-attributes.md#class),
    [camunda:delegateExpression](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/custom-extensions/extension-attributes.md#delegateexpression),
    [camunda:exclusive](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/custom-extensions/extension-attributes.md#exclusive),
    [camunda:expression](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/custom-extensions/extension-attributes.md#expression),
    [camunda:jobPriority](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/custom-extensions/extension-attributes.md#jobpriority),
    [camunda:resultVariable](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/custom-extensions/extension-attributes.md#resultvariable),
    [camunda:topic](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/custom-extensions/extension-attributes.md#topic),
    [camunda:type](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/custom-extensions/extension-attributes.md#type),
    [camunda:taskPriority](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/custom-extensions/extension-attributes.md#taskpriority)
    </td>
  </tr>
  <tr>
    <th>Extension Elements</th>
    <td>
    [camunda:failedJobRetryTimeCycle](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/custom-extensions/extension-elements.md#failedjobretrytimecycle),
    [camunda:field](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/custom-extensions/extension-elements.md#field),
    [camunda:connector](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/custom-extensions/extension-elements.md#connector),
    [camunda:inputOutput](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/custom-extensions/extension-elements.md#inputoutput)
    </td>
  </tr>
  <tr>
    <th>Constraints</th>
    <td>
      One of the attributes <code>camunda:class</code>, <code>camunda:delegateExpression</code>,
      <code>camunda:type</code> or <code>camunda:expression</code> is mandatory
    </td>
  </tr>
  <tr>
    <td></td>
    <td>
      The attribute <code>camunda:resultVariable</code> can only be used in combination with the
      <code>camunda:expression</code> attribute
    </td>
  </tr>
  <tr>
    <td></td>
    <td>
      The <code>camunda:exclusive</code> attribute is only evaluated if the attribute
      <code>camunda:asyncBefore</code> or <code>camunda:asyncAfter</code> is set to <code>true</code>
    </td>
  </tr>
  <tr>
    <td></td>
    <td>
      The attribute <code>camunda:topic</code> can only be used when the <code>camunda:type</code> attribute is set to <code>external</code>.
    </td>
  </tr>
  <tr>
    <td></td>
    <td>
      The attribute <code>camunda:taskPriority</code> can only be used when the <code>camunda:type</code> attribute is set to <code>external</code>.
    </td>
  </tr>
</table>


# Additional Resources

* [Tasks](http://camunda.org/bpmn/reference.html#activities-task) in the [BPMN Modeling Reference](http://camunda.org/bpmn/reference.html) section
* [How to call a Webservice from BPMN](http://www.bpm-guide.de/2010/12/09/how-to-call-a-webservice-from-bpmn/). Please note that this article is outdated. However, it is still valid regarding how you would call a Web Service using the process engine.
