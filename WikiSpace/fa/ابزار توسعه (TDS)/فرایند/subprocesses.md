Reusability and grouping.

[**Embedded Subprocess**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/subprocesses/embedded-subprocess.md)
Group a set of flow nodes into a scope.
[**Call Activity**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/subprocesses/call-activity.md)
Call a process from another process.
[**Event Subprocess**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/subprocesses/event-subprocess.md)
Event-driven subprocess.
[**Transaction Subprocess**](/apps/hmadmin/WikiSpace/wiki/dfg/ابزار+توسعه+%28TDS%29/فرایند/subprocesses/transaction-subprocess.md)
Model Business Transactions.