A fieldset can be used to create a title of an area of the form. This is useful to put inside Layout components or in between lots of related fields. This form component is display only and will not be saved to the api.


 ![](/trnwikistatic/Development_Tools_TDS/Form/LayoutComponents/002.png)

Fieldset Legend
Enter the legend that will appear for the fieldset.

Custom CSS Class
A custom CSS class to add to this component. You may add multiple class names separated by a space.
