The Column layout component can be used to split any area into two columns. Simply drag and drop the Column button onto the form and the area you drop it on will be split in two.


 ![](/trnwikistatic/Development_Tools_TDS/Form/LayoutComponents/001.png)
