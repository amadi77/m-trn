
The table component allows creating a table with columns and rows that allow adding additional components within it.

 ![](/trnwikistatic/Development_Tools_TDS/Form/LayoutComponents/004.png)

umber of Rows
The number of rows on the table. This can be adjusted at any time.

Number of Columns
The number of columns on the table. This can be adjusted at any time.

Custom CSS Class
A custom CSS class to add to this component. You may add multiple class names separated by a space.

Striped
Whether or not the table is striped for odd and even rows.

Bordered
Whether or not the table has a border set on it. (This can be changed by your own CSS as well)

Hover
Whether or not to add a hover class on rows when the mouse hovers over them.

Condensed
Whether or not to condense the size of each sell by removing padding.