In addition to Layout Components, which are detailed in the next section of this guide, you can marginally change the arrangement of the components on your Form within the Layout settings. Each component allows for marginal layout changes from top, bottom, left, and right.

To change the layout, simply input a margin amount in the Top, Right, Bottom, or Left field. Components will be arranged accordingly depending on what margin field you input to. Set margins must be a valid CSS measurement input like “10px” in order to render properly.


 ![](/trnwikistatic/Development_Tools_TDS/Form/FormComponents/043.png)
