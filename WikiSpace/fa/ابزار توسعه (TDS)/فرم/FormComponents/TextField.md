A textfield can be used for short and general text input. There are options to define input masks and validations, allowing users to mold information into desired formats.

 ![](/trnwikistatic/Development_Tools_TDS/Form/FormComponents/005.png)
