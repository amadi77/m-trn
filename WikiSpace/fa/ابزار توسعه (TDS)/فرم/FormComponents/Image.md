Images within Form.io are created on the Form using the File component, which is capable of uploading images.

Displaying the images, however, is done within the implementation of the application retrieving the data. As an example, here is an Angular directive which illustrates how an image can be loaded within a <img> tag within your application dynamically from the file component on the form.


              .center-loader {
                font-size: 2em;
                position: absolute;
                left: 50%;
                top: 50%;
                margin-top: -0.5em;
                margin-left: -0.5em;
              }

      <div photo-loader src="'https://myproject.form.io/user/submission/234234234'" field="'photo'" default="'/assets/images/profile.png'"></div>

      angular.module('myapp')
        .filter('trusted', function ($sce) {
          return function(url) {
            return $sce.trustAsResourceUrl(url);
          };
        })
        .directive('photoLoader', function() {
          return {
            restrict: 'A',
            scope: {
              src: '=',
              field: '=',
              default: '='
            },
            template: 
              '<div>' +
                '<i ng-if="photoLoading" style="color: white;" class="center-loader glyphicon glyphicon-refresh glyphicon-spin"></i>' +
                '<img src="{{ photo | trusted }}" style="width:100%" />' +
              '</div>',
            controller: ['$scope', 'Formio', '$element', function($scope, Formio, $element) {
              $scope.photoLoading = false;
              $scope.photo = $scope.default;
              var formio = new Formio($scope.src);
              formio.loadSubmission().then(function(user) {
                if (user.data[$scope.field] && user.data[$scope.field].length) {
                  $scope.photoLoading = true;
                  var photoElement = $element.find('img')[0];
                  if (photoElement) {
                    photoElement.onload = function() {
                      $scope.photoLoading = false;
                      setTimeout($scope.$apply.bind($scope), 10);
                    }; 
                  }
                  formio.downloadFile(user.data[$scope.field][0]).then(function (photo) {
                    $scope.photo = photo.url;
                    setTimeout($scope.$apply.bind($scope), 10);
                  });
                }
              });
            }]
          };
        });

  
  