To add a form component to a form, drag and drop the component from the left column into the desired location within the form.

Each new form starts with a submit button automatically added to it. This can be removed or edited as necessary.

 ![شکل شماره1](/trnwikistatic/Development_Tools_TDS/Form/FormComponents/001.png)
