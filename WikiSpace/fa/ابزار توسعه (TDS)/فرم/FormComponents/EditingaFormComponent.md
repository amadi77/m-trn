To edit a form component on a form, hover over the component and click the gear icon. You will then be presented with a settings form for the component.

 ![شکل شماره2](/trnwikistatic/Development_Tools_TDS/Form/FormComponents/002.png)
The settings for a form component are different for each component type.

