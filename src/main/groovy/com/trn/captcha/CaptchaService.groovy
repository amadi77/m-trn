package com.trn.captcha

import com.octo.captcha.service.CaptchaServiceException
import com.octo.captcha.service.image.DefaultManageableImageCaptchaService
import com.octo.captcha.service.image.ImageCaptchaService
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class CaptchaService {
    protected final static Logger logger = LoggerFactory.getLogger(CaptchaService.class)

    private static ImageCaptchaService instance = new DefaultManageableImageCaptchaService()

    static ImageCaptchaService getInstance() {
        return instance
    }

    static boolean validateResponseForID(String  moquiSessionToken, String response) {
        boolean isResponseCorrect = false
        try {

            isResponseCorrect = getInstance().validateResponseForID(moquiSessionToken, response)
        } catch (CaptchaServiceException e) {
            //should not happen, may be thrown if the id is not valid
            logger.warn("CaptchaServiceException: ${e}")
        }
        return isResponseCorrect
    }

}
