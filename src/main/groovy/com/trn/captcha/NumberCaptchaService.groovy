package com.trn.captcha

import com.octo.captcha.component.image.backgroundgenerator.BackgroundGenerator
import com.octo.captcha.component.image.backgroundgenerator.UniColorBackgroundGenerator
import com.octo.captcha.component.image.color.RandomRangeColorGenerator
import com.octo.captcha.component.image.fontgenerator.FontGenerator
import com.octo.captcha.component.image.fontgenerator.RandomFontGenerator
import com.octo.captcha.component.image.textpaster.RandomTextPaster
import com.octo.captcha.component.image.textpaster.TextPaster
import com.octo.captcha.component.image.wordtoimage.ComposedWordToImage
import com.octo.captcha.component.image.wordtoimage.WordToImage
import com.octo.captcha.component.word.wordgenerator.RandomWordGenerator
import com.octo.captcha.component.word.wordgenerator.WordGenerator
import com.octo.captcha.engine.image.ListImageCaptchaEngine
import com.octo.captcha.image.gimpy.GimpyFactory
import com.octo.captcha.service.CaptchaServiceException
import com.octo.captcha.service.captchastore.FastHashMapCaptchaStore
import com.octo.captcha.service.image.DefaultManageableImageCaptchaService
import com.octo.captcha.service.image.ImageCaptchaService
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.awt.*

class NumberCaptchaService {
    protected final static Logger logger = LoggerFactory.getLogger(NumberCaptchaService.class)
    private static ImageCaptchaService instance = new DefaultManageableImageCaptchaService(
            new FastHashMapCaptchaStore(),
            new MyImageCaptchaEngine(),
            180,
            100000,
            75000);

    static ImageCaptchaService getInstance() {
        return instance
    }

    static boolean validateResponseForID(String moquiSessionToken, String response) {
        boolean isResponseCorrect = false
        try {
            isResponseCorrect = getInstance().validateResponseForID(moquiSessionToken, response)
        } catch (CaptchaServiceException e) {
            //should not happen, may be thrown if the id is not valid
            logger.warn("CaptchaServiceException: ${e}")
        }
        return isResponseCorrect
    }

}

class MyImageCaptchaEngine extends ListImageCaptchaEngine {
    @Override
    protected void buildInitialFactories() {
        WordGenerator wgen = new RandomWordGenerator("0123456789");
        int[] red = [0, 255]
        int[] green = [0, 255]
        int[] blue = [0, 255]
        RandomRangeColorGenerator cgen = new RandomRangeColorGenerator(red, green, blue);

        TextPaster textPaster = new RandomTextPaster(new Integer(4), new Integer(5), cgen, Boolean.TRUE);

        BackgroundGenerator backgroundGenerator = new UniColorBackgroundGenerator(new Integer(240), new Integer(50), new Color(252, 252, 253));
        Font[] fontsList =
                [
                        new Font("Helvetica", Font.TYPE1_FONT, 10),
                        new Font("Arial", 0, 14),
                        new Font("Vardana", 0, 17)
                ]


        FontGenerator fontGenerator = new RandomFontGenerator(new Integer(18), new Integer(30), fontsList);
        WordToImage wordToImage = new ComposedWordToImage(fontGenerator, backgroundGenerator, textPaster);
        this.addFactory(new GimpyFactory(wgen, wordToImage));
    }
}
