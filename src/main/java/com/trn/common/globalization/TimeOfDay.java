package com.trn.common.globalization;

/**
 * Created aras Team.
 * User: ardeshir
 * Date: Dec 23, 2017
 * Time: 10:58:26 AM
 */
public enum TimeOfDay
{
    FirstOfDay,             //00
    Now,                    //01
    LastOfDay,              //02
    Noon,                   //03
    The2thSecondOfDay       //04
}
