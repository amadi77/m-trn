package com.trn.common.globalization.util;

/**
 * Created aras Team.
 * User: Reza
 * Date: Nov 13, 2010
 * Time: 10:09:37 AM
 */
public class NumberFormatter
{
    private static String[] parts = {"", "هزار", "میلیون", "میلیارد", "تریلیون"};
    private static String[] orders0 = {"", "یک", "دو", "سه", "چهار", "پنج", "شش", "هفت", "هشت", "نه"};
    private static String[] orders1 = {"", "", "بیست", "سی", "چهل", "پنجاه", "شصت", "هفتاد", "هشتاد", "نود"};
    private static String[] orders2 = {"", "يكصد", "دویست", "سیصد", "چهارصد", "پانصد", "ششصد", "هفتصد", "هشتصد", "نهصد"};
    private static String[] digits = {"ده", "یازده", "دوازده", "سیزده", "چهارده", "پانزده", "شانزده", "هفده", "هجده", "نوزده"};

    public static String NumberToPersianText(String input)
    {
        StringBuilder output = new StringBuilder();
        if (!input.equals(""))
        {
            String temp;
            int i = input.length() - 1;
            int j = 0;
            int part = 0;
            while (i >= 0)
            {
                temp = input.toCharArray()[i] + "";
                if ((i != 0) && (j == 0) & (input.toCharArray()[i - 1] == '1'))
                {
                    temp = input.substring(i - 1);
                    temp = TwoDigitToText(temp);
                    output.insert(0, temp);
                    if ((i != 0) & ((i - 1) != 0))
                        output.insert(0, " و ");
                    i = i - 2;
                    j++;
                }
                else
                {
                    temp = DigitToText(temp, j);
                    if (!temp.equals(""))
                    {
                        output.insert(0, temp);
                        if (i != 0)
                            output.insert(0, " و ");
                    }
                    i--;
                }
                if (j == 2)
                {
                    part++;
                    if (i != -1 && (!reverse(separate(input)).split(",")[part].equals("000")))
                        output.insert(0, " " + PartsName(part) + " ");
                    j = 0;
                }
                else
                    j++;
            }
        }
        return output.toString();
    }

    private static String PartsName(int part)
    {
        if (part > parts.length - 1)
            return "";
        return parts[part];
    }

    private static String DigitToText(String digit, int order)
    {
        int value = Integer.parseInt(digit);
        if (order == 0)
            if (value > orders0.length - 1)
                return "";
            else
                return orders0[value];
        else if (order == 1)
            if (value > orders1.length - 1)
                return "";
            else
                return orders1[value];
        else if (order == 2)
            if (value > orders2.length - 1)
                return "";
            else
                return orders2[value];
        return "";
    }

    private static String TwoDigitToText(String digit)
    {
        int value = Integer.parseInt(digit.substring(1).substring(0, 1));
        if (value > digits.length - 1)
            return "";
        else
            return digits[value];
    }

    public static String separate(String value)
    {
        String result = "";
        int counter = 1;
        for (int i = value.length(); i > 0; i--)
        {

            if ((counter > 3) && (counter % 3) == 1)
                result = "," + result;
            result = value.toCharArray()[i - 1] + result;
            counter++;
        }
        return result;
    }

    public static String reverse(String input)
    {
        String result = "";
        for (int i = 0; i < input.length(); i++)
            result = input.toCharArray()[i] + result;
        return result;
    }
}

