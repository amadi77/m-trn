/*
	PersianCalendar.java
	2003-09-24 14:56:36
	Copyright � Ghasem Kiani <ghasemkiani@yahoo.com>
	
	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*
	HISTORY:
	Version 2.1 2005-03-18:
		A bug was corrected. Calculation of Julian days was being done on 
		the timezone-dependent time, not on the universal time. Now, this 
		is corrected.
	Version 2.0 2005-02-21:
		Some of the functionality was exported to other classes to be 
		usable by the com.ghasemkiani.util.icu.PersianCalendar class 
		which is based on IBM's ICU4J.
		Calculation algorithm was rewritten and some bugs were fixed. 
		Specifically, two functions for modulo and quotient were 
		introduced that solve some of the problems that arose in years 
		before 475 A.H.
		Added a new read/write property: julianDay.
		Characters ARABIC LETTER YEH and ARABIC LETTER KAF were replaced 
		by ARABIC LETTER FARSI YEH and ARABIC LETTER KEHEH, respectively, 
		in the Persian names of months and week days. This was done in 
		accordance with the CLDR locale for the Persian language, though I 
		don't like this personally, because the characters for Arabic and 
		Persian scripts had better be identical as much as possible. 
		So that we can use Cp1256, Internet searches in Persian are more 
		effective, and there is much less confusion in general.
		Changed Amordad to Mordad, since the former, though more correct, 
		is seldom used today.
	Version 1.3 2003-12-12:
		Added accessor get methods for "properties" persianMonthName
		and persianWeekDayName. Corrected some errors in the 
		documentation.
	Version 1.2 2003-11-17:
		Converted Persian literals to Unicode escape sequences.
	Version 1.1 2003-10-23:
		Added Persian names for months and days of the week.
		Added Javadoc documentation for the API.
	Version 1.0 2003-09-25:
		Started the project.
*/

package com.trn.common.globalization;

//import java.util.Date;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static com.trn.common.globalization.PersianCalendarUtils.*;

//import com.pdr.globalization.DateFields;
//import com.pdr.globalization.PersianCalendarConstants;

/**
 * This class is a subclass of <code>java.util.GregorianCalendar</code>,
 * with the added functionality that it can set/get date in the Persian
 * calendar system.
 * <p/>
 * The algorithms for conversion between Persian and Gregorian calendar systems
 * are placed in <code>{@link PersianCalendarHelper}</code> class.
 *
 * @author <a href="mailto:ghasemkiani@yahoo.com">Ghasem Kiani</a>
 * @version 2.1
 */        

public class PersianCalendar extends GregorianCalendar implements PersianCalendarConstants
{
    private static String copyright = "Copyright \u00a9 2003-2005 Ghasem Kiani <ghasemkiani@yahoo.com>. All Rights Reserved.";
    // Julian day 0, 00:00:00 hours (midnight); milliseconds since 1970-01-01 00:00:00 UTC (Gregorian Calendar)
    private static final long JULIAN_EPOCH_MILLIS = -210866803200000L;
    private static final long ONE_DAY_MILLIS = 24L * 60L * 60L * 1000L;

    /**
     * Get the Julian day corresponding to the date of this calendar.
     *
     * @return the Julian day corresponding to the date of this calendar.
     * @since 2.0
     */
    public long getJulianDay()
    {
        return div(getTimeInMillis() + 12600000 - JULIAN_EPOCH_MILLIS, ONE_DAY_MILLIS);
    }

    /**
     * Set the date of this calendar to the specified Julian day.
     *
     * @param julianDay the desired Julian day to be set as the date of this calendar.
     * @since 2.0
     */
    public void setJulianDay(long julianDay)
    {
        setTimeInMillis(JULIAN_EPOCH_MILLIS + julianDay * ONE_DAY_MILLIS + mod(getTimeInMillis() - JULIAN_EPOCH_MILLIS, ONE_DAY_MILLIS));
    }

    /**
     * Sets the date of this calendar object to the specified
     * Persian date (year, month, and day fields)
     *
     * @param year  the Persian year.
     * @param month the Persian month (zero-based).
     * @param day   the Persian day of month.
     * @since 1.0
     */
    public void setDateFields(int year, int month, int day)
    {
        setDateFields(new DateFields(year, month, day));
    }

    /**
     * Sets the date of this calendar object to the specified
     * Persian date fields
     *
     * @param dateFields the Persian date fields.
     * @since 1.0
     */
    public void setDateFields(DateFields dateFields)
    {
        int y = dateFields.getYear();
        int m = dateFields.getMonth();
        int d = dateFields.getDay();
        setJulianDay(PersianCalendarHelper.pj(y > 0 ? y : y + 1, m, d));
    }

    /**
     * Retrieves the date of this calendar object as the
     * Persian date fields
     *
     * @return the date of this calendar as Persian date fields.
     * @since 1.0
     */
    public DateFields getDateFields()
    {
        long julianDay = getJulianDay();
        long r = PersianCalendarHelper.jp(julianDay);
        long y = y(r);
        int m = m(r);
        int d = d(r);
        return new DateFields((int) (y > 0 ? y : y - 1), (int) m, (int) d);
    }

    /**
     * Persian month names.
     *
     * @since 1.1
     */
    public static final String[] persianMonths =
            {
                    "فروردین",              // Farvardin
                    "اردیبهشت",             // Ordibehesht
                    "خرداد",                // Khordad
                    "تیر",                  // Tir
                    "مرداد",                // Mordad
                    "شهریور",               // Shahrivar
                    "مهر",                  // Mehr
                    "آبان",                 // Aban
                    "آذر",                  // Azar
                    "دی",                   // Dey
                    "بهمن",                 // Bahman
                    "اسفند"        //                  Esfand
            };
//    public static final String[] persianMonths =
//	{
//		"ÝÑæÑÏ?ä",             // Farvardin
//		"ÇÑÏ?ÈåÔÊ", // Ordibehesht
//		"ÎÑÏÇÏ",                         // Khordad
//		"Ê?Ñ",                                     // Tir
//		"ãÑÏÇÏ",                         // Mordad
//		"ÔåÑ?æÑ",                   // Shahrivar
//		"ãåÑ",                                     // Mehr
//		"ÂÈÇä",                               // Aban
//		"ÂÐÑ",                                     // Azar
//		"Ï?",                                           // Dey
//		"Èåãä",                               // Bahman
//		"ÇÓÝäÏ"                          // Esfand
//	};


    /**
     * Persian week day names.
     *
     * @since 1.1
     */
    public static final String[] persianWeekDays =
            {
                    "شنبه",                 // shanbeh
                    "یکشنبه",               // yek-shanbeh
                    "دوشنبه",               // do-shanbeh
                    "سه شنبه",              // seh-shanbeh
                    "چهارشنبه",             // chahar-shanbeh
                    "پنجشنبه",              // panj-shanbeh
                    "جمعه"    //                   jom'eh
            };
//    public static final String[] persianWeekDays =
//	{
//		"ÔäÈå",                         // shanbeh
//		"?˜ÔäÈå",       // yek-shanbeh
//		"ÏæÔäÈå",             // do-shanbeh
//		"ÓåÔäÈå",       // seh-shanbeh
//		"åÇÑÔäÈå", // chahar-shanbeh
//		"äÌÔäÈå", // panj-shanbeh
//		"ÂÏ?äå"                          // jom'eh
//	};


    /**
     * Gives the name of the specified Persian month.
     *
     * @param month the Persian month (zero-based).
     * @return the name of the specified Persian month in Persian.
     * @since 1.1
     */
    public static String getPersianMonthName(int month)
    {
        return persianMonths[month];
    }

    /**
     * Gives the name of the current Persian month for this calendar's date.
     *
     * @return the name of the current Persian month for this calendar's date in Persian.
     * @since 1.3
     */
    public String getPersianMonthName()
    {
        return getPersianMonthName(getDateFields().getMonth());
    }

    /**
     * Gives the Persian name of the specified day of week.
     *
     * @param weekDay the day of week (use symbolic constants in the <code>java.util.Calendar</code> class).
     * @return the name of the specified day of week in Persian.
     * @since 1.1
     */
    public static String getPersianWeekDayName(int weekDay)
    {
        switch (weekDay)
        {
            case SATURDAY:
                return persianWeekDays[0];
            case SUNDAY:
                return persianWeekDays[1];
            case MONDAY:
                return persianWeekDays[2];
            case TUESDAY:
                return persianWeekDays[3];
            case WEDNESDAY:
                return persianWeekDays[4];
            case THURSDAY:
                return persianWeekDays[5];
            case FRIDAY:
                return persianWeekDays[6];
        }
        return "";
    }

    /**
     * Gives the Persian name of the current day of the week for this
     * calendar's date.
     *
     * @return the name of the current day of week for this calendar's date in Persian.
     * @since 1.3
     */
    public String getPersianWeekDayName()
    {
        return getPersianWeekDayName(get(DAY_OF_WEEK));
    }

    public int get(int field)
    {
        switch (field)
        {
            case Calendar.DAY_OF_MONTH:
                return getDateFields().getDay();
            case Calendar.YEAR:
                return getDateFields().getYear();
            case Calendar.MONTH:
                return getDateFields().getMonth();
            default:
                return super.get(field);   
        }
    }

    public PersianCalendar()
    {
        initTimeZone();
//        this.setTimeInMillis(this.getTime().getTime() - this.getDaylight());
    }

    public PersianCalendar(Date date)
    {
        initTimeZone();
        this.setTime(date);
//        this.setTimeInMillis(this.getTime().getTime() + this.getDaylight());
    }

    private void initTimeZone()              
    {                                   
//        Calendar current = Calendar.getInstance();
//        Calendar start = Calendar.getInstance();
//        start.set(current.get(Calendar.YEAR), Calendar.MARCH, 20);
//        Calendar end = Calendar.getInstance();
//        end.set(current.get(Calendar.YEAR), Calendar.AUGUST, 21);
//        this.setTimeZone(new SimpleTimeZone(
//                12600000,
//                "Asia/Tehran",
//                start.get(Calendar.MONTH),
////                start.get(Calendar.DAY_OF_WEEK),
//                0,
//                start.get(Calendar.DAY_OF_WEEK_IN_MONTH),
//                SimpleTimeZone.WALL_TIME,
//                end.get(Calendar.MONTH),
////                end.get(Calendar.DAY_OF_WEEK),
//                0,
//                end.get(Calendar.DAY_OF_WEEK_IN_MONTH),
//                SimpleTimeZone.WALL_TIME,
//                3600000
//        ));
    }

    private long getDaylight()
    {
        long result = 0;
        if (this.get(Calendar.MONTH) < 6)
        {
            if (this.get(Calendar.MONTH) == 5 &&
                    this.get(Calendar.DAY_OF_MONTH) < 30 && this.get(Calendar.DAY_OF_MONTH) > 2)
            {
                result += 14400000;
            }
            else
            {
                result += 10800000;
            }
        }
        else
        {
            result += 10800000;
        }

        result += 1800000;
        return result;
    }
}
