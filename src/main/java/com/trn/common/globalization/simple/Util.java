package com.trn.common.globalization.simple;

import java.util.Calendar;

/**
 * Created aras Team.
 * User: Ardeshir
 * Date: Jan 18, 2010
 * Time: 4:05:33 PM
 */
public class Util
{                              
    public static double getDiff(String fromPersianDate, String toPersianDate, int field)
    {
        if (field == 2) return getDiffMonth(fromPersianDate, toPersianDate);
        PersianCalendar fromCalendar = new PersianCalendar(fromPersianDate);
        PersianCalendar toCalendar = new PersianCalendar(toPersianDate);
        double counter = 0;
        double leapYear = 0;
        double unLeapYear = 0;
        double leapMonth = 0;
        if (fromCalendar.before(toCalendar))
        {
            int firstDay = fromCalendar.getDay();
            int firstMonth = fromCalendar.getMonth();
            int lastMonth = fromCalendar.getMonth();
            int lastYear = fromCalendar.getYear();
            while (fromCalendar.before(toCalendar))
            {
                counter++;
                fromCalendar.add(Calendar.DAY_OF_MONTH, 1);
                if (lastMonth != fromCalendar.getMonth())
                {
                    if (lastMonth < 7)
                        leapMonth++;

                    lastMonth = fromCalendar.getMonth();
                }

                if (lastYear != fromCalendar.getYear())
                {
                    if (PersianCalendar.isLeep(lastYear))
                        leapYear++;
                    else
                        unLeapYear++;

                    lastYear = fromCalendar.getYear();
                }
            }
            if (PersianCalendar.isLeepMonth(firstMonth) && firstDay != toCalendar.getDay())
                leapMonth += 1;
        }
        else
            return 0;

        switch (field)
        {
            case 5:
            {
                return counter;
            }
            case 2:
            {
                counter++;
                return (counter - (counter < 30 ? 0 : leapMonth) + unLeapYear) / 30;
            }
            case 1:
            {
                return (counter - leapYear) / 365;
            }
        }
        return 0;
    }

    public static double getDiffMonth(String fromPersianDate, String toPersianDate)
    {
        PersianCalendar fromCalendar = new PersianCalendar(fromPersianDate);
        PersianCalendar toCalendar = new PersianCalendar(toPersianDate);
        double monthCounter = 0;
        int counter = 1;
        if (fromCalendar.before(toCalendar))
        {
            PersianCalendar tempFromCalendar = new PersianCalendar(fromPersianDate);
            while (tempFromCalendar.before(toCalendar))
            {
                tempFromCalendar = new PersianCalendar(fromPersianDate);
                tempFromCalendar.add(Calendar.MONTH, counter);
                if (tempFromCalendar.after(toCalendar))
                {
                  break;
                }
                else
                {
                    counter++;
                    monthCounter++;
                }
            }
            return monthCounter;
        }
        else
            return 0;
    }
}
