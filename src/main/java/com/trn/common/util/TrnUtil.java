package com.trn.common.util;

import com.ibm.icu.text.DateFormat;
import com.ibm.icu.util.Calendar;
import com.ibm.icu.util.ULocale;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author mansour_ashtari
 */
public class TrnUtil {

    public static String convertDateToJalali(String date, String dateFormat) {
        ULocale locale = new ULocale("fa_IR@calendar=persian");
        Calendar calendar = Calendar.getInstance(locale);
        calendar.setTimeInMillis(convertStrToDate(date, dateFormat).getTime());
        return DateFormat.getPatternInstance(DateFormat.YEAR_NUM_MONTH_DAY, locale).format(calendar);
    }

    public static String convertDateToJalali(Date date) {
        ULocale locale = new ULocale("fa_IR@calendar=persian");
        Calendar calendar = Calendar.getInstance(locale);
        calendar.setTimeInMillis(date.getTime());
        return DateFormat.getPatternInstance(DateFormat.YEAR_NUM_MONTH_DAY, locale).format(calendar);
    }

    private static Date convertStrToDate(String strDate, String dateFormat) {
        java.text.DateFormat format = new SimpleDateFormat(dateFormat);
        Date date = null;
        Date date1 = null;
        try {
            date = format.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
            java.text.DateFormat format1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            try {
                date1 = format1.parse(strDate);
            } catch (ParseException e2) {
                e2.printStackTrace();
            }
        }
        if (date == null) {
            date = date1;
        }
        return date;
    }

    public static Double convertCurrencyToNumber(String currency) {
        if (currency == null) {
            currency = "";
        }
        return Double.parseDouble(currency.length() > 0 ? currency.replace(",", "") : "0.0");
    }

    public static String converter(String enNumber) {
        char[] persianChars = {'٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩'};

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < enNumber.length(); i++) {
            if (Character.isDigit(enNumber.charAt(i))) {
                builder.append(persianChars[(int) (enNumber.charAt(i)) - 48]);
            } else {
                builder.append(enNumber.charAt(i));
            }
        }
        System.out.println("Number In Arabic : " +builder.toString() );
        return builder.toString();
    }
    public static Map<String,Object> convertEnglishNumberToPersian(Map<String,Object> vars) {
        for (Map.Entry<String, Object> entry : vars.entrySet()) {
            if (entry.getValue() instanceof Map) {
                System.out.println("Map object found, iterating again");
                convertEnglishNumberToPersian((Map<String, Object>) entry.getValue());
            } else if (entry.getValue() instanceof String){
                if(isInteger(entry.getValue().toString())) {
                    entry.setValue(converter(entry.getValue().toString()));
                }
                System.out.println("found end , value is:"+ entry.getValue());
            }
            else if(entry.getValue() instanceof List) {
                for (Object obj:(List)entry.getValue()) {
                    if (obj instanceof Map) {
                        convertEnglishNumberToPersian((Map<String, Object>) obj);
                    }else if(obj instanceof String) {
                        if(isInteger(obj.toString())) {
                            entry.setValue(converter(entry.getValue().toString()));
                        }
                    }
                }
            }
        }
        return vars;
    }
    public static boolean isInteger(String str) {
        if (str == null) {
            return false;
        }
        int length = str.length();
        if (length == 0) {
            return false;
        }
        int i = 0;
        if (str.charAt(0) == '-') {
            if (length == 1) {
                return false;
            }
            i = 1;
        }
        for (; i < length; i++) {
            char c = str.charAt(i);
            if (c < '0' || c > '9') {
                return false;
            }
        }
        return true;
    }
}
