package com.trn.common;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

/**
 * @author mansour_ashtari
 */
public class ReadExcel {


    private static List getDataListFromExcel(String filePath) throws IOException {
        String extension = FilenameUtils.getExtension(filePath);
        Workbook workbook;
        if (extension.equals("xls")) {
            workbook = new HSSFWorkbook(new FileInputStream(new File(filePath)));
        } else {
            workbook = new XSSFWorkbook(new FileInputStream(new File(filePath)));
        }
        return execute(workbook);
    }

    private static List getDataListFromExcel(FileItem excelFile) throws IOException {
        Workbook workbook;
        if (excelFile.getName().substring(excelFile.getName().lastIndexOf(".")).equals("xls")) {
            workbook = new HSSFWorkbook(excelFile.getInputStream());
        } else {
            workbook = new XSSFWorkbook(excelFile.getInputStream());
        }
        return execute(workbook);
    }


    private static List execute(Workbook workbook) {
        List dataList = new LinkedList();
        Sheet sheet = workbook.getSheetAt(0);
        Iterator rowIterator = sheet.iterator();

        LinkedHashMap innerMap;
        List listFields = new ArrayList<>();
        int rowindex = 0;
        while (rowIterator.hasNext()) {
            Row row = (Row) rowIterator.next();
            Iterator cellIterator = row.cellIterator();

            int colindex = 0;
            innerMap = new LinkedHashMap();
            while (cellIterator.hasNext()) {
                Cell cell = (Cell) cellIterator.next();
                if (rowindex == 0) {
                    listFields.add(cell.getStringCellValue());
                } else {
                    if (Cell.CELL_TYPE_NUMERIC == cell.getCellType()) {
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                    }
                    innerMap.put(listFields.get(colindex), cell.getStringCellValue());
                }
                colindex++;
            }
            if (rowindex != 0) {
                dataList.add(innerMap);
            }
            rowindex++;
        }

        return dataList;
    }
}