package com.trn.common;

import org.apache.commons.codec.binary.Base64;
import org.apache.poi.xwpf.usermodel.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTAbstractNum;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBookmark;

import java.io.*;
import java.math.BigInteger;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author mansour_ashtari
 */
public class WriteDoc {

    public static HashMap writeToDocx(String letterContent, String bookmarkName, String filePath) throws IOException {
        ClassLoader classLoader = new WriteDoc().getClass().getClassLoader();
        HashMap<String, Object> result = new HashMap<>();
        Document elementDocument = Jsoup.parse(letterContent);
        Elements elements = elementDocument.getElementsByTag("body");
        List<Map<String, Object>> convertedElements = extractData(elements);

        XWPFDocument document = new XWPFDocument(new FileInputStream(filePath));
        insertAtBookmark(document.getParagraphs().get(0), bookmarkName, convertedElements);
        String fileTempPath = classLoader.getResource("/").getPath() + UUID.randomUUID() + ".docx";
        File file = new File(fileTempPath);
        if (file.createNewFile()) {
            document.write(new FileOutputStream(file));
        }
        result.put("value", encodeFileToBase64Binary(file));
        file.delete();
        return result;
    }

    private static void insertAtBookmark(XWPFParagraph paragraph, String bookmarkName, List<Map<String, Object>> textElements) {
        List<CTBookmark> bookmarkList;
        Iterator<CTBookmark> bookmarkIter;
        CTBookmark bookmark;
        bookmarkList = paragraph.getCTP().getBookmarkStartList();
        bookmarkIter = bookmarkList.iterator();
        while (bookmarkIter.hasNext()) {
            bookmark = bookmarkIter.next();
            if (bookmark.getName().equals(bookmarkName)) {
                for (int j = 0; j < textElements.size(); j++) {
                    XWPFParagraph newParagraph = paragraph.getDocument().createParagraph();
                    newParagraph.setStyle("Heading1");
                    XWPFRun run = createRun(newParagraph, textElements.get(j));
                    if (textElements.get(j).containsKey("olTagClass")) {
                        newParagraph.setNumID(addListStyle(paragraph));
                    }
                    if (textElements.get(j).containsKey("ulTagClass")) {
                        newParagraph.setNumID(addListStyle(paragraph));
                        newParagraph.getCTP().getPPr().getNumPr().addNewIlvl().setVal(BigInteger.valueOf(1));
                    }
                    run.setText(textElements.get(j).get("value").toString());
                }
            }
        }

    }

    private static XWPFRun createRun(XWPFParagraph newParagraph, Map<String, Object> elementTextValue) {
        XWPFRun run = newParagraph.createRun();
        setAlignmentProperties(newParagraph, elementTextValue, "p");
        setFontProperties(run, elementTextValue);
        // olTag alignment
        if (elementTextValue.containsKey("olTagClass")) {
            setAlignmentProperties(newParagraph, elementTextValue, "ol");
        }
        if (elementTextValue.containsKey("ulTagClass")) {
            setAlignmentProperties(newParagraph, elementTextValue, "ul");
        }
        if (elementTextValue.containsKey("strongTag")) {
            run.setBold(true);
        }
        return run;
    }

    private static BigInteger addListStyle(XWPFParagraph paragraph) {

        CTAbstractNum cTAbstractNum = CTAbstractNum.Factory.newInstance();
        XWPFAbstractNum abstractNum = new XWPFAbstractNum(cTAbstractNum);
        XWPFNumbering numbering = paragraph.getDocument().createNumbering();
        BigInteger abstractNumID = numbering.addAbstractNum(abstractNum);
        return numbering.addNum(abstractNumID);
    }

    private static void setAlignmentProperties(XWPFParagraph paragraph, Map<String, Object> elementTextValue, String tagName) {
        if (elementTextValue.containsKey(tagName + "TagClass")) {
            if (elementTextValue.get(tagName + "TagClass").toString().contains("ql-align-justify")) {
                paragraph.setAlignment(ParagraphAlignment.BOTH);
            } else if (elementTextValue.get(tagName + "TagClass").toString().contains("ql-align-right")) {
                paragraph.setAlignment(ParagraphAlignment.RIGHT);
            } else if (elementTextValue.get(tagName + "TagClass").toString().contains("ql-align-left")) {
                paragraph.setAlignment(ParagraphAlignment.LEFT);
            } else if (elementTextValue.get(tagName + "TagClass").toString().contains("ql-align-center")) {
                paragraph.setAlignment(ParagraphAlignment.CENTER);
            }
        }
    }

    private static void setFontProperties(XWPFRun run, Map<String, Object> elementTextValue) {
        setFontSize(run, elementTextValue);
        setFontFamily(run, elementTextValue);
    }

    private static void setFontSize(XWPFRun run, Map<String, Object> elementTextValue) {
        String fontSize = "normal";
        if (elementTextValue.containsKey("spanTagClass")) {
            if (!elementTextValue.get("spanTagClass").toString().equals("")) {
                fontSize = patternMatcher(fontSize, "ql-size-", elementTextValue, "spanTagClass");
            }
        } else if (elementTextValue.containsKey("strongTagClass")) {
            if (!elementTextValue.get("strongTagClass").toString().equals("")) {
                fontSize = patternMatcher(fontSize, "ql-size-", elementTextValue, "strongTagClass");
            }
        }
        if (fontSize.equals("normal")) {
            run.setFontSize(10);
        } else if (fontSize.equals("large")) {
            run.setFontSize(15);
        } else if (fontSize.equals("huge")) {
            run.setFontSize(25);
        }
    }

    private static void setFontFamily(XWPFRun run, Map<String, Object> elementTextValue) {
        String fontFamily = "serif";
        if (elementTextValue.containsKey("spanTagClass")) {
            if (!elementTextValue.get("spanTagClass").toString().equals("")) {
                fontFamily = patternMatcher(fontFamily, "ql-font-", elementTextValue, "spanTagClass");
            }
        } else if (elementTextValue.containsKey("strongTagClass")) {
            if (!elementTextValue.get("strongTagClass").toString().equals("")) {
                fontFamily = patternMatcher(fontFamily, "ql-font-", elementTextValue, "strongTagClass");
            }
        }
        if (fontFamily.equals("serif")) {
            run.setFontFamily("B Zar");
        } else if (fontFamily.equals("monospace")) {
            run.setFontFamily("IranNastaliq");
        } else {
            run.setFontFamily("B Zar");
        }

    }

    private static String patternMatcher(String input, String regx, Map<String, Object> elementTextValue, String tagName) {
        Pattern pattern = Pattern.compile(regx + "(.*?) ");
        Pattern pattern2 = Pattern.compile(regx + "(.*)");
        Matcher matcher = pattern.matcher(elementTextValue.get(tagName).toString());
        Matcher matcher2 = pattern2.matcher(elementTextValue.get(tagName).toString());
        if (matcher.find()) {
            input = matcher.group(1);
        } else if (matcher2.find()) {
            input = matcher2.group(1);
        }
        return input;
    }

    private static List<Map<String, Object>> extractData(Elements elements) {
        List<Map<String, Object>> extractedTagData = new LinkedList<>();
        extractedTagData.addAll(extractTags(elements, "p"));
        if (elements.select("ol").size() > 0) {
            extractedTagData.addAll(extractTags(elements, "ol"));
        }
        if (elements.select("ul").size() > 0) {
            extractedTagData.addAll(extractTags(elements, "ul"));
        }
        System.out.println(extractedTagData);
        return extractedTagData;
    }

    private static List<Map<String, Object>> extractTags(Elements elements, String tagName) {
        List<Map<String, Object>> extractedTagData = new LinkedList<>();
        Map<String, Object> pTagInnerMap;
        for (Element element : elements.select(tagName)) {
            for (int i = 0; i < element.childNodeSize(); i++) {
                pTagInnerMap = new LinkedHashMap<>();
                if (element.child(i).select("span").hasAttr("class")) {
                    pTagInnerMap.put("spanTagClass", element.child(i).select("span").attr("class"));
                }
                if (element.child(i).select("strong").size() > 0) {
                    pTagInnerMap.put("strongTag", "bold");
                    if (element.child(i).select("strong").hasAttr("class")) {
                        pTagInnerMap.put("strongTagClass", element.child(i).select("strong").attr("class"));
                    }
                }
                if (element.child(i).hasAttr("class")) {
                    pTagInnerMap.put(tagName + "TagClass", element.child(i).attr("class"));
                }
                if (element.child(i).hasText()) {
                    pTagInnerMap.put("value", element.child(i).text());
                }
                if (pTagInnerMap.size() > 0) {
                    extractedTagData.add(pTagInnerMap);
                }
            }

        }
        System.out.println(extractedTagData);
        return extractedTagData;
    }

    private static String encodeFileToBase64Binary(File file) throws IOException {
        byte[] bytes = loadFile(file);
        byte[] encoded = Base64.encodeBase64(bytes);
        return new String(encoded);
    }

    private static byte[] loadFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);
        long length = file.length();
        byte[] bytes = new byte[(int) length];

        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
                && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }
        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " + file.getName());
        }
        is.close();
        return bytes;
    }
}
