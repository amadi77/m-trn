package com.trn.common.kksk;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FindUserByUsernameFromKksk {

    public static void findUserByUsernameFromKksk(List usernameList, String fileUrl) {
        InputStream fis = null;

        try {
            fileUrl = fileUrl.replace("\\", "/");
            System.out.println("FILE_PATHFILE_PATHFILE_PATHFILE_PATH" + fileUrl);
            if (fileUrl.startsWith("file:///"))
                fileUrl = fileUrl.replace("file:///", "");
            if (fileUrl.startsWith("file://"))
                fileUrl = fileUrl.replace("file://", "");
            fis = new FileInputStream(fileUrl);
            System.out.println("FILE_PATHFILE_PATHFILE_PATHFILE_PATH2" + fileUrl);
            // Using XSSF for xlsx format, for xls use HSSF
            Workbook workbook = new XSSFWorkbook(fis);
            InputModel inputModel = new InputModel();
            int numberOfSheets = workbook.getNumberOfSheets();

            for (int i = 0; i < numberOfSheets; i++) {
                Sheet sheet = workbook.getSheetAt(i);
                Iterator rowIterator = sheet.iterator();

                //iterating over each row
                while (rowIterator.hasNext()) {
                    ArrayList<String> cellValueList = new ArrayList<String>();
                    Row row = (Row) rowIterator.next();
                    Iterator cellIterator = row.cellIterator();

                    //Iterating over each cell (column wise)  in a particular row.
                    if (row.getRowNum() != 0) {
                        if (row.getCell(0) != null) {
                            while (cellIterator.hasNext()) {
                                Cell cell = (Cell) cellIterator.next();
                                if (cell.getColumnIndex() == 0) {
                                    inputModel.setNid(String.valueOf(cell.getStringCellValue()).trim());
                                    for (Object anUsernameList : usernameList) {

                                        if (anUsernameList.equals(String.valueOf(cell.getStringCellValue()).trim())) {
                                            inputModel.setStatus("یافت شد");
                                            break;
                                        } else {
                                            inputModel.setStatus("یافت نشد");
                                        }
                                    }
                                } else if (cell.getColumnIndex() == 1) {
                                    cell.setCellValue(inputModel.getStatus());
                                }

                            }
                        }else {
                            break;
                        }
                    }
                }
            }
            FileOutputStream fos = new FileOutputStream(fileUrl);
            workbook.write(fos);
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalArgumentException(e);
        }
    }
}

