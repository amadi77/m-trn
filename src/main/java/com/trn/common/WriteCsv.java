package com.trn.common;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class WriteCsv {

	private static String write(ArrayList<HashMap<String, Object>> vars) {
		StringBuilder sb;
		sb = new StringBuilder();
		// write column header names
		Iterator<Map.Entry<String, Object>> itHeader = vars.get(0).entrySet().iterator();
		while (itHeader.hasNext()) {
			Map.Entry<String, Object> pairHeader = itHeader.next();
			sb.append((pairHeader.getKey()+"").replaceAll(","," "));
			if(itHeader.hasNext()){
				sb.append(",");
			}else{
				sb.append("\r\n");
			}
		}
		for (int i = 0; i < vars.size(); i++) {
			Iterator<Map.Entry<String, Object>> it = vars.get(i).entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<String, Object> pair = it.next();
				sb.append((""+pair.getValue()).replaceAll(","," "));
				if(it.hasNext()) {
					sb.append(",");
				}else{
					sb.append("\r\n");
				}
			}
		}
		return String.valueOf(sb);
	}
}
