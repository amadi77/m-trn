package com.trn.common.screen;

import freemarker.template.*;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author mansour-ashtari
 */
public class FreeMarkerEngine {

    private static Configuration configuration = new Configuration();
    public FreeMarkerEngine(){
    }

    public static String process(String templateName, Map vars, String templateDirectory) {
        try {
            configuration.setDirectoryForTemplateLoading(new File(templateDirectory));
            configuration.setObjectWrapper(new DefaultObjectWrapper());
            configuration.setDefaultEncoding("UTF-8");
            configuration.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
            configuration.setIncompatibleImprovements(new Version(2, 3, 20));
            Template template = configuration.getTemplate(templateName);
            StringWriter stringWriter = new StringWriter();
            template.process(vars, stringWriter);

            return stringWriter.toString();

        } catch (IOException | TemplateException e) {
            throw new RuntimeException(e);
        }
    }

}
