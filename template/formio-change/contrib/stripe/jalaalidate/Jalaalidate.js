/* globals Stripe */
import _ from 'lodash';
import Validator from '../../../components/Validator';
import BaseComponent from '../../../components/base/Base';
import Formio from '../../../Formio';

/**
 * This is the JalaalidateComponent class.
 */
export default class JalaalidateComponent extends BaseComponent {
  constructor(component, options, data) {
    super(component, options, data);
  }

  elementInfo() {
    const info = super.elementInfo();
    info.type = 'input';
    info.attr.type = 'text';
    info.changeEvent = 'input';
    if (info.component.required===true) {
        info.attr.required = 'required';
    }
    if (info.component.disabled===true) {
        info.attr.required = 'required';
    }
    if (info.component.time===true||info.component.withTime===true) {
      info.attr.class = 'persian-datepicker-time ';
    }
    else {
      info.attr.class = 'persian-datepicker ';
    }

    if (info.component.responseType==='unix') {
      info.attr.class+='persian-datepicker-unix hidden';
    }
    else if (info.component.responseType==='iso') {
      info.attr.class+='persian-datepicker-iso hidden';
    }
    else if (info.component.responseType==='jalaali') {
      info.attr.class+='persian-datepicker-jalaali form-control';
    }
    else {
      info.attr.class+='persian-datepicker-iso hidden';
    }
    return info;
  }

  get emptyValue() {
    return '';
  }
}

if (typeof global === 'object' && global.Formio && global.Formio.registerComponent) {
  global.Formio.registerComponent('jalaalidate', JalaalidateComponent);
}
