import Harness from '../../../../test/harness';
import Jalaalidate from './Jalaalidate';

import {
  comp1
} from './fixtures';

describe('Jalaalidate Component', () => {
  it('Should build an jalaalidate component', (done) => {
    Harness.testCreate(JalaalidateComponent, comp1).then(() => {
      done();
    });
  });
});
