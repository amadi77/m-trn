export default {
  'input': true,
  'tableView': true,
  'label': 'jalaalidate',
  'key': 'jalaalidate',
  'placeholder': '',
  'multiple': false,
  'protected': false,
  'clearOnHide': true,
  'unique': false,
  'persistent': true,
  'action': 'button',
  'validate': {
    'required': true
  },
  'type': 'jalaalidate',
  'tags': [

  ],
  'conditional': {
    'show': '',
    'when': null,
    'eq': ''
  }
};
