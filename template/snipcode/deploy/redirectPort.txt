sudo yum install iptables-services
* Add Rule
sudo iptables -A INPUT -i eth0 -p tcp --dport 80 -j ACCEPT
sudo iptables -A INPUT -i eth0 -p tcp --dport 8040 -j ACCEPT
sudo iptables -A PREROUTING -t nat -i eth0 -p tcp --dport 80 -j REDIRECT --to-port 8040
sudo iptables-save
sudo iptables-restart

* Delete Rule
sudo iptables -t nat -D PREROUTING -p tcp --dport 80 -j REDIRECT --to 8040
sudo iptables-save
sudo iptables-restart

****************OR***************

sudo firewall-cmd --permanent --zone=public --add-forward-port=port=80:proto=tcp:toport=8040
