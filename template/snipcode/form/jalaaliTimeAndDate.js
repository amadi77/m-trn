var comp =
{
    "type": "jalaali",
    "conditional": {
    "show": "",
        "when": null,
        "eq": ""
    },
    "key": "estimatedStartDate",
    "persistent": true,
    "disabled": false,
    "validate": {"required":true},
    "label": "تاریخ شروع"
}
;
//these are hidden components
value = new persianDate(new Date(row.eventDate)).format("YYYY/MM/DD HH:mm");