var workEffortTypeEnumId = data.workEffortTypeEnumId;
var rootWorkEffortId = data.rootWorkEffortId;
var parentWorkEffortId = data.parentWorkEffortId;
var token = null;var host = window.location.origin+"/rest/s1/";
var url = host + "mantle/workEfforts?pageSize=10000";
if(workEffortTypeEnumId) url+='&workEffortTypeEnumId='+workEffortTypeEnumId;
if(rootWorkEffortId) url+='&rootWorkEffortId='+rootWorkEffortId;
if(parentWorkEffortId) url+='&parentWorkEffortId='+parentWorkEffortId;
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function () { if (this.readyState == 4 && this.status == 200) values = JSON.parse(xhttp.responseText)};
xhttp.open("GET", url, false);
if (token) {xhttp.setRequestHeader('Authorization', token);} else {xhttp.withCredentials = true;}
xhttp.send();