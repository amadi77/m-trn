var token = null;
var dimensionTypeId = data.dimensionTypeId;
var uomTypeEnumId = data.uomTypeEnumId;
var orderByField = null;
var pageSize = 200;
var host = window.location.origin;
var url = host+ "/rest/s1/mantle/products/dimensionTypes";
if (pageSize) url += "?pageSize=" + pageSize;
if (dimensionTypeId) url += "&dimensionTypeId=" + dimensionTypeId;
if (uomTypeEnumId) url += "&uomTypeEnumId=" + uomTypeEnumId;
if (orderByField) url += "&orderByField=" + orderByField;
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function () { if (this.readyState == 4 && this.status == 200) values = JSON.parse(xhttp.responseText);};
xhttp.open("GET", url, false);
if (token) {xhttp.setRequestHeader('Authorization', token);} else {xhttp.withCredentials = true;}
xhttp.send();