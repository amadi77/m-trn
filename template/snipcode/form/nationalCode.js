var allDigitEqual = ["0000000000", "1111111111", "2222222222", "3333333333", "4444444444", "5555555555", "6666666666", "7777777777", "8888888888", "9999999999"];
if (allDigitEqual.indexOf(input)) {
    valid = 'کد ملی اشتباه است';
}
if (input.length != 10) {
    valid = 'کد ملی اشتباه است';
}
if (input.length == 10 && !allDigitEqual.indexOf(input)) {
    var chArray = input.split('');
    var num0 = parseInt(chArray[0].toString()) * 10;
    var num2 = parseInt(chArray[1].toString()) * 9;
    var num3 = parseInt(chArray[2].toString()) * 8;
    var num4 = parseInt(chArray[3].toString()) * 7;
    var num5 = parseInt(chArray[4].toString()) * 6;
    var num6 = parseInt(chArray[5].toString()) * 5;
    var num7 = parseInt(chArray[6].toString()) * 4;
    var num8 = parseInt(chArray[7].toString()) * 3;
    var num9 = parseInt(chArray[8].toString()) * 2;
    var a = parseInt(chArray[9].toString());
    var b = (((((((num0 + num2) + num3) + num4) + num5) + num6) + num7) + num8) + num9;
    var c = b % 11;
    valid = (((c < 2) && (a === c)) || ((c >= 2) && ((11 - c) === a))) ? true : 'کد ملی اشتباه است';
}