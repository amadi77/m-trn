var token = null;
var orderByField = "contactMechName";
var contactMechTypeEnumId = "CmtPostalAddress";
// var host = data._coreHostRemote + "/rest/s1/";
var host = window.location.origin + "/rest/s1/";
var url = host + "mantle/parties/contactMechs/purposes?pageNoLimit=true";
if (contactMechTypeEnumId) url += "&contactMechTypeEnumId=" + contactMechTypeEnumId;
if (orderByField) url += "&orderByField=" + orderByField;
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function () { if (this.readyState == 4 && this.status == 200) values = JSON.parse(xhttp.responseText);};
xhttp.open("GET", url, false);
if (token) {xhttp.setRequestHeader('Authorization', token);} else {xhttp.withCredentials = true;}
xhttp.send();
