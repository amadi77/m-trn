var token = null;
var orderByField = "statusId";

var assetTypeEnumId= data.assetTypeEnumId;
var facilityId= data.facilityId;
var pageSize = 10000;
var host = window.location.origin+"/rest/s1/";
var url = host +  "mantle/assets?pageNoLimit=true";
if(pageSize)url +="&pageSize="+pageSize;
if (assetTypeEnumId) url += "&assetTypeEnumId=" + assetTypeEnumId;
if (facilityId) url += "&facilityId=" + facilityId;
if (orderByField) url += "&orderByField=" + orderByField;
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function () { if (this.readyState == 4 && this.status == 200) values = JSON.parse(xhttp.responseText);};
xhttp.open("GET", url, false);
if (token) {xhttp.setRequestHeader('Authorization', token);} else {xhttp.withCredentials = true;}
xhttp.send();