var token =null;//data._basicToken
var host =window.location.origin;
var url = host + "/rest/s1/trn/common/export/pdf";
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200){
        var hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:application/pdf;base64,' + JSON.parse(xhttp.responseText).res;
        hiddenElement.target = '_blank';
        hiddenElement.download = 'trn-export.pdf';
        hiddenElement.click();
    }
};
xhttp.open("GET", url, false);
if (token) {xhttp.setRequestHeader('Authorization', token);} else {xhttp.withCredentials = true;}
xhttp.setRequestHeader('Authorization','Basic ' + token);
xhttp.setRequestHeader('Content-Type','application/json');
xhttp.setRequestHeader('Accept','application/json');
xhttp.send();