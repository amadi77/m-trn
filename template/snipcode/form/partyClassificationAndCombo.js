var token = null;
var parentClassificationId = data.parentClassificationId;
var classificationTypeEnumId = data.classificationTypeEnumId;
var orderByField = null;
var pageSize = 200;
var host = window.location.origin;
var url = host+ "/rest/s1/mantle/parties/classification";
if (pageSize) url += "?pageSize=" + pageSize;
if (classificationTypeEnumId) url += "&classificationTypeEnumId=" + classificationTypeEnumId;
if (parentClassificationId) url += "&parentClassificationId=" + parentClassificationId;
if (orderByField) url += "&orderByField=" + orderByField;
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function () { if (this.readyState == 4 && this.status == 200) values = JSON.parse(xhttp.responseText);};
xhttp.open("GET", url, false);
if (token) {xhttp.setRequestHeader('Authorization', token);} else {xhttp.withCredentials = true;}
xhttp.send();