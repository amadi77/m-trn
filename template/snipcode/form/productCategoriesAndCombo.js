var token = null;
var orderByField = "productCategoryId";
var productCategoryTypeEnumId = "PctMaterials";
// var host = data._coreHostRemote + "/rest/s1/";
var host = window.location.origin + "/rest/s1/";
var url = host + "mantle/products/categories?pagesize=10000";
if (productCategoryTypeEnumId) url += "&productCategoryTypeEnumId=" + productCategoryTypeEnumId;
if (orderByField) url += "&orderByField=" + orderByField;
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function () { if (this.readyState == 4 && this.status == 200) values = JSON.parse(xhttp.responseText);};
xhttp.open("GET", url, false);
if (token) {xhttp.setRequestHeader('Authorization', token);} else {xhttp.withCredentials = true;}
xhttp.send();
