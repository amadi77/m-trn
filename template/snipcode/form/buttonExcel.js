var token = null;//data._basicToken
var host = window.location.origin;
var url = host + "/rest/s1/trn/common/export/csv";
var xhttp = new XMLHttpRequest();
var data = [];
xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
        var hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:application/csv;base64,' + JSON.parse(xhttp.responseText).res;
        hiddenElement.target = '_blank';
        hiddenElement.download = 'trn-export.csv';
        hiddenElement.click();
    }
};
xhttp.open("POST", url, false);
if (token) {
    xhttp.setRequestHeader('Authorization', token);
} else {
    xhttp.withCredentials = true;
}
xhttp.setRequestHeader('Authorization', 'Basic ' + token);
xhttp.setRequestHeader('Content-Type', 'application/json');
xhttp.setRequestHeader('Accept', 'application/json');
xhttp.send(JSON.stringify(data));