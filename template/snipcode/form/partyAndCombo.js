var token = null;
var partyTypeEnumId = data.partyTypeEnumId;
var roleTypeId = data.roleTypeId;
var username = data.username;
var combinedName = data.combinedName;
var organizationName = data.organizationName;
var firstName = data.firstName;
var lastName = data.lastName;
var emailAddress = data.emailAddress;

var host = window.location.origin+"/rest/s1/";
var url = host + "mantle/parties?pageSize=10000";var xhttp = new XMLHttpRequest();
if(partyTypeEnumId) url+='&partyTypeEnumId='+partyTypeEnumId;
if(roleTypeId) url+='&roleTypeId='+roleTypeId;
if(username) url+='&username='+username;
if(combinedName) url+='&combinedName='+combinedName;
if(organizationName) url+='&organizationName='+organizationName;
if(firstName) url+='&firstName='+firstName;
if(lastName) url+='&lastName='+lastName;
if(emailAddress) url+='&emailAddress='+emailAddress;
xhttp.onreadystatechange = function () { if (this.readyState == 4 && this.status == 200) values = JSON.parse(xhttp.responseText).partyIdNameList};
xhttp.open("GET", url, false);
if (token) {xhttp.setRequestHeader('Authorization', token);} else {xhttp.withCredentials = true;}
xhttp.send();