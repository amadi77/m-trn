var token = null;
var productFeatureId = data.productFeatureId;
var productFeatureTypeEnumId = data.productFeatureTypeEnumId;
var orderByField = null;
var pageSize = 200;
var host = window.location.origin;
var url = host+ "/rest/s1/mantle/products/features";
if (pageSize) url += "?pageSize=" + pageSize;
if (productFeatureId) url += "&productFeatureId=" + productFeatureId;
if (productFeatureTypeEnumId) url += "&productFeatureTypeEnumId=" + productFeatureTypeEnumId;
if (orderByField) url += "&orderByField=" + orderByField;
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function () { if (this.readyState == 4 && this.status == 200) values = JSON.parse(xhttp.responseText);};
xhttp.open("GET", url, false);
if (token) {xhttp.setRequestHeader('Authorization', token);} else {xhttp.withCredentials = true;}
xhttp.send();a