var token = null;
var orderByField = "geoName";
var geoTypeEnumId = "GEOT_CITY";
var geoId = data.province;
var geoAssocTypeEnumId = null;
// var host = data._coreHostRemote + "/rest/s1/";
var host = window.location.origin + "/rest/s1/";
var url = host + ( (!geoId) ? "moqui/basic/geos?pageNoLimit=true&pageSize=1000" : "moqui/basic/geos/"+geoId+"/regions?pageNoLimit=true&pageSize=1000");
if (geoTypeEnumId) url += "&geoTypeEnumId=" + geoTypeEnumId;
if (geoAssocTypeEnumId  && geoId) url += "&geoAssocTypeEnumId=" + geoAssocTypeEnumId;
if (orderByField  && !geoId) url += "&orderByField=" + orderByField;
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function () { if (this.readyState == 4 && this.status == 200) values = JSON.parse(xhttp.responseText);};
xhttp.open("GET", url, false);
if (token) {xhttp.setRequestHeader('Authorization', token);} else {xhttp.withCredentials = true;}
xhttp.send();