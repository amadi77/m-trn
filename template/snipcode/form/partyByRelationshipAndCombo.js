var token = null;
var relationshipTypeEnumId = "PrtOrgRollup";
var fromPartyId= data.fromPartyId;
var fromRoleTypeId= data.fromRoleTypeId;
var toRoleTypeId= data.toRoleTypeId;
var toPartyId= data.toPartyId;


var host = window.location.origin;
var url = host+ "/rest/s1/mcls/party/byRelationship";
url += "?relationshipTypeEnumId=" + relationshipTypeEnumId;
if (fromPartyId) url += "&fromPartyId=" + fromPartyId;
if (fromRoleTypeId) url += "&fromRoleTypeId=" + fromRoleTypeId;
if (toRoleTypeId) url += "&toRoleTypeId=" + toRoleTypeId;
if (toPartyId) url += "&toPartyId=" + toPartyId;

var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function () { if (this.readyState == 4 && this.status == 200) values = JSON.parse(xhttp.responseText).outList;};
xhttp.open("GET", url, false);
if (token) {xhttp.setRequestHeader('Authorization', token);} else {xhttp.withCredentials = true;}
xhttp.send();