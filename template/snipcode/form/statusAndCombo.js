var token = null;
var statusId = data.statusId;
var statusTypeId = data.statusTypeId;
var orderByField = null;
var pageSize = 200;
var host = window.location.origin;
var url = host+ "/rest/s1/moqui/basic/statuses";
if (pageSize) url += "?pageSize=" + pageSize;
if (statusId) url += "&statusId=" + statusId;
if (statusTypeId) url += "&statusTypeId=" + statusTypeId;
if (orderByField) url += "&orderByField=" + orderByField;
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function () { if (this.readyState == 4 && this.status == 200) values = JSON.parse(xhttp.responseText);};
xhttp.open("GET", url, false);
if (token) {xhttp.setRequestHeader('Authorization', token);} else {xhttp.withCredentials = true;}
xhttp.send();