<?xml version="1.0" encoding="UTF-8"?>
<!-- Trn License -->
<#if ${rootDefaultMenuTitle}>
<screen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://moqui.org/xsd/xml-screen-2.1.xsd"
        default-menu-title="${rootDefaultMenuTitle}" menu-image="${rootMenuImage}" menu-image-type="${rootMenuImageType}"  >
    <#else>
<screen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://moqui.org/xsd/xml-screen-2.1.xsd"
        menu-image="${rootMenuImage}" menu-image-type="${rootMenuImageType}">
</#if>
    <#if ${appRoot}>
    <always-actions>
        ${rootAlwaysActionCode}
    </always-actions>
    </#if>
    <#if ${rootAlwaysUseFullPath}>
    <subscreens default-item="${rootDefaultItem}" always-use-full-path="${rootAlwaysUseFullPath}"/>
    <#if ${rootSubScreenItems}
    <#list rootSubScreenItems as itm>
    <#if ${itm.menuInclude} >
    <subscreens-item name="${itm.name}" location="${itm.location}"
    menu-include="${itm.menuInclude}"/>
    <#else>
    <subscreens-item name="${itm.name}" location="${itm.location}"
                     menu-index="${itm.menuIndex}"/>
    </#if>
    </#list>

    </#if>
    <#else>
    <subscreens default-item="${rootDefaultItem}" />
    </#if>
    <#if ${rootPreAction}>
    <pre-actions>${rootPreActionCode}</pre-actions>
    </#if>
    <widgets>
        <#if ${rootSubscreensPanel}>
        <subscreens-panel id="${rootSubScreenId}" type="${rootType}" title="${subScreenPanelTitle}"/>
        <#else>
        <subscreens-active id="${rootSubScreenId}"/>
        </#if>
    </widgets>
</screen>
