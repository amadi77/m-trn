<?xml version="1.0" encoding="UTF-8"?>
<!-- This software is in Trn Licence -->
<screen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://moqui.org/xsd/xml-screen-2.1.xsd"
        default-menu-title="${dashboardMenuTitle}" menu-image="${dashboardMenuImage}" menu-image-type="${dashboardMenuImageType}" default-menu-index="${dashboardDefaultMenuIndex}" >

    <#if ${dashboardTransition}>
    <transition name="transitionName" begin-transaction="false">
        <actions>
            <script></script>
            <message></message>
        </actions>
        <default-response url="."/>
    </transition>
    </#if>

    <#if ${dashboardAction}>
    <actions>

    </actions>
    </#if>

    <widgets>
        <container >
            <container-row>
                <!--<row-col md="3" style="text-center">-->
                <!--<container style="tasklist-panel-item">-->
                <!--<image url="/taskliststatic/images/icon/10.png" url-type="screen" width="150"/>-->
                <!--<label type="h3" text="process window" />-->
                <!--<label type="p" text="List of process that you can start" />-->
                <!--<link url="../processWindow" text="Show" link-type="anchor-button" btn-type="success" style="btn-block" />-->
                <!--</container>-->
                <!--</row-col>-->
                <row-col md="4" style="text-center">
                    <container style="tasklist-panel-item">
                        <image url="/taskliststatic/images/tasklist.png" url-type="screen" width="150"/>
                        <label type="h3" text="Tasklist Dashboard" />
                        <label type="p" text="All Assign Task" />
                        <link url="../dashboard" text="Show" link-type="anchor-button" btn-type="success" style="btn-block" />
                    </container>
                </row-col>
                <row-col md="4" style="text-center">
                    <container style="tasklist-panel-item">
                        <image url="/taskliststatic/images/sendBox.jpg" url-type="screen" width="150"/>
                        <label type="h3" text="Sent Box" />
                        <label type="p" text="All Tasks you've been submitted" />
                        <link url="../sentBox" text="Show" link-type="anchor-button" btn-type="success" style="btn-block" />
                    </container>
                </row-col>
                <row-col md="4" style="text-center">
                    <container style="tasklist-panel-item">
                        <image url="/taskliststatic/images/trace.jpg" url-type="screen" width="150"/>
                        <label type="h3" text="trace" />
                        <label type="p" text="Trace the Process started" />
                        <link url="../processTrace" text="Show" link-type="anchor-button" btn-type="success" style="btn-block" />
                    </container>
                </row-col>
            </container-row>
        </container>
    </widgets>
</screen>
