<script>
    $(function() {
       if("${ec.getUser().getTimeZone().toZoneId().toString()}"=="Asia/Tehran"){
            $(".date-time-view, .Timestamp").html(function(){
                return (($(this).html()!=""&&$(this).html()!="null") ? (new persianDate(new Date($(this).html())).format("YYYY/MM/DD HH:mm")) : '--')
            });
            $(".date-view").html(function(){
                return (($(this).html()!=""&&$(this).html()!="null") ? (new persianDate(new Date($(this).html())).format("YYYY/MM/DD")) : '--')
            });
            $(".date-time-view-form > input").val(function(){
                return new persianDate(new Date($(this).val())).format("YYYY/MM/DD HH:mm")
            });
            $(".date-view-form > input").val(function(){
                return new persianDate(new Date($(this).val())).format("YYYY/MM/DD")
            });
            // TODO:implement p2g
        }else{
           $(".date-time-view, .Timestamp").html(function(){
               return (($(this).html()!=""&&$(this).html()!="null") ? (new persianDate(new Date($(this).html())).format("YYYY/MM/DD HH:mm")+"<br/>"+$(this).html()) : '--')
           });
           $(".date-view").html(function(){
               return (($(this).html()!=""&&$(this).html()!="null") ? (new persianDate(new Date($(this).html())).format("YYYY/MM/DD")+"<br/>"+$(this).html()) : '--')
           });

       }
    });
</script>