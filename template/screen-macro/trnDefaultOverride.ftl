<#include "../../../../template/screen-macro/DefaultScreenMacros.html.ftl"/>
<#macro "form-single">
    <#if sri.doBoundaryComments()><!-- BEGIN form-single[@name=${.node["@name"]}] --></#if>
<#-- Use the formNode assembled based on other settings instead of the straight one from the file: -->
    <#assign formInstance = sri.getFormInstance(.node["@name"])>
    <#assign formNode = formInstance.getFormNode()>
    <#t>${sri.pushSingleFormMapContext(formNode["@map"]!"fieldValues")}
    <#assign skipStart = formNode["@skip-start"]! == "true">
    <#assign skipEnd = formNode["@skip-end"]! == "true">
    <#assign ownerForm = formNode["@owner-form"]!>
    <#if ownerForm?has_content><#assign skipStart = true><#assign skipEnd = true></#if>
    <#assign urlInstance = sri.makeUrlByType(formNode["@transition"], "transition", null, "true")>
    <#assign formId>${ec.getResource().expandNoL10n(formNode["@name"], "")}<#if sectionEntryIndex?has_content>_${sectionEntryIndex}</#if></#assign>
    <#if !skipStart>
    <form name="${formId}" id="${formId}" method="post" action="${urlInstance.url}"<#if formInstance.isUpload()>
          enctype="multipart/form-data"</#if> class="moqui-form-single">
        <input type="hidden" name="moquiFormName" value="${formNode["@name"]}">
        <input type="hidden" name="moquiSessionToken" value="${(ec.getWeb().sessionToken)!}">
        <#assign lastUpdatedString = sri.getNamedValuePlain("lastUpdatedStamp", formNode)>
        <#if lastUpdatedString?has_content><input type="hidden" name="lastUpdatedStamp"
                                                  value="${lastUpdatedString}"></#if>
    </#if>
        <fieldset class="form-horizontal"<#if urlInstance.disableLink> disabled="disabled"</#if>>
        <#if formNode["field-layout"]?has_content>
            <#recurse formNode["field-layout"][0]/>
        <#else>
            <#list formNode["field"] as fieldNode><@formSingleSubField fieldNode formId/></#list>
        </#if>
        </fieldset>
    <#if !skipEnd></form></#if>
    <#if !skipStart>
        <script>
            $("#${formId}").validate({
                errorClass: 'help-block', errorElement: 'span',
                highlight: function (element, errorClass, validClass) {
                    $(element).parents('.form-group').removeClass('has-success').addClass('has-error');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).parents('.form-group').removeClass('has-error').addClass('has-success');
                }
            });
            $('#${formId} [data-toggle="tooltip"]').tooltip({placement: 'auto top'});

            <#-- if background-submit=true init ajaxForm; for examples see http://www.malsup.com/jquery/form/#ajaxForm -->
            <#if formNode["@background-submit"]! == "true">
            function backgroundSuccess${formId}(responseText, statusText, xhr, $form) {
                <#if formNode["@background-reload-id"]?has_content>
                    load${formNode["@background-reload-id"]}();
                </#if>
                <#if formNode["@background-message"]?has_content>
                    alert("${formNode["@background-message"]}");
                </#if>
                <#if formNode["@background-hide-id"]?has_content>
                    $('#${formNode["@background-hide-id"]}').modal('hide');
                </#if>
            }
            $("#${formId}").ajaxForm({success: backgroundSuccess${formId}, resetForm: false});
            </#if>
        </script>
    </#if>
    <#if formNode["@focus-field"]?has_content>
        <script>$("#${formId}").find('[name^="${formNode["@focus-field"]}"]').addClass('default-focus').focus();</script>
    </#if>
    <#t>${sri.popContext()}<#-- context was pushed for the form-single so pop here at the end -->
    <#if sri.doBoundaryComments()><!-- END   form-single[@name=${.node["@name"]}] --></#if>
    <#assign ownerForm = ""><#-- clear ownerForm so later form fields don't pick it up -->
</#macro>
<#macro "container-box">
    <#assign contBoxDivId><@nodeId .node/></#assign>
    <#assign boxHeader = .node["box-header"][0]>
    <#assign boxType = ec.resource.expand(.node["@type"], "")!>
    <#if !boxType?has_content><#assign boxType = "default"></#if>
    <div class="panel panel-${boxType} <#if .node["@style"]?has_content> ${ec.getResource().expandNoL10n(.node["@style"], "")}</#if>"<#if contBoxDivId?has_content>
         id="${contBoxDivId}"</#if>>
        <div class="panel-heading">
            <#if boxHeader["@title"]?has_content><h5>${ec.getResource().expand(boxHeader["@title"]!"", "")}</h5></#if>
            <#recurse boxHeader>
            <#if .node["box-toolbar"]?has_content>
                <div class="panel-toolbar">
                    <#recurse .node["box-toolbar"][0]>
                </div>
            </#if>
        </div>
        <#if .node["box-body"]?has_content>
            <div class="panel-body"<#if .node["box-body"][0]["@height"]?has_content>
                 style="max-height: ${.node["box-body"][0]["@height"]}px; overflow-y: auto;"</#if>>
                <#recurse .node["box-body"][0]>
            </div>
        </#if>
        <#if .node["box-body-nopad"]?has_content>
            <#recurse .node["box-body-nopad"][0]>
        </#if>
    </div>
</#macro>
<#macro "subscreens-panel">
    <#assign dynamic = .node["@dynamic"]! == "true" && .node["@id"]?has_content>
    <#assign dynamicActive = 0>
    <#assign displayMenu = sri.activeInCurrentMenu!true && hideNav! != "true">
    <#assign menuId><#if .node["@id"]?has_content>${.node["@id"]}-menu<#else>subscreensPanelMenu</#if></#assign>
    <#assign menuTitle = .node["@title"]!sri.getActiveScreenDef().getDefaultMenuName()!"Menu">
    <#if .node["@type"]! == "popup">
        <#if hideNav! != "true">
        <div class="hidden">
            <li id="${menuId}" class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">${ec.getResource().expand(menuTitle, "")} <i
                        class="glyphicon glyphicon-chevron-right"></i></a>
                <ul class="dropdown-menu">
                <#list sri.getActiveScreenDef().getMenuSubscreensItems() as subscreensItem>
                    <#assign urlInstance = sri.buildUrl(subscreensItem.name)>
                    <#if urlInstance.isPermitted()>
                        <li<#if urlInstance.inCurrentScreenPath> class="active"</#if>><a
                                href="<#if urlInstance.disableLink>#<#else>${urlInstance.minimalPathUrlWithParams}</#if>"><#rt>
                            <#assign expandedMenuTitle = ec.getResource().expand(subscreensItem.menuTitle, "")>
                            <#if urlInstance.sui.menuImage?has_content>
                                <#if urlInstance.sui.menuImageType == "icon">
                                    <#t><i class="${urlInstance.sui.menuImage}" style="padding-right: 8px;"></i>
                                <#elseif urlInstance.sui.menuImageType == "url-plain">
                                    <#t><img src="${urlInstance.sui.menuImage}" alt="${expandedMenuTitle}" width="18"
                                             style="padding-right: 4px;"/>
                                <#else><#rt>
                                    <#t><img src="${sri.buildUrl(urlInstance.sui.menuImage).url}"
                                             alt="${expandedMenuTitle}" height="18" style="padding-right: 4px;"/>
                                </#if><#rt>
                            <#else><#rt>
                                <#t><i class="glyphicon glyphicon-link" style="padding-right: 8px;"></i>
                            </#if><#rt>
                            <#t>${expandedMenuTitle}
                        <#lt></a></li>
                    </#if>
                </#list>
                </ul>
            </li>
        </div>
        <#-- move the menu to the header menus section -->
        <script>$("#${.node["@header-menus-id"]!"header-menus"}").append($("#${menuId}"));</script>
        </#if>
        ${sri.renderSubscreen()}
    <#elseif .node["@type"]! == "stack">
        <h1>LATER stack type subscreens-panel not yet supported.</h1>
    <#elseif .node["@type"]! == "wizard">
        <h1>LATER wizard type subscreens-panel not yet supported.</h1>
    <#elseif .node["@type"]! == "trn-dashboard">
        <div<#if .node["@id"]?has_content> id="${.node["@id"]}-trn-dashboard"</#if> class="trn-dashboard">
        <#assign menuSubscreensItems=sri.getActiveScreenDef().getMenuSubscreensItems()>
        <#assign counter = 0>
        <#if menuSubscreensItems?has_content && (menuSubscreensItems?size > 0)>
            <#assign menuItemSize = menuSubscreensItems?size>
            <#if !sri.getActiveScreenHasNext()>

                <#list menuSubscreensItems as subscreensItem>
                    <#assign urlInstance = sri.buildUrl(subscreensItem.name)>
                    <#if counter==0>
                            <div class="row">
                    </#if>

                    <#if !urlInstance.isPermitted()>
                        <#assign menuItemSize = menuItemSize - 1>
                    <#else>
                            <div class="col-md-3 text-center">
                                <div class="trn-dashboard-item">
                                <#if urlInstance.sui.menuImage?has_content>
                                    <#if urlInstance.sui.menuImageType == "icon">
                                        <i class="${urlInstance.sui.menuImage} trn-dashboard-img"></i>
                                    <#elseif urlInstance.sui.menuImageType == "url-plain">
                                        <img src="${urlInstance.sui.menuImage}" class="trn-dashboard-img"/>
                                    <#else><#rt>
                                        <img src="${sri.buildUrl(urlInstance.sui.menuImage).url}"
                                             class="trn-dashboard-img">
                                    </#if>
                                <#else>
                                    <img src="/trnstatic/images/trn-icon/5.png" class="trn-dashboard-img"/>
                                </#if>
                                    <h4 class="text-inline trn-dashboard-item-title">${ec.getResource().expand(subscreensItem.menuTitle+"_title", "")}</h4>
                                    <p class="text-inline trn-dashboard-item-text">${ec.getResource().expand(subscreensItem.menuTitle+"_text", "")}</p>
                                    <a href="<#if urlInstance.disableLink>#<#else>${urlInstance.minimalPathUrlWithParams}</#if>"
                                       class="btn btn-primary btn-sm trn-dashboard-item-link <#if urlInstance.disableLink>disabled</#if>">${ec.getResource().expand(subscreensItem.menuTitle+"_btn", "")}</a>
                                <#if dynamic>
                                    <#assign urlInstance = urlInstance.addParameter("lastStandalone", "true")>
                                    <#if urlInstance.inCurrentScreenPath>
                                        <#assign dynamicActive = subscreensItem_index>
                                        <#assign urlInstance = urlInstance.addParameters(ec.getWeb().requestParameters)>
                                    </#if>
                                </#if>
                                </div>
                            </div>
                        <#assign counter += 1>
                    </#if>

                    <#if (counter == menuItemSize)>
                            </div>
                    <#else>
                        <#if counter%4==0></div>
                        <div class="row">
                        </#if>
                    </#if>
                </#list>
            </#if>
        </#if>
        <#if hideNav! != "true">
        <#-- add to navbar bread crumbs too -->
                <a id="${menuId}-crumb" class="navbar-text" href="${sri.buildUrl(".")}"
                   style="display: none;">${ec.getResource().expand(menuTitle, "")} <i
                        class="glyphicon glyphicon-chevron-right"></i></a>
                <script>$("#navbar-menu-crumbs").append($("#${menuId}-crumb"));</script>
        </#if>
        </div>
        <#if sri.getActiveScreenHasNext()>
            ${sri.renderSubscreen()}
        </#if>
    <#else>
    <#-- default to type=tab -->
        <div<#if .node["@id"]?has_content> id="${.node["@id"]}-tabpanel"</#if>>
            <#assign menuSubscreensItems=sri.getActiveScreenDef().getMenuSubscreensItems()>
            <#if menuSubscreensItems?has_content && (menuSubscreensItems?size > 1)>
                <#if displayMenu>
                    <ul<#if .node["@id"]?has_content> id="${.node["@id"]}-menu"</#if> class="nav nav-tabs"
                                                      role="tablist">
                    <#list menuSubscreensItems as subscreensItem>
                        <#assign urlInstance = sri.buildUrl(subscreensItem.name)>
                        <#if urlInstance.isPermitted()>
                            <#if dynamic>
                                <#assign urlInstance = urlInstance.addParameter("lastStandalone", "true")>
                                <#if urlInstance.inCurrentScreenPath>
                                    <#assign dynamicActive = subscreensItem_index>
                                    <#assign urlInstance = urlInstance.addParameters(ec.getWeb().requestParameters)>
                                </#if>
                            </#if>
                            <li class="<#if urlInstance.disableLink>disabled<#elseif urlInstance.inCurrentScreenPath>active</#if>">
                                <a href="<#if urlInstance.disableLink>#<#else>${urlInstance.minimalPathUrlWithParams}</#if>">${ec.getResource().expand(subscreensItem.menuTitle, "")}</a>
                            </li>
                        </#if>
                    </#list>
                    </ul>
                </#if>
            </#if>
            <#if hideNav! != "true">
            <#-- add to navbar bread crumbs too -->
                <a id="${menuId}-crumb" class="navbar-text" href="${sri.buildUrl(".")}"
                   style="display: none;">${ec.getResource().expand(menuTitle, "")} <i
                        class="glyphicon glyphicon-chevron-right"></i></a>
                <script>$("#navbar-menu-crumbs").append($("#${menuId}-crumb"));</script>
            </#if>
            <#if !dynamic || !displayMenu>
            <#-- these make it more similar to the HTML produced when dynamic, but not needed: <div<#if .node["@id"]?has_content> id="${.node["@id"]}-active"</#if> class="ui-tabs-panel"> -->
                <div class="meepo-tabs">
                    ${sri.renderSubscreen()}
                </div>
            <#-- </div> -->
            </#if>
        </div>
        <#if dynamic && displayMenu>
            <#assign afterScreenScript>
                $("#${.node["@id"]}").tabs({ collapsible: true, selected: ${dynamicActive},
                    spinner: '<span class="ui-loading">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>',
                    ajaxOptions: { error: function(xhr, status, index, anchor) { $(anchor.hash).html("Error loading screen..."); } },
                    load: function(event, ui) { <#-- activateAllButtons(); --> }
                });
            </#assign>
            <#t>${sri.appendToScriptWriter(afterScreenScript)}
        </#if>
    </#if>
</#macro>

<#macro "date-period">
    <#assign id><@fieldId .node/></#assign>
    <#assign curFieldName><@fieldName .node/></#assign>
    <#assign fvOffset = ec.getContext().get(curFieldName + "_poffset")!>
    <#assign fvPeriod = ec.getContext().get(curFieldName + "_period")!?lower_case>
    <#assign fvDate = ec.getContext().get(curFieldName + "_pdate")!"">
    <#assign allowEmpty = .node["@allow-empty"]!"true">
<div class="date-period">
    <select name="${curFieldName}_poffset" id="${id}_poffset"<#if .node?parent["@tooltip"]?has_content>
            data-toggle="tooltip"
            title="${ec.getResource().expand(.node?parent["@tooltip"], "")}"</#if><#if ownerForm?has_content>
            form="${ownerForm}"</#if>>
        <#if (allowEmpty! != "false")>
            <option value="">&nbsp;</option>
        </#if>
        <option value="0"<#if fvOffset == "0"> selected="selected"</#if>>${ec.getL10n().localize("This")}</option>
        <option value="-1"<#if fvOffset == "-1"> selected="selected"</#if>>${ec.getL10n().localize("Last")}</option>
        <option value="1"<#if fvOffset == "1"> selected="selected"</#if>>${ec.getL10n().localize("Next")}</option>
        <option value="-2"<#if fvOffset == "-2"> selected="selected"</#if>>-2</option>
        <option value="2"<#if fvOffset == "2"> selected="selected"</#if>>+2</option>
        <option value="-3"<#if fvOffset == "-3"> selected="selected"</#if>>-3</option>
        <option value="-4"<#if fvOffset == "-4"> selected="selected"</#if>>-4</option>
        <option value="-6"<#if fvOffset == "-6"> selected="selected"</#if>>-6</option>
        <option value="-12"<#if fvOffset == "-12"> selected="selected"</#if>>-12</option>
    </select>
    <select name="${curFieldName}_period" id="${id}_period"<#if .node?parent["@tooltip"]?has_content>
            data-toggle="tooltip"
            title="${ec.getResource().expand(.node?parent["@tooltip"], "")}"</#if><#if ownerForm?has_content>
            form="${ownerForm}"</#if>>
        <#if (allowEmpty! != "false")>
            <option value="">&nbsp;</option>
        </#if>
        <option value="day" <#if fvPeriod == "day"> selected="selected"</#if>>${ec.getL10n().localize("Day")}</option>
        <option value="7d" <#if fvPeriod == "7d"> selected="selected"</#if>>7 ${ec.getL10n().localize("Days")}</option>
        <option value="30d" <#if fvPeriod == "30d"> selected="selected"</#if>>
            30 ${ec.getL10n().localize("Days")}</option>
        <option value="week" <#if fvPeriod == "week">selected="selected"</#if>>${ec.getL10n().localize("Week")}</option>
        <option value="weeks" <#if fvPeriod == "weeks">
                selected="selected"</#if>>${ec.getL10n().localize("Weeks")}</option>
        <option value="month" <#if fvPeriod == "month">
                selected="selected"</#if>>${ec.getL10n().localize("Month")}</option>
        <option value="months" <#if fvPeriod == "months">
                selected="selected"</#if>>${ec.getL10n().localize("Months")}</option>
        <option value="quarter" <#if fvPeriod == "quarter">
                selected="selected"</#if>>${ec.getL10n().localize("Quarter")}</option>
        <option value="year" <#if fvPeriod == "year">selected="selected"</#if>>${ec.getL10n().localize("Year")}</option>
        <option value="7r" <#if fvPeriod == "7r"> selected="selected"</#if>>+/-7d</option>
        <option value="30r" <#if fvPeriod == "30r"> selected="selected"</#if>>+/-30d</option>
    </select>
    <div class="input-group date" id="${id}_pdate">
        <input type="text" class="form-control" name="${curFieldName}_pdate" value="${fvDate?html}" size="10"
               maxlength="10"<#if ownerForm?has_content> form="${ownerForm}"</#if>>
        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
        <span id="${id}_pdateP" class="input-group-addon persian-datepicker-override-picker"><span
                class="glyphicon glyphicon-calendar"></span></span>
        <span id="${id}_pdatePP" class="input-group-addon persian-datepicker-override-show"></span>
    </div>
    <script>
        function FAToEN(str) {
            var id = {
                '۰': '0',
                '۱': '1',
                '۲': '2',
                '۳': '3',
                '۴': '4',
                '۵': '5',
                '۶': '6',
                '۷': '7',
                '۸': '8',
                '۹': '9'
            };
            return str.replace(/[^0-9.]/g, function (w) {
                return id[w] || w;
            });
        }

        if ($('#${id}_pdate > input').val() != undefined && $('#${id}_pdate > input').val() != '') {
            $('#${id}_pdatePP').html(new persianDate(new Date($('#${id}_pdate > input').val())).format("YYYY/MM/DD"));
        }

        $("#${id}_poffset").select2({});
        $("#${id}_period").select2({});
        $('#${id}_pdate').datetimepicker({
            toolbarPlacement: 'top',
            showClose: true,
            showClear: true,
            showTodayButton: true,
            useStrict: true,
            defaultDate: '${fvDate?html}' && moment('${fvDate?html}', 'YYYY-MM-DD'),
            format: 'YYYY-MM-DD',
            extraFormats: ['l', 'L', 'YYYY-MM-DD'],
            locale: "${ec.getUser().locale.toLanguageTag()}",
            keyBinds: {
                t: function () {
                    this.date(moment());
                }, up: function () {
                    this.date(this.date().clone().add(1, 'd'));
                }, down: function () {
                    this.date(this.date().clone().subtract(1, 'd'));
                }, 'control up': function () {
                    this.date(this.date().clone().add(1, 'd'));
                }, 'control down': function () {
                    this.date(this.date().clone().subtract(1, 'd'));
                }
            }
        });
        $('#${id}_pdateP').persianDatepicker({
            onSelect: function (unixDate) {
                var selectedDa = "";
                if (unixDate) {
                    selectedDa = new Date(unixDate);
                } else {
                    var listDate = [];
                    listDate.push(parseInt(FAToEN($(this.model.input.elem).val().substring(0, 4))));
                    listDate.push(parseInt(FAToEN($(this.model.input.elem).val().substring(5, 7))));
                    listDate.push(parseInt(FAToEN($(this.model.input.elem).val().substring(8, 10))));
                    listDate.push(parseInt(FAToEN($(this.model.input.elem).val().substring(11, 13))));
                    listDate.push(parseInt(FAToEN($(this.model.input.elem).val().substring(14, 16))));
                    selectedDa = new Date(new persianDate(listDate).toLocale("en").toCalendar('gregorian').format("YYYY/MM/DD HH:mm"));
                }
                $('#${id}_pdatePP').html(new persianDate(selectedDa).format("YYYY/MM/DD"));
                $('#${id}_pdate > input').val(selectedDa.getFullYear() + "-" + ("0" + (parseInt(selectedDa.getMonth()) + 1)).slice(-2) + "-" + ("0" + selectedDa.getDate()).slice(-2));
            }, format: 'YYYY-MM-DD', toolbox: {calendarSwitch: {enabled: false}, submitButton: {enabled: true}}
        });
    </script>
</div>
</#macro>
<#macro "date-find">
    <#if .node["@type"]! == "time"><#assign size=9><#assign maxlength=13><#assign defaultFormat="HH:mm"><#assign extraFormatsVal = "['LT', 'LTS', 'HH:mm']">
    <#elseif .node["@type"]! == "date"><#assign size=10><#assign maxlength=10><#assign defaultFormat="yyyy-MM-dd"><#assign extraFormatsVal = "['l', 'L', 'YYYY-MM-DD']">
    <#else><#assign size=16><#assign maxlength=23><#assign defaultFormat="yyyy-MM-dd HH:mm"><#assign extraFormatsVal = "['YYYY-MM-DD HH:mm', 'YYYY-MM-DD HH:mm:ss', 'MM/DD/YYYY HH:mm']">
    </#if>
    <#assign datepickerFormat><@getMomentDateFormat .node["@format"]!defaultFormat/></#assign>
    <#assign curFieldName><@fieldName .node/></#assign>
    <#assign fieldValueFrom = ec.getL10n().format(ec.getContext().get(curFieldName + "_from")!?default(.node["@default-value-from"]!""), defaultFormat)>
    <#assign fieldValueThru = ec.getL10n().format(ec.getContext().get(curFieldName + "_thru")!?default(.node["@default-value-thru"]!""), defaultFormat)>
    <#assign id><@fieldId .node/></#assign>
<span class="form-date-find">
      <span>${ec.getL10n().localize("From")}&nbsp;</span>
    <#if .node["@type"]! != "time">
        <div class="input-group date" id="${id}_from">
            <input type="text" class="form-control" name="${curFieldName}_from" value="${fieldValueFrom?html}"
                   size="${size}" maxlength="${maxlength}"<#if .node?parent["@tooltip"]?has_content>
                   data-toggle="tooltip"
                   title="${ec.getResource().expand(.node?parent["@tooltip"], "")}"</#if><#if ownerForm?has_content>
                   form="${ownerForm}"</#if>>
        <#--<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>-->
        </div>
        <span id="${id}_fromP" class="input-group-addon persian-datepicker-override-picker"><span
                class="glyphicon glyphicon-calendar"></span></span>
        <span id="${id}_fromPP" class="input-group-addon persian-datepicker-override-show"></span>

        <script>
            function FAToEN(str) {
                var id = {
                    '۰': '0',
                    '۱': '1',
                    '۲': '2',
                    '۳': '3',
                    '۴': '4',
                    '۵': '5',
                    '۶': '6',
                    '۷': '7',
                    '۸': '8',
                    '۹': '9'
                };
                return str.replace(/[^0-9.]/g, function (w) {
                    return id[w] || w;
                });
            }

            if ($('#${id}_from > input').val() != undefined && $('#${id}_from > input').val() != '') {
                $('#${id}_fromPP').html(new persianDate(new Date($('#${id}_from > input').val())).format("YYYY/MM/DD"));
            }
            $('#${id}_from').datetimepicker({
                toolbarPlacement: 'top',
                showClose: true,
                showClear: true,
                useStrict: true,
                showTodayButton: true,
                defaultDate: '${fieldValueFrom?html}' && moment('${fieldValueFrom?html}', '${datepickerFormat}'),
                format: '${datepickerFormat}',
                extraFormats:${extraFormatsVal},
                stepping: 5,
                locale: "${ec.getUser().locale.toLanguageTag()}",
                keyBinds: {
                    t: function () {
                        this.date(moment());
                    }, up: function () {
                        this.date(this.date().clone().add(1, 'd'));
                    }, down: function () {
                        this.date(this.date().clone().subtract(1, 'd'));
                    }, 'control up': function () {
                        this.date(this.date().clone().add(1, 'd'));
                    }, 'control down': function () {
                        this.date(this.date().clone().subtract(1, 'd'));
                    }
                }
            });
            $('#${id}_fromP').persianDatepicker({
                onSelect: function (unixDate) {
                    var selectedDa = "";
                    if (unixDate) {
                        selectedDa = new Date(unixDate);
                    } else {
                        var listDate = [];
                        listDate.push(parseInt(FAToEN($(this.model.input.elem).val().substring(0, 4))));
                        listDate.push(parseInt(FAToEN($(this.model.input.elem).val().substring(5, 7))));
                        listDate.push(parseInt(FAToEN($(this.model.input.elem).val().substring(8, 10))));
                        listDate.push(parseInt(FAToEN($(this.model.input.elem).val().substring(11, 13))));
                        listDate.push(parseInt(FAToEN($(this.model.input.elem).val().substring(14, 16))));
                        selectedDa = new Date(new persianDate(listDate).toLocale("en").toCalendar('gregorian').format("YYYY/MM/DD HH:mm"));
                    }
                    $('#${id}_fromPP').html(new persianDate(selectedDa).format("YYYY/MM/DD"));
                    $('#${id}_from > input').val(selectedDa.getFullYear() + "-" + ("0" + (parseInt(selectedDa.getMonth()) + 1)).slice(-2) + "-" + ("0" + selectedDa.getDate()).slice(-2));
                }, format: 'YYYY-MM-DD', toolbox: {calendarSwitch: {enabled: false}, submitButton: {enabled: true}}
            });
        </script>

    <#else>
        <input type="text" class="form-control" pattern="^(?:(?:([01]?\d|2[0-3]):)?([0-5]?\d):)?([0-5]?\d)$"
               name="${curFieldName}_from" value="${fieldValueFrom?html}" size="${size}"
               maxlength="${maxlength}"<#if .node?parent["@tooltip"]?has_content> data-toggle="tooltip"
               title="${ec.getResource().expand(.node?parent["@tooltip"], "")}"</#if><#if ownerForm?has_content>
               form="${ownerForm}"</#if>>
    </#if>
    </span>
<span class="form-date-find">
      <span>${ec.getL10n().localize("Thru")}&nbsp;</span>
    <#if .node["@type"]! != "time">
        <div class="input-group date" id="${id}_thru">
            <input type="text" class="form-control" name="${curFieldName}_thru" value="${fieldValueThru?html}"
                   size="${size}" maxlength="${maxlength}"<#if .node?parent["@tooltip"]?has_content>
                   data-toggle="tooltip"
                   title="${ec.getResource().expand(.node?parent["@tooltip"], "")}"</#if><#if ownerForm?has_content>
                   form="${ownerForm}"</#if>>
        <#--<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>-->
        </div>
        <span id="${id}_thruP" class="input-group-addon persian-datepicker-override-picker"><span
                class="glyphicon glyphicon-calendar"></span></span>
        <span id="${id}_thruPP" class="input-group-addon persian-datepicker-override-show"></span>
        <script>
            function FAToEN(str) {
                var id = {
                    '۰': '0',
                    '۱': '1',
                    '۲': '2',
                    '۳': '3',
                    '۴': '4',
                    '۵': '5',
                    '۶': '6',
                    '۷': '7',
                    '۸': '8',
                    '۹': '9'
                };
                return str.replace(/[^0-9.]/g, function (w) {
                    return id[w] || w;
                });
            }

            if ($('#${id}_thru > input').val() != undefined && $('#${id}_thru > input').val() != '') {
                $('#${id}_thruPP').html(new persianDate(new Date($('#${id}_thru > input').val())).format("YYYY/MM/DD"));
            }
            $('#${id}_thru').datetimepicker({
                toolbarPlacement: 'top',
                showClose: true,
                showClear: true,
                useStrict: true,
                showTodayButton: true,
                defaultDate: '${fieldValueThru?html}' && moment('${fieldValueThru?html}', '${datepickerFormat}'),
                format: '${datepickerFormat}',
                extraFormats:${extraFormatsVal},
                stepping: 5,
                locale: "${ec.getUser().locale.toLanguageTag()}",
                keyBinds: {
                    t: function () {
                        this.date(moment());
                    }, up: function () {
                        this.date(this.date().clone().add(1, 'd'));
                    }, down: function () {
                        this.date(this.date().clone().subtract(1, 'd'));
                    }, 'control up': function () {
                        this.date(this.date().clone().add(1, 'd'));
                    }, 'control down': function () {
                        this.date(this.date().clone().subtract(1, 'd'));
                    }
                }
            });
            $('#${id}_thruP').persianDatepicker({
                onSelect: function (unixDate) {
                    var selectedDa = "";
                    if (unixDate) {
                        selectedDa = new Date(unixDate);
                    } else {
                        var listDate = [];
                        listDate.push(parseInt(FAToEN($(this.model.input.elem).val().substring(0, 4))));
                        listDate.push(parseInt(FAToEN($(this.model.input.elem).val().substring(5, 7))));
                        listDate.push(parseInt(FAToEN($(this.model.input.elem).val().substring(8, 10))));
                        listDate.push(parseInt(FAToEN($(this.model.input.elem).val().substring(11, 13))));
                        listDate.push(parseInt(FAToEN($(this.model.input.elem).val().substring(14, 16))));
                        selectedDa = new Date(new persianDate(listDate).toLocale("en").toCalendar('gregorian').format("YYYY/MM/DD HH:mm"));
                    }
                    $('#${id}_thruPP').html(new persianDate(selectedDa).format("YYYY/MM/DD"));
                    $('#${id}_thru > input').val(selectedDa.getFullYear() + "-" + ("0" + (parseInt(selectedDa.getMonth()) + 1)).slice(-2) + "-" + ("0" + selectedDa.getDate()).slice(-2));
                }, format: 'YYYY-MM-DD', toolbox: {calendarSwitch: {enabled: false}, submitButton: {enabled: true}}
            });
        </script>
    <#else>
        <input type="text" class="form-control" pattern="^(?:(?:([01]?\d|2[0-3]):)?([0-5]?\d):)?([0-5]?\d)$"
               name="${curFieldName}_thru" value="${fieldValueThru?html}" size="${size}"
               maxlength="${maxlength}"<#if .node?parent["@tooltip"]?has_content> data-toggle="tooltip"
               title="${ec.getResource().expand(.node?parent["@tooltip"], "")}"</#if><#if ownerForm?has_content>
               form="${ownerForm}"</#if>>
    </#if>
    </span>
</#macro>
<#macro "date-time">
    <#assign dtSubFieldNode = .node?parent>
    <#assign dtFieldNode = dtSubFieldNode?parent>
    <#assign javaFormat = .node["@format"]!>
    <#assign validationClasses = formInstance.getFieldValidationClasses(dtSubFieldNode)>
    <#if !javaFormat?has_content>
        <#if .node["@type"]! == "time"><#assign javaFormat="HH:mm">
        <#elseif .node["@type"]! == "date"><#assign javaFormat="yyyy-MM-dd">
        <#else><#assign javaFormat="yyyy-MM-dd HH:mm"></#if>
    </#if>
    <#assign datepickerFormat><@getMomentDateFormat javaFormat/></#assign>
    <#assign fieldValue = sri.getFieldValueString(dtFieldNode, .node["@default-value"]!"", javaFormat)>

    <#assign id><@fieldId .node/></#assign>

    <#if .node["@type"]! == "time"><#assign size=9><#assign maxlength=13><#assign extraFormatsVal = "['LT', 'LTS', 'HH:mm']">
    <#elseif .node["@type"]! == "date"><#assign size=10><#assign maxlength=10><#assign extraFormatsVal = "['l', 'L', 'YYYY-MM-DD']">
    <#else><#assign size=16><#assign maxlength=23><#assign extraFormatsVal = "['YYYY-MM-DD HH:mm', 'YYYY-MM-DD HH:mm:ss', 'MM/DD/YYYY HH:mm']"></#if>
    <#assign size = .node["@size"]!size>
    <#assign maxlength = .node["@max-length"]!maxlength>

    <#if .node["@type"]! != "time">
    <div class="input-group date" id="${id}">
        <input type="text"
               class="form-control<#if validationClasses?contains("required")> required</#if>"<#if validationClasses?contains("required")>
               required="required"</#if> name="<@fieldName .node/>" value="${fieldValue?html}" size="${size}"
               maxlength="${maxlength}"<#if .node?parent["@tooltip"]?has_content> data-toggle="tooltip"
               title="${ec.getResource().expand(.node?parent["@tooltip"], "")}"</#if><#if ownerForm?has_content>
               form="${ownerForm}"</#if>>
    <#--<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>-->
    </div>
    <span id="${id}P" class="input-group-addon persian-datepicker-override-picker"><span
            class="glyphicon glyphicon-calendar"></span></span>
    <span id="${id}PP" class="input-group-addon persian-datepicker-override-show"></span>

    <script>
        function FAToEN(str) {
            var id = {
                '۰': '0',
                '۱': '1',
                '۲': '2',
                '۳': '3',
                '۴': '4',
                '۵': '5',
                '۶': '6',
                '۷': '7',
                '۸': '8',
                '۹': '9'
            };
            return str.replace(/[^0-9.]/g, function (w) {
                return id[w] || w;
            });
        }
        <#if .node["@type"]! == "time">
            var ${id}format = "HH:mm";
            var ${id}needTime = true;
        <#elseif .node["@type"]! == "date">
            var ${id}format = "YYYY/MM/DD";
            var ${id}needTime = false;
        <#else>
            var ${id}format = "YYYY/MM/DD HH:mm";
            var ${id}needTime = true;
        </#if>
        if ($('#${id} > input').val() != undefined && $('#${id} > input').val() != '') {
            $('#${id}PP').html(new persianDate(new Date($('#${id} > input').val())).format(${id}format));
        }

        $('#${id}').datetimepicker({
            toolbarPlacement: 'top',
            showClose: true,
            showClear: true,
            showTodayButton: true,
            useStrict: true,
            defaultDate: '${fieldValue?html}' && moment('${fieldValue?html}', '${datepickerFormat}'),
            format: '${datepickerFormat}',
            extraFormats:${extraFormatsVal},
            stepping: 5,
            locale: "${ec.getUser().locale.toLanguageTag()}"
        });
        $('#${id}').on("dp.change", function () {
            var jqEl = $('#${id}');
            jqEl.val(jqEl.find("input").first().val());
            $('#${id}PP').html(new persianDate(new Date(jqEl.find("input").first().val())).format(${id}format));
            jqEl.trigger("change");
        });
        $('#${id}P').persianDatepicker({
            onSelect: function (unixDate) {
                var selectedDa = "";
                if (unixDate) {
                    selectedDa = new Date(unixDate);
                } else {
                    var listDate = [];
                    listDate.push(parseInt(FAToEN($(this.model.input.elem).val().substring(0, 4))));
                    listDate.push(parseInt(FAToEN($(this.model.input.elem).val().substring(5, 7))));
                    listDate.push(parseInt(FAToEN($(this.model.input.elem).val().substring(8, 10))));
                    listDate.push(parseInt(FAToEN($(this.model.input.elem).val().substring(11, 13))));
                    listDate.push(parseInt(FAToEN($(this.model.input.elem).val().substring(14, 16))));
                    selectedDa = new Date(new persianDate(listDate).toLocale("en").toCalendar('gregorian').format("YYYY/MM/DD HH:mm"));
                }
                $('#${id}PP').html(new persianDate(selectedDa).format(${id}format));
                if (${id}format == "HH:mm") {
                    $('#${id},#${id} > input').val(("0" + selectedDa.getHours()).slice(-2) + ":" + ("0" + selectedDa.getMinutes()).slice(-2));
                } else if (${id}format == "YYYY/MM/DD") {
                    $('#${id},#${id} > input').val(selectedDa.getFullYear() + "-" + ("0" + (parseInt(selectedDa.getMonth()) + 1)).slice(-2) + "-" + ("0" + selectedDa.getDate()).slice(-2));
                } else { //YYYY/MM/DD HH:mm
                    $('#${id},#${id} > input').val(selectedDa.getFullYear() + "-" + ("0" + (parseInt(selectedDa.getMonth()) + 1)).slice(-2) + "-" + ("0" + selectedDa.getDate()).slice(-2) + " " + ("0" + selectedDa.getHours()).slice(-2) + ":" + ("0" + selectedDa.getMinutes()).slice(-2));
                }
            },
            position: [50, -120],
            persianDigit: false,
            format: "YYYY/MM/DD HH:mm",
            toolbox: {
                calendarSwitch: {enabled: true}
            }, submitButton: {enabled: true}, timePicker: {
                enabled: ${id}needTime,
                step: 1,
                hour: {
                    enabled: ${id}needTime,
                    step: 1
                },
                minute: {
                    enabled: ${id}needTime,
                    step: 1
                },
                second: {
                    enabled: false
                }
            }
        });

    </script>
    <#else>
    <#-- datetimepicker does not support time only, even with plain HH:mm format; use a regex to validate time format -->
    <input type="text" class="form-control" pattern="^(?:(?:([01]?\d|2[0-3]):)?([0-5]?\d):)?([0-5]?\d)$"
           name="<@fieldName .node/>" value="${fieldValue?html}" size="${size}"
           maxlength="${maxlength}"<#if .node?parent["@tooltip"]?has_content> data-toggle="tooltip"
           title="${ec.getResource().expand(.node?parent["@tooltip"], "")}"</#if><#if ownerForm?has_content>
           form="${ownerForm}"</#if>>
    </#if>
</#macro>

<#macro "json-form-single">
    <#assign instancePurpose = Static["org.moqui.util.SystemBinding"].getPropOrEnv("instance_purpose")!"production">
    <#assign submissionType = .node["@submission-type"]!"regular">
    <#if .node["@transition"]??>
        <#assign urlInstance = sri.makeUrlByType(.node["@transition"], "transition", null, "true")>
    </#if>
    <#if .node["@rest"]??>
        <#assign urlInstance = sri.makeUrlByType(.node["@rest"], "rest", null, "true")>
    </#if>
    <#if .node["@process-key"]??>
        <#assign urlInstance = "/bpms/api/engine/engine/default/process-definition/key/"+.node["@process-key"]+"/submit-form">
        <#assign submissionType = "process">
    </#if>
    <#assign redirectUrl = "#">
    <#if .node["@redirect-url"]??>
        <#assign redirectUrl = sri.makeUrlByType(.node["@redirect-url"], "screen", null, "true")>
    </#if>
    <#assign jsonSrc = ec.getResource().expression(.node["@json-src"],"")>
    <#assign jsonSrcPrefix = jsonSrc.substring(0, 12)> <#--jsonSrcPrefix = "component://" yet-->
    <#assign jsonSrcSufix = jsonSrc.substring(12, jsonSrc.length())>
    <#if jsonSrc.indexOf(jsonSrcPrefix) !=-1 >
    <#-- search for same file in other components override directory to ovveride the form -->
        <#list ec.factory.getComponentInfoList() as comp>
            <#assign compName = comp.name>
            <#assign ovrSrc = jsonSrcPrefix + compName +"/override/" +jsonSrcSufix >
            <#assign isFile = ec.getResource().getLocationReference(ovrSrc).isFile()>
            <#if isFile>
                <#assign jsonSrc = ovrSrc>
                <#break>
            </#if>
        </#list>
    </#if>
    <#assign onErrorExp = .node["@error-text"]!"err.responseText">
    <#assign onSuccessExp = ec.l10n.localize(.node["@success-text"]!"ارسال شد!")>
    <#assign showAlert = .node["@show-alert"]!"true">
    <#assign downloadFile = .node["@download-file"]!"false">
    <#assign fileType = .node["@file-type"]!"Text/Plain">
    <#assign fileName = .node["@file-name"]!"Text/Plain">
    <#assign embededFormReplaceFrom = .node["@embeded-form-replace-from"]!"">
    <#assign embededFormReplaceTo = .node["@embeded-form-replace-to"]!"">
    <#assign formId>${ec.getResource().expandNoL10n(.node["@name"], "")}<#if sectionEntryIndex?has_content>_${sectionEntryIndex}</#if></#assign>
<div class="formio-trn-rtl">
    <div id="${formId}"></div>
    <input id="formioSessionToken" value="${ec.web.sessionToken}" hidden/>
    <input id="formioBasicToken" value="${ec.user.getPreference('basicToken')}" hidden/>
</div>
<script>
    var _scope${formId} = {};
    _scope${formId}.formSchema = <#if ec.getResource().getLocationReference(jsonSrc).isFile() ><#include jsonSrc parse=false encoding="UTF-8"> <#else> {}</#if> ;
    _scope${formId}.formOptions = {};
        <#if .node["@token-name"]??>_scope${formId}.basicToken = "Basic " + "${ec.user.getPreference(.node["@token-name"])}";</#if>
        <#if .node["@token-bearer"]??>_scope${formId}.bearerToken = "Bearer " + "${ec.user.getPreference(.node["@token-bearer"])}";</#if>
        <#if .node["@token-value"]??>_scope${formId}.basicToken = "${.node["@token-value"]}";</#if>
        <#if .node["@embeded-form-replace-from"]??>_scope${formId}.embededFormReplaceFrom = "${.node["@embeded-form-replace-from"]}";</#if>
        <#if .node["@embeded-form-replace-to"]??>_scope${formId}.embededFormReplaceTo = "${.node["@embeded-form-replace-to"]}";</#if>
    _scope${formId}.formOptions.i18n = {};
    <#if .node["@process-key"]??>
        <#assign params = {"processDefinitionId":.node["@process-key"],"processDefinitionKey":.node["@process-key"] }>
        _scope${formId}.newBusinessKey = "${ec.service.sync().name('tasklistService.new#businessKey').parameters(params).call().newBusinessKey}";
    </#if>
    _scope${formId}.formOptions.i18n.en = <#include "component://trn/template/screen-macro/formi18n.json" parse=false encoding="UTF-8">;
        <#if (submissionType=="pv" || submissionType=="process")>
        var convertToPv = function (dataForConvert) {
            var data = {variables: {}};
            for (var variable in dataForConvert) {
                if (dataForConvert.hasOwnProperty(variable)) {
                    data.variables[variable] = {value: dataForConvert[variable]};
                }
            }
            if (dataForConvert.hasOwnProperty("businessKey")) {
                data.businessKey = dataForConvert.businessKey;
            }
            return data;
        };
        </#if>
    function getArrayDuplicates(arra1) {
        var object = {};
        var result = [];
        arra1.forEach(function (item) {
            if (!object[item])
                object[item] = 0;
            object[item] += 1;
        });

        for (var prop in object) {
            if (object[prop] >= 2) {
                result.push(prop);
            }
        }
        return result;
    };

    function iterateForEmbedFormUi() {
        FormioUtils.eachComponent(_scope${formId}.formSchema.components, function (component) {
            if (component.type == "embedForm") {
                var embededFormReplaceFrom = _scope${formId}.embededFormReplaceFrom;
                var embededFormReplaceTo = _scope${formId}.embededFormReplaceTo;
                if (embededFormReplaceFrom != undefined && embededFormReplaceFrom != "" && embededFormReplaceTo != "" && embededFormReplaceTo != undefined) {
                    component.formSrc = component.formSrc.replace(embededFormReplaceFrom, embededFormReplaceTo)
                }
                if (!component.formSrc) {
                    alert(component.key + " : your embedded form doesn't have formSrc property");
                } else {
                    if (component.formSrc.substring(0, 12) == "component://") {
                        $.ajax({
                            url: window.location.origin + "/rest/s1/trn/form/jsonString?jsonSrcLocation=" + component.formSrc,
                            type: "get",
                            contentType: "application/json",
                            async: false,
                            timeout: 7000,
                            beforeSend: function (xhr) {
                                xhr.setRequestHeader("Authorization", _scope${formId}.basicToken);
                            }
                        }).done(function success(response) {
                            component.type = "fieldset";
                            component.persistent = false;
                            component.input = false;
                            component.hideLabel = false;
                            component.customClass = "fieldset-nostyle";
                            if (component.componentKey) {
                                component.components = [FormioUtils.getComponent(JSON.parse(response.jsonString).components, component.componentKey, true)];
                            } else {
                                component.components = JSON.parse(response.jsonString).components;
                            }
                            iterateForEmbedFormUi();
                        }).fail(function error(err) {
                            console.log("error getting embedded form ", err);
                            alert("Internal Error - error getting embedded form " + err);
                        });
                    } else {
                        alert("invalid formSrc in your embedded form component : --> " + component.formSrc + " -- it should start with  formio:/");
                    }
                }
            }
        }, true);
    }

    iterateForEmbedFormUi();
        <#include "component://trn/template/screen-macro/formRendererContrib.js" parse=false encoding="UTF-8">
    <#--<#include "component://trn/template/screen-macro/preValueBtn.js" parse=false encoding="UTF-8">-->

    <#--_scope${formId}.formSchema.components.push(preValueBtn);-->
    <#if (instancePurpose?? && instancePurpose =='dev')>
        var previewDataStructuredBtn = {
            "label": "preview Data Structure",
            "showValidations": false,
            "disableOnInvalid": false,
            "disabled": false,
            "clearOnHide": false,
            "mask": false,
            "action": "custom",
            "theme": "primary",
            "type": "button",
            "hidden": false,
            "key": "previewDataStructuredBtn",
            "input": false,
            "properties": [
                {
                    "key": "",
                    "value": ""
                }
            ],
            "tags": [],
            "custom": "\n" +
            "var xmlString = \"<div class='row'><div class=\\\"col-md-6\\\"> <h4>submission data</h4> <textarea style=\\\"direction: ltr;min-height: 470px;width:97%;\\\" id=\\\"subPrev\\\" /> </div> <div class=\\\"col-md-6\\\"> <h4>default data</h4> <textarea style=\\\"direction: ltr;min-height: 470px;width:97%;\\\" id=\\\"defPrev\\\" /> </div></div>\";\n" +
            "var el =$(document.createElement(\"div\")).html(xmlString);\n" +
            "el.find('#defPrev').val(JSON.stringify(form.submission.data.preValue,null,4));\n" +
            "var subD =JSON.parse(JSON.stringify(form.submission.data)); \n" +
            "delete subD.preValue; \n" +
            "el.find('#subPrev').val(JSON.stringify(subD,null,4));\n" +
            "swal({content: el[0], className: \"swal-width-1124\"});"
        };
        _scope${formId}.formSchema.components.push(previewDataStructuredBtn);

    </#if>
    _scope${formId}Form = null;
    Formio.createForm(document.getElementById('${formId}'), _scope${formId}.formSchema, _scope${formId}.formOptions).then(function (form) {
        _scope${formId}Form = form;
        form.language = "en";
        form.nosubmit = true;
        var apiNameList = [];
        FormioUtils.eachComponent(form.components, function (component) {
            apiNameList.push(component.component.key);
            //TODO: move the below code to formio contrib
            if (component.component.type === "jalaalidate") {
                if (component.component.defaultValue) {
                    component.setValue(new Date());
                }
            }
        }, true);

        var apiNameListDup = getArrayDuplicates(apiNameList);
        if (apiNameListDup.length > 0) {
            alert("Error: Duplicate ApiName in form -->" + apiNameListDup.toString());
            console.log("Error: Duplicate ApiName in form -->" + apiNameListDup.toString());
        }
        var defValueAndPV = {};
        <#if .node["@default-value"]??>
            defValueAndPV = JSON.parse('${ec.getResource().expression("groovy.json.JsonOutput.toJson("+.node["@default-value"]+")","").replaceAll("'","\\\\'").replaceAll("\\\\n"," ").replaceAll('\\\\"',"\\\\'")}');
            defValueAndPV.preValue = JSON.parse(JSON.stringify(defValueAndPV));
            form.submission = {data: defValueAndPV};
        </#if>
        form.on('render', function () {
            <#if .node["@default-value"]??>
                setTimeout(function () {
                    $("button[name='data[preValueBtn]']").click();
                }, 250);
            </#if>
            <#include "component://trn/template/screen-macro/fixModalJsonForm.js" parse=false encoding="UTF-8">
        });
        form.on('submit', function (submission) {
            delete submission.data.preValue;
                _scope${formId}.submissionData = <#if .node["@submission-wrapper"]??>{'${.node["@submission-wrapper"]}': submission.data}<#else>submission.data</#if>;
                <#if !(.node["@require-authentication"]?? && .node["@require-authentication"]=="anonymous-all")>_scope${formId}.submissionData.moquiSessionToken = "${(ec.getWeb().sessionToken)!}";</#if>
                <#if .node["@token-name"]??>
                    _scope${formId}.submissionData.basicToken = _scope${formId}.basicToken;
                </#if>
                <#if .node["@process-key"]??>
                    _scope${formId}.submissionData.businessKey = _scope${formId}.newBusinessKey;
                </#if>
            $.ajax({
                url: "${urlInstance!'#'}",
                type: "post",
                contentType: "application/json",
                <#if .node["@token-name"]?? || .node["@token-value"]??>

                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Authorization", _scope${formId}.basicToken);
                    },
                </#if>
            data:JSON.stringify(<#if (submissionType=="pv" || submissionType=="process")>convertToPv(_scope${formId}.submissionData)
                    <#else>_scope${formId}.submissionData</#if>),
                    success
        :

            function success(response) {
                var redirectURL = null;
                var sccssMsg = "${onSuccessExp}";
                var showAlert = "${showAlert}";
                var fileType = "${fileType}";
                var fileName = "${fileName}";
                var downloadFile = "${downloadFile}";
                console.log("downloadFile", downloadFile);
                console.log("fileType", fileType);
              <#if .node["@transition"]??>
                  <#if .node["@redirect-url"]??>
                        redirectURL = "${redirectUrl}";
                  <#else>
                        if (response && response.toString().indexOf('<a id="history-menu-link') != -1) {
                            if (response.split('<a id="history-menu-link')[1].split('<li><a href="')[1].split('">')[0]) {
                                redirectURL = response.split('<a id="history-menu-link')[1].split('<li><a href="')[1].split('">')[0];
                            } else {
                                redirectURL = "${redirectUrl}";
                            }
                        } else {
                            if (response.toString().indexOf('http://') != -1) {
                                redirectURL = response.toString();
                            } else {
                                redirectURL = null;
                            }
                        }
                  </#if>
              <#elseif .node["@rest"]??>
                    redirectURL = "${redirectUrl}";
              </#if>
                if (showAlert == undefined || showAlert != 'false') {
                    swal("تراکنش موفق!", sccssMsg, "success", {button: "قبول"}).then(function (vall) {
                        if (redirectURL) {
                            window.location.replace(redirectURL);
                        } else {
                            window.location.reload();
                        }
                    });
                } else {
                    if (redirectURL) {
                        window.location.replace(redirectURL);
                    } else {
                        // window.location.reload();
                    }
                }
                if (downloadFile == 'true' || downloadFile == true) {
                    if (fileType == 'url') {
                        window.open(encodeURIComponent(response), '_blank')
                    } else {
                        let element = document.createElement('a');
                        element.setAttribute('href', 'data:' + fileType + ';charset=utf-8,' + encodeURIComponent(response));
                        element.setAttribute('download', fileName);
                        element.style.display = 'none';
                        document.body.appendChild(element);
                        element.click();
                        document.body.removeChild(element);
                    }
                }

            }
        }).
            fail(function error(err) {
                console.log("error on submit", err.responseText);
                var errMsg = ${onErrorExp};
                try {
                    errMsg = JSON.parse(errMsg);
                } catch (e) {
                    errMsg = errMsg;
                }
                if (errMsg.hasOwnProperty("errors")) {
                    errMsg = errMsg.errors;
                }
                if (typeof errMsg === "object") {
                    errMsg = JSON.stringify(errMsg);
                }
                swal("خطا!", errMsg, "error", {button: "قبول"});
            });
        });
    });

</script>
    <#if sri.doBoundaryComments()><!-- END   json-form-single[@name=${.node["@name"]}] --></#if>
</#macro>
<#macro "report-view">
    <#assign dashHost = ec.service.sync().name('trnService.get#prop').parameters({"propName":"metabase_host"}).call().value>
    <#assign dashPort = ec.service.sync().name('trnService.get#prop').parameters({"propName":"metabase_port"}).call().value>
    <#assign dashUrl = ec.web.getRequestUrl().split('//')[0]+'//'+ec.web.getRequestUrl().split('/')[2]+'/dashboard/metabase'>
    <#assign dashReportType = .node["@report-type"]!'question'>
    <#assign dashReportId = .node["@report-id"]?number>
    <#assign dashStyle= .node["@style"]!''>
    <#if dashReportType=='question'>
        <#assign dashEmbedUrl = '/embed/question/'>
    <#else>
        <#assign dashEmbedUrl = '/embed/dashboard/'>
    </#if>
    <#assign payload = { "resource": { dashReportType: dashReportId},"params": {}}>
    <#assign token = ec.service.sync().name('dashboardService.create#jwtToken').parameters({"payload":payload}).call().token>
    <#assign dashIframeUrl = dashUrl + dashEmbedUrl + token + '#bordered=true&amp;titled=true'>
    <#assign dashWidth = .node["@width"]!"100%">
    <#assign dashHeight = .node["@height"]!"600px">
    <iframe src="${dashIframeUrl}"
            width="${dashWidth}"
            height="${dashHeight}"
            frameborder="0"
            allowtransparency
            class="${dashStyle}"></iframe>
</#macro>

<#macro "export-download">
    <#assign templatePath = ec.getResource().expression(.node["@template-path"],"")>
    <#assign variables = .node["@variables"]>
    <#assign name = .node["@name"]>
    <#assign label = .node["@label"]>
    <#assign dashStyle= .node["@style"]!''>
<script>
    function dlExport${name}() {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var hiddenElement = document.createElement('a');
                hiddenElement.href = 'data:application/pdf;base64,' + JSON.parse(xhttp.responseText).res;
                hiddenElement.target = '_blank';
                hiddenElement.download = 'trn-export.pdf';
                hiddenElement.click();
            }
        };
        xhttp.open("POST", window.location.origin + "/rest/s1/trn/common/export/pdf", false);
        xhttp.withCredentials = true;
        xhttp.setRequestHeader('Content-Type', 'application/json');
        xhttp.setRequestHeader('Accept', 'application/json');
        xhttp.send(JSON.stringify({
            moquiSessionToken: "${ec.web.sessionToken}",
            reportPath: '${templatePath}',
            pdfVariables: JSON.parse('${ec.getResource().expression("groovy.json.JsonOutput.toJson("+variables+")","").replaceAll("'","\\\\'").replaceAll("\\\\n"," ").replaceAll('\\\\"',"\\\\'")}')
        }));
    }
</script>
    <button onclick="dlExport${name}()" class="btn btn-primary ${style}">${label}</button>
</#macro>

<#macro "display-entity">
    <#assign fieldValue = sri.getFieldEntityValue(.node)!/>
    <#assign fieldColor = sri.getFieldEntityColor(.node)!/>
    <#assign dispHidden = (!.node["@also-hidden"]?has_content || .node["@also-hidden"] == "true") && !(skipForm!false)>
    <#t><span class="text-inline<#if .node["@style"]?has_content> ${ec.getResource().expand(.node["@style"], "")}</#if>"
              style="<#if fieldColor?has_content> color:${fieldColor}</#if>"><#if fieldValue?has_content><#if .node["@encode"]! == "false">${fieldValue!"&nbsp;"}<#else>${(fieldValue!" ")?html?replace("\n", "<br>")}</#if><#else>
    &nbsp;</#if></span>
<#-- don't default to fieldValue for the hidden input value, will only be different from the entry value if @text is used, and we don't want that in the hidden value -->
    <#t><#if dispHidden><input type="hidden" id="<@fieldId .node/>" name="<@fieldName .node/>"
                               value="${sri.getFieldValuePlainString(.node?parent?parent, "")?html}"<#if ownerForm?has_content>
                               form="${ownerForm}"</#if>></#if>
</#macro>
