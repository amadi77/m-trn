function iterateForEmbedForm() {
    FormioUtils.eachComponent(_scope.formioJSON.components, function (component) {
        if (component.type == "embedForm") {
            if (!component.formSrc) {
                alert(component.key + " : your embedded form doesn't have formSrc property");
            }else{
                if(component.formSrc.substring(0,8)=="formio:/"){
                    $.ajax({url:_scope.hostName + _scope.paContextPath + component.formSrc.substring(7),
                        type:"get",
                        contentType: "application/json",
                        async:false,
                        timeout:7000,
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader("Authorization", _scope.basicToken);
                        }
                    }).done(function success(response) {
                        component.type = "fieldset";
                        component.persistent = false;
                        component.input = false;
                        component.hideLabel = false;
                        component.customClass = "fieldset-nostyle";
                        if (component.componentKey) {
                            component.components = [FormioUtils.getComponent(response.components,component.componentKey,true)];
                        } else {
                            component.components = response.components;
                        }
                        iterateForEmbedForm();
                    }).fail(function error(err) {
                        console.log("error getting embedded form ", err.responseText);
                        alert("Internal Error - error getting embedded form "+ err.responseText);
                    });
                } else {
                    alert("invalid formSrc in your embedded form component : --> "+component.formSrc+" -- it should start with  formio:/");
                }
            }
        }
    }, true);
}
