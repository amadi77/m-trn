
<#include "../../../../template/screen-macro/DefaultScreenMacros.html.ftl"/>
<#--TODO: add if the timeZone is tehran then shows the persian-datepicker -->

<#macro "date-period">
    <#assign id><@fieldId .node/></#assign>
    <#assign curFieldName><@fieldName .node/></#assign>
    <#assign fvOffset = ec.getContext().get(curFieldName + "_poffset")!>
    <#assign fvPeriod = ec.getContext().get(curFieldName + "_period")!?lower_case>
    <#assign fvDate = ec.getContext().get(curFieldName + "_pdate")!"">
    <#assign allowEmpty = .node["@allow-empty"]!"true">
<div class="date-period">
    <select name="${curFieldName}_poffset" id="${id}_poffset"<#if .node?parent["@tooltip"]?has_content> data-toggle="tooltip" title="${ec.getResource().expand(.node?parent["@tooltip"], "")}"</#if><#if ownerForm?has_content> form="${ownerForm}"</#if>>
        <#if (allowEmpty! != "false")>
            <option value="">&nbsp;</option>
        </#if>
        <option value="0"<#if fvOffset == "0"> selected="selected"</#if>>${ec.getL10n().localize("This")}</option>
        <option value="-1"<#if fvOffset == "-1"> selected="selected"</#if>>${ec.getL10n().localize("Last")}</option>
        <option value="1"<#if fvOffset == "1"> selected="selected"</#if>>${ec.getL10n().localize("Next")}</option>
        <option value="-2"<#if fvOffset == "-2"> selected="selected"</#if>>-2</option>
        <option value="2"<#if fvOffset == "2"> selected="selected"</#if>>+2</option>
        <option value="-3"<#if fvOffset == "-3"> selected="selected"</#if>>-3</option>
        <option value="3"<#if fvOffset == "3"> selected="selected"</#if>>+3</option>
    </select>

    <select name="${curFieldName}_period" id="${id}_period"<#if .node?parent["@tooltip"]?has_content> data-toggle="tooltip" title="${ec.getResource().expand(.node?parent["@tooltip"], "")}"</#if><#if ownerForm?has_content> form="${ownerForm}"</#if>>
        <#if (allowEmpty! != "false")>
            <option value="">&nbsp;</option>
        </#if>
        <option value="day" <#if fvPeriod == "day"> selected="selected"</#if>>${ec.getL10n().localize("Day")}</option>
        <option value="7d" <#if fvPeriod == "7d"> selected="selected"</#if>>7 ${ec.getL10n().localize("Days")}</option>
        <option value="30d" <#if fvPeriod == "30d"> selected="selected"</#if>>30 ${ec.getL10n().localize("Days")}</option>
        <option value="week" <#if fvPeriod == "week"> selected="selected"</#if>>${ec.getL10n().localize("Week")}</option>
        <option value="month" <#if fvPeriod == "month"> selected="selected"</#if>>${ec.getL10n().localize("Month")}</option>
        <option value="quarter" <#if fvPeriod == "quarter"> selected="selected"</#if>>${ec.getL10n().localize("Quarter")}</option>
        <option value="year" <#if fvPeriod == "year"> selected="selected"</#if>>${ec.getL10n().localize("Year")}</option>
        <option value="7r" <#if fvPeriod == "7r"> selected="selected"</#if>>+/-7d</option>
        <option value="30r" <#if fvPeriod == "30r"> selected="selected"</#if>>+/-30d</option>
    </select>
    <div class="input-group date" id="${id}_pdate">

        <input type="text" class="form-control" name="${curFieldName}_pdate" value="${fvDate?html}" size="10" maxlength="10"<#if ownerForm?has_content> form="${ownerForm}"</#if>>
        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
    </div>
    <span id="${id}_pdateP" class="input-group-addon persian-datepicker-override-picker" ><span class="glyphicon glyphicon-calendar"></span></span>
    <span id="${id}_pdatePP" class="input-group-addon persian-datepicker-override-show"></span>
    <script>
        if($('#${id}_pdate > input').val()!=undefined && $('#${id}_pdate > input').val()!=''){
            $('#${id}_pdatePP').html(new persianDate(new Date($('#${id}_pdate > input').val())).format("YYYY/MM/DD"));}

        $("#${id}_poffset").select2({ }); $("#${id}_period").select2({ });
        $('#${id}_pdate').datetimepicker({toolbarPlacement:'top', showClose:true, showClear:true, showTodayButton:true, useStrict:true, defaultDate: '${fvDate?html}' && moment('${fvDate?html}','YYYY-MM-DD'), format:'YYYY-MM-DD', extraFormats:['l', 'L', 'YYYY-MM-DD'], locale:"${ec.getUser().locale.toLanguageTag()}"});
        $('#${id}_pdateP').persianDatepicker({onSelect:function (unixDate) {
            var selectedDa= new Date(unixDate);
            $('#${id}_pdatePP').html(new persianDate(selectedDa).format("YYYY/MM/DD"));
            $('#${id}_pdate > input').val(selectedDa.getFullYear()+"-"+(parseInt(selectedDa.getMonth())+1)+"-"+selectedDa.getDate());
        },format: 'YYYY-MM-DD', toolbox:{ calendarSwitch:{enabled: false},submitButton:{enabled: true}}});
    </script>
</div>
</#macro>

<#macro "date-find">
    <#if .node["@type"]! == "time"><#assign size=9><#assign maxlength=13><#assign defaultFormat="HH:mm"><#assign extraFormatsVal = "['LT', 'LTS', 'HH:mm']">
    <#elseif .node["@type"]! == "date"><#assign size=10><#assign maxlength=10><#assign defaultFormat="yyyy-MM-dd"><#assign extraFormatsVal = "['l', 'L', 'YYYY-MM-DD']">
    <#else><#assign size=16><#assign maxlength=23><#assign defaultFormat="yyyy-MM-dd HH:mm"><#assign extraFormatsVal = "['YYYY-MM-DD HH:mm', 'YYYY-MM-DD HH:mm:ss', 'MM/DD/YYYY HH:mm']">
    </#if>
    <#assign datepickerFormat><@getMomentDateFormat .node["@format"]!defaultFormat/></#assign>
    <#assign curFieldName><@fieldName .node/></#assign>
    <#assign fieldValueFrom = ec.getL10n().format(ec.getContext().get(curFieldName + "_from")!?default(.node["@default-value-from"]!""), defaultFormat)>
    <#assign fieldValueThru = ec.getL10n().format(ec.getContext().get(curFieldName + "_thru")!?default(.node["@default-value-thru"]!""), defaultFormat)>
    <#assign id><@fieldId .node/></#assign>
<span class="form-date-find">
      <span>${ec.getL10n().localize("From")}&nbsp;</span>
    <#if .node["@type"]! != "time">
        <div class="input-group date" id="${id}_from">
            <input type="text" class="form-control" name="${curFieldName}_from" value="${fieldValueFrom?html}" size="${size}" maxlength="${maxlength}"<#if .node?parent["@tooltip"]?has_content> data-toggle="tooltip" title="${ec.getResource().expand(.node?parent["@tooltip"], "")}"</#if><#if ownerForm?has_content> form="${ownerForm}"</#if>>
            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
        </div>
        <span id="${id}_fromP" class="input-group-addon persian-datepicker-override-picker" ><span class="glyphicon glyphicon-calendar"></span></span>
        <span id="${id}_fromPP" class="input-group-addon persian-datepicker-override-show"></span>

        <script>
            if($('#${id}_from > input').val()!=undefined && $('#${id}_from > input').val()!=''){
                $('#${id}_fromPP').html(new persianDate(new Date($('#${id}_from > input').val())).format("YYYY/MM/DD"));}

            $('#${id}_from').datetimepicker({toolbarPlacement:'top', showClose:true, showClear:true, useStrict:true, showTodayButton:true, defaultDate:'${fieldValueFrom?html}' && moment('${fieldValueFrom?html}','${datepickerFormat}'), format:'${datepickerFormat}', extraFormats:${extraFormatsVal}, stepping:5, locale:"${ec.getUser().locale.toLanguageTag()}"});
            $('#${id}_fromP').persianDatepicker({onSelect:function (unixDate) {
                var selectedDa= new Date(unixDate);
                $('#${id}_fromPP').html(new persianDate(selectedDa).format("YYYY/MM/DD"));
                $('#${id}_from > input').val(selectedDa.getFullYear()+"-"+(parseInt(selectedDa.getMonth())+1)+"-"+selectedDa.getDate());
            },format: 'YYYY-MM-DD', toolbox:{ calendarSwitch:{enabled: false},submitButton:{enabled: true}}});
        </script>
    <#else>
        <input type="text" class="form-control" pattern="^(?:(?:([01]?\d|2[0-3]):)?([0-5]?\d):)?([0-5]?\d)$"
               name="${curFieldName}_from" value="${fieldValueFrom?html}" size="${size}" maxlength="${maxlength}"<#if .node?parent["@tooltip"]?has_content> data-toggle="tooltip" title="${ec.getResource().expand(.node?parent["@tooltip"], "")}"</#if><#if ownerForm?has_content> form="${ownerForm}"</#if>>
    </#if>
    </span>
<span class="form-date-find">
      <span>${ec.getL10n().localize("Thru")}&nbsp;</span>
    <#if .node["@type"]! != "time">
        <div class="input-group date" id="${id}_thru">
            <input type="text" class="form-control" name="${curFieldName}_thru" value="${fieldValueThru?html}" size="${size}" maxlength="${maxlength}"<#if .node?parent["@tooltip"]?has_content> data-toggle="tooltip" title="${ec.getResource().expand(.node?parent["@tooltip"], "")}"</#if><#if ownerForm?has_content> form="${ownerForm}"</#if>>
            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
        </div>
    <span id="${id}_thruP" class="input-group-addon persian-datepicker-override-picker" ><span class="glyphicon glyphicon-calendar"></span></span>
    <span id="${id}_thruPP" class="input-group-addon persian-datepicker-override-show"></span>
        <script>
            if($('#${id}_thru > input').val()!=undefined && $('#${id}_thru > input').val()!=''){
                $('#${id}_thruPP').html(new persianDate(new Date($('#${id}_thru > input').val())).format("YYYY/MM/DD"));}

            $('#${id}_thru').datetimepicker({toolbarPlacement:'top', showClose:true, showClear:true, useStrict:true, showTodayButton:true, defaultDate:'${fieldValueThru?html}' && moment('${fieldValueThru?html}','${datepickerFormat}'), format:'${datepickerFormat}', extraFormats:${extraFormatsVal}, stepping:5, locale:"${ec.getUser().locale.toLanguageTag()}"});
            $('#${id}_thruP').persianDatepicker({onSelect:function (unixDate) {
                var selectedDa= new Date(unixDate);
                $('#${id}_thruPP').html(new persianDate(selectedDa).format("YYYY/MM/DD"));
                $('#${id}_thru > input').val(selectedDa.getFullYear()+"-"+(parseInt(selectedDa.getMonth())+1)+"-"+selectedDa.getDate());
            },format: 'YYYY-MM-DD', toolbox:{ calendarSwitch:{enabled: false},submitButton:{enabled: true}}});
        </script>
    <#else>
        <input type="text" class="form-control" pattern="^(?:(?:([01]?\d|2[0-3]):)?([0-5]?\d):)?([0-5]?\d)$"
               name="${curFieldName}_thru" value="${fieldValueThru?html}" size="${size}" maxlength="${maxlength}"<#if .node?parent["@tooltip"]?has_content> data-toggle="tooltip" title="${ec.getResource().expand(.node?parent["@tooltip"], "")}"</#if><#if ownerForm?has_content> form="${ownerForm}"</#if>>
    </#if>
    </span>
</#macro>

<#macro "date-time">
    <#assign dtFieldNode = .node?parent?parent>
    <#assign javaFormat = .node["@format"]!>
    <#assign validationClasses = formInstance.getFieldValidationClasses(dtFieldNode["@name"])>
    <#if !javaFormat?has_content>
        <#if .node["@type"]! == "time"><#assign javaFormat="HH:mm">
        <#elseif .node["@type"]! == "date"><#assign javaFormat="yyyy-MM-dd">
        <#else><#assign javaFormat="yyyy-MM-dd HH:mm"></#if>
    </#if>
    <#assign datepickerFormat><@getMomentDateFormat javaFormat/></#assign>
    <#assign fieldValue = sri.getFieldValueString(dtFieldNode, .node["@default-value"]!"", javaFormat)>

    <#assign id><@fieldId .node/></#assign>

    <#if .node["@type"]! == "time"><#assign size=9><#assign maxlength=13><#assign extraFormatsVal = "['LT', 'LTS', 'HH:mm']">
    <#elseif .node["@type"]! == "date"><#assign size=10><#assign maxlength=10><#assign extraFormatsVal = "['l', 'L', 'YYYY-MM-DD']">
    <#else><#assign size=16><#assign maxlength=23><#assign extraFormatsVal = "['YYYY-MM-DD HH:mm', 'YYYY-MM-DD HH:mm:ss', 'MM/DD/YYYY HH:mm']"></#if>
    <#assign size = .node["@size"]!size>
    <#assign maxlength = .node["@max-length"]!maxlength>

    <#if .node["@type"]! != "time">
    <div class="input-group date" id="${id}">
        <input type="text" class="form-control<#if validationClasses?contains("required")> required</#if>"<#if validationClasses?contains("required")> required="required"</#if> name="<@fieldName .node/>" value="${fieldValue?html}" size="${size}" maxlength="${maxlength}"<#if .node?parent["@tooltip"]?has_content> data-toggle="tooltip" title="${ec.getResource().expand(.node?parent["@tooltip"], "")}"</#if><#if ownerForm?has_content> form="${ownerForm}"</#if>>
        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
    </div>
    <span id="${id}P" class="input-group-addon persian-datepicker-override-picker" ><span class="glyphicon glyphicon-calendar"></span></span>
    <span id="${id}PP" class="input-group-addon persian-datepicker-override-show"></span>
    <script>
        if($('#${id} > input').val()!=undefined && $('#${id} > input').val()!=''){
            $('#${id}PP').html(new persianDate(new Date($('#${id} > input').val())).format("YYYY/MM/DD HH:mm"));}

        $('#${id}').datetimepicker({toolbarPlacement:'top', showClose:true, showClear:true, showTodayButton:true,
            useStrict:true, defaultDate: '${fieldValue?html}' && moment('${fieldValue?html}','${datepickerFormat}'),
            format:'${datepickerFormat}', extraFormats:${extraFormatsVal}, stepping:5, locale:"${ec.getUser().locale.toLanguageTag()}"});
        $('#${id}').on("dp.change", function() {
            var jqEl = $('#${id}');
            jqEl.val(jqEl.find("input").first().val());
            $('#${id}PP').html(new persianDate(new Date(jqEl.find("input").first().val())).format("YYYY/MM/DD HH:mm"));
            jqEl.trigger("change");
        });
        $('#${id}P').persianDatepicker({onSelect:function (unixDate) {
            var selectedDa= new Date(unixDate);
            $('#${id}PP').html(new persianDate(selectedDa).format("YYYY/MM/DD HH:mm"));
            $('#${id},#${id} > input').val(selectedDa.getFullYear()+"-"+(parseInt(selectedDa.getMonth())+1)+"-"+selectedDa.getDate() +" "+selectedDa.getHours()+":"+selectedDa.getMinutes()+":"+selectedDa.getSeconds());
        },format: 'YYYY-MM-DD', toolbox:{ calendarSwitch:{enabled: false},submitButton:{enabled: true},timePicker:{enabled: true}}});

    </script>
    <#else>
    <#-- datetimepicker does not support time only, even with plain HH:mm format; use a regex to validate time format -->
    <input type="text" class="form-control" pattern="^(?:(?:([01]?\d|2[0-3]):)?([0-5]?\d):)?([0-5]?\d)$" name="<@fieldName .node/>" value="${fieldValue?html}" size="${size}" maxlength="${maxlength}"<#if .node?parent["@tooltip"]?has_content> data-toggle="tooltip" title="${ec.getResource().expand(.node?parent["@tooltip"], "")}"</#if><#if ownerForm?has_content> form="${ownerForm}"</#if>>
    </#if>
</#macro>
