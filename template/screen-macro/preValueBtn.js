
var preValueBtn =  {
    "label": "preValueBtn",
    "showValidations": false,
    "disableOnInvalid": false,
    "disabled": false,
    "clearOnHide": false,
    "mask": false,
    "action": "custom",
    "theme": "primary",
    "type": "button",
    "hidden": true,
    "key": "preValueBtn",
    "input": true,
    "properties": [
        {
            "key": "",
            "value": ""
        }
    ],
    "tags": [],
    "custom": "utils.eachComponent(components, function (comp) { if ((typeof comp.setValue === 'function' && utils.getValue({data: submission.data.preValue}, comp.key) != null && !utils.isLayoutComponent(comp)) || (typeof comp.setValue === 'function' && utils.getValue({data: submission.data.preValue}, comp.key) != null && utils.isLayoutComponent(comp) && comp.type == \"datagrid\")) { if (comp.type != \"datagrid\") { if (comp.type != 'file') { comp.setValue(utils.getValue({data: submission.data.preValue}, comp.key)); } } else { if (utils.getValue({data: submission.data.preValue}, comp.key).length == 1) { var temp = utils.getValue({data: submission.data.preValue}, comp.key)[0]; var flag = false; for (var itm in temp) { if (temp.hasOwnProperty(itm)) { if (temp[itm] && temp[itm].length > 0) { flag = true; break; } } } if (flag) { if (comp.type != 'file') { comp.setValue(utils.getValue({data: submission.data.preValue}, comp.key)); } } } else if (utils.getValue({data: submission.data.preValue}, comp.key).length > 1) { if (comp.type != 'file') { comp.setValue(utils.getValue({data: submission.data.preValue}, comp.key)); } } } } }, true);"
};
var fillFormBtn =  {
    "label": "افزودن قالب جدید",
    "showValidations": false,
    "disableOnInvalid": false,
    "disabled": false,
    "clearOnHide": false,
    "mask": false,
    "action": "custom",
    "theme": "primary",
    "type": "button",
    "hidden": false,
    "key": "fillFormBtn",
    "input": true,
    "properties": [
        {
            "key": "",
            "value": ""
        }
    ],
    "tags": [],
    "custom": "window.open(window.location.origin+'/apps/formTemplate?formKey='+data.formKey,'_blank');"
};
