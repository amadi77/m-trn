import net.sf.jasperreports.engine.JREmptyDataSource
import net.sf.jasperreports.engine.JasperExportManager
import net.sf.jasperreports.engine.JasperFillManager
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource
import org.apache.commons.io.FilenameUtils

def runtimePath = org.moqui.util.SystemBinding.getPropOrEnv('moqui.runtime')
String fontPath = runtimePath+"/component/m-trn/screen/trnstatic/fonts/BNazanin.ttf"
String report = runtimePath+"/component"+reportPath
String fileExtension = FilenameUtils.getExtension(report)
if(fileExtension != "jasper") {
    throw new Exception("report file extension must be.jasper but extension is --> "+fileExtention.toString())
}
JasperPrint jasperPrint
pdfVariables.put("contextPath", runtimePath)
Map<String, Object> fields = new HashMap<String, Object>()
for (itm in pdfVariables){
    if(itm.value instanceof List){
//            the old way tp handle list
//            fields.put(itm.key, new JRMapCollectionDataSource(itm.value))
        fields.put(itm.key, (List) itm.value)
    }else{
        fields.put(itm.key, itm.value)
    }
}
jasperPrint = JasperFillManager.fillReport(report, fields, new JREmptyDataSource())
jasperPrint.getDefaultStyle().setPdfFontName(fontPath)
jasperPrint.getDefaultStyle().setPdfEncoding("Identity-H")
jasperPrint.getDefaultStyle().setPdfEmbedded(true)

res2 = JasperExportManager.exportReportToPdf(jasperPrint)
res = res2.encodeBase64().toString()

