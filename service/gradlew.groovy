
def cmds = []
cmds.add(ec.ecfi.runtimePath + "/../gradlew.bat")
cmds.add(cmd) //getComponent,getDepends,gitPullAll,gitStatusAll,gitUpstreamAll,cleanPullLoad
if(component)
    cmds.add("-Pcomponent="+component)
if(locationType)
    cmds.add("-PlocationType="+locationType) //git,current,release

println cmds.toString()
ProcessBuilder pb = new ProcessBuilder(cmds)
pb.directory(new File(ec.ecfi.runtimePath + "/.."))
gradlewProcess = pb.inheritIO().start()