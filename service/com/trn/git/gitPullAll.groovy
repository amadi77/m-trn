import org.ajoberstar.grgit.*

def doGitPullWithStatus(def gitDir) {
    try {
        def curGrgit = Grgit.open(dir: gitDir)
        logger.lifecycle("\nPulling ${gitDir} (branch:${curGrgit.branch.current()?.name}, tracking:${curGrgit.branch.current()?.trackingBranch?.name})")

        def beforeHead = curGrgit.head()
        curGrgit.pull()
        def afterHead = curGrgit.head()
        if (beforeHead == afterHead) {
            logger.lifecycle("Already up-to-date.")
        } else {
            List<Commit> commits = curGrgit.log { range(beforeHead, afterHead) }
            for (Commit commit in commits) logger.lifecycle("- ${commit.getAbbreviatedId(7)} by ${commit.committer?.name}: ${commit.shortMessage}")
        }
    } catch (Throwable t) {
        logger.error(t.message)
    }
}

// framework and runtime
if (file(".git").exists()) { doGitPullWithStatus(file('.').path) }
if (file("runtime/.git").exists()) { doGitPullWithStatus(file('runtime').path) }
// all directories under runtime/component
for (File compDir in file('runtime/component').listFiles().findAll { it.isDirectory() && it.listFiles().find { it.name == '.git' } }) {
    doGitPullWithStatus(compDir.path)
}