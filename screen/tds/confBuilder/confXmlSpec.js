var moquiXmlInitiate = "";

// Create elements object
var moquiElements = {
    "moqui-conf":xmlTemplates.element.moquiConf,
    "init-param":xmlTemplates.element.initParam,
    "default-property":xmlTemplates.element.defaultProperty,
    "tools":xmlTemplates.element.tools,
    "tool-factory":xmlTemplates.element.toolFactory,
    "cache-list":xmlTemplates.element.cacheList,
    "cache":xmlTemplates.element.cache,
    "server-stats":xmlTemplates.element.serverStats,
    "artifact-stats":xmlTemplates.element.artifactStats,
    "webapp-list":xmlTemplates.element.webappList,
    "webapp":xmlTemplates.element.webapp,
    "root-screen":xmlTemplates.element.rootScreen,
    "error-screen":xmlTemplates.element.errorScreen,
    "first-hit-in-visit":xmlTemplates.element.firstHitInVisit,
    "after-request":xmlTemplates.element.afterRequest,
    "before-request":xmlTemplates.element.beforeRequest,
    "after-login":xmlTemplates.element.afterLogin,
    "before-logout":xmlTemplates.element.beforeLogout,
    "after-startup":xmlTemplates.element.afterStartup,
    "before-shutdown":xmlTemplates.element.beforeShutdown,
    "context-param":xmlTemplates.element.contextParam,
    "filter":xmlTemplates.element.filter,
    "listener":xmlTemplates.element.listener,
    "servlet":xmlTemplates.element.servlet,
    "session-config":xmlTemplates.element.sessionConfig,
    "endpoint":xmlTemplates.element.endpoint,
    "artifact-execution-facade":xmlTemplates.element.artifactExecutionFacade,
    "artifact-execution":xmlTemplates.element.artifactExecution,
    "user-facade":xmlTemplates.element.userFacade,
    "login-key":xmlTemplates.element.loginKey,
    "login":xmlTemplates.element.login,
    "transaction-facade":xmlTemplates.element.transactionFacade,
    "transaction-jndi":xmlTemplates.element.transactionJndi,
    "transaction-internal":xmlTemplates.element.transactionInternal,
    "resource-facade":xmlTemplates.element.resourceFacade,
    "resource-reference":xmlTemplates.element.resourceReference,
    "template-renderer":xmlTemplates.element.templateRenderer,
    "script-runner":xmlTemplates.element.scriptRunner,
    "screen-facade":xmlTemplates.element.screenFacade,
    "screen-text-output":xmlTemplates.element.screenTextOutput,
    "service-facade":xmlTemplates.element.serviceFacade,
    "service-location":xmlTemplates.element.serviceLocation,
    "service-type":xmlTemplates.element.serviceType,
    "service-file":xmlTemplates.element.serviceFile,
    "server-jndi":xmlTemplates.element.serverJndi,
    "entity-facade":xmlTemplates.element.entityFacade,
    "datasource":xmlTemplates.element.datasource,
    "inline-jdbc":xmlTemplates.element.inlineJdbc,
    "jndi-jdbc":xmlTemplates.element.jndiJdbc,
    "xa-properties":xmlTemplates.element.xaProperties,
    "inline-other":xmlTemplates.element.inlineOther,
    "load-entity":xmlTemplates.element.loadEntity,
    "load-data":xmlTemplates.element.loadData,
    "database-list":xmlTemplates.element.databaseList,
    "database":xmlTemplates.element.database,
    "dictionary-type":xmlTemplates.element.dictionaryType,
    "database-type":xmlTemplates.element.databaseType,
    "repository-list":xmlTemplates.element.repositoryList,
    "repository":xmlTemplates.element.repository,
    "component-list":xmlTemplates.element.componentList,
    "component-dir":xmlTemplates.element.componentDir,
    "component":xmlTemplates.element.component,
    "password":xmlTemplates.element.passwordMoqui,
    "depends-on":xmlTemplates.element.dependsOnMoqui,
    "description":xmlTemplates.element.description,
    /////////////////// action elements//////////////////
    "actions": xmlTemplates.element.actions,
    "message":xmlTemplates.element.message,
    "service-call": xmlTemplates.element.serviceCall,
    "check-errors":xmlTemplates.element.checkErrors,
    "condition": xmlTemplates.element.condition,
    "set": xmlTemplates.element.set,
    "entity-find-one":xmlTemplates.element.entityFindOne,
    "entity-find":xmlTemplates.element.entityFind,
    "if": xmlTemplates.element.if,
    "else": xmlTemplates.element.else,
    "service-group": xmlTemplates.element.serviceGroup,
    "service-invoke": xmlTemplates.element.serviceInvoke,
    "script": xmlTemplates.element.script,
    "order-map-list": xmlTemplates.element.orderMapList,
    "filter-map-list": xmlTemplates.element.filterMapList,
    "date-filter": xmlTemplates.element.dateFilter,
    "entity-data":xmlTemplates.element.entityData,
    "default-parameters":xmlTemplates.element.defaultParameters,
    "search-from-inputs":xmlTemplates.element.searchFromInputs,
    "econditions":xmlTemplates.element.econditions,
    "having-econditions":xmlTemplates.element.havingEconditions,
    "econdition":xmlTemplates.element.econdition,
    "econdition-object":xmlTemplates.element.econditionObject,
    "select-field":xmlTemplates.element.selectField,
    "order-by":xmlTemplates.element.orderBy,
    "limit-range":xmlTemplates.element.limitRange,
    "limit-view":xmlTemplates.element.limitView,
    "use-iterator":xmlTemplates.element.useIterator,
    "field-map":xmlTemplates.element.fieldMap,
    "entity-find-count":xmlTemplates.element.entityFindCount,
    "entity-find-related-one":xmlTemplates.element.entityFindRelatedOne,
    "entity-find-related":xmlTemplates.element.entityFindRelated,
    "entity-make-value":xmlTemplates.element.entityMakeValue,
    "entity-create":xmlTemplates.element.entityCreate,
    "entity-update":xmlTemplates.element.entityUpdate,
    "entity-delete":xmlTemplates.element.entityDelete,
    "entity-delete-related":xmlTemplates.element.entityDeleteRelated,
    "entity-delete-by-condition":xmlTemplates.element.entityDeleteByCondition,
    "entity-set":xmlTemplates.element.entitySet,
    "entity-sequenced-id-primary":xmlTemplates.element.entitySequencedIdPrimary,
    "entity-sequenced-id-secondary":xmlTemplates.element.entitySequencedIdSecondary,
    "break":xmlTemplates.element.break,
    "continue":xmlTemplates.element.continue,
    "iterate":xmlTemplates.element.iterate,
    "return":xmlTemplates.element.return,
    "assert":xmlTemplates.element.assert,
    "while":xmlTemplates.element.while,
    "then":xmlTemplates.element.then,
    "else-if":xmlTemplates.element.elseIf,
    "or":xmlTemplates.element.or,
    "and":xmlTemplates.element.and,
    "not":xmlTemplates.element.not,
    "compare":xmlTemplates.element.compare,
    "expression":xmlTemplates.element.expression,
    "log":xmlTemplates.element.log,


};
var moquiXmlSpec = {
    onchange: function () {
        //console.log("Ah been chaaanged!");
    },
    validate: function (jsElement) {
        if (typeof(jsElement) == "string") jsElement = Xonomy.xml2js(jsElement);
        var valid = true;
        var elementSpec = this.elements[jsElement.name];
        if (elementSpec.validate) {
            elementSpec.validate(jsElement); //validate the element
        }
        for (var iAttribute = 0; iAttribute < jsElement.attributes.length; iAttribute++) {
            var jsAttribute = jsElement.attributes[iAttribute];
            var attributeSpec = elementSpec.attributes[jsAttribute.name];
            if (attributeSpec.validate) {
                if (!attributeSpec.validate(jsAttribute)) valid = false; //validate the attribute
            }
        }
        for (var iChild = 0; iChild < jsElement.children.length; iChild++) {
            if (jsElement.children[iChild].type == "element") {
                var jsChild = jsElement.children[iChild];
                if (!this.validate(jsChild)) valid = false; //recurse to the child element
            }
        }
        return valid;
    },
    elements: moquiElements
};
