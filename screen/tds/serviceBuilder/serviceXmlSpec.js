var serviceXmlInitiate = "";

// Create elements object
var serviceElements = {
    "services":xmlTemplates.element.services,
    "service":xmlTemplates.element.serviceSpec,
    "in-parameters":xmlTemplates.element.inParameters,
    "out-parameters":xmlTemplates.element.outParameters,
    "implements":xmlTemplates.element.implements,
    "auto-parameters":xmlTemplates.element.autoParameters,
    "exclude":xmlTemplates.element.excludeService,
    "parameter":xmlTemplates.element.parameterService,
    "val-or":xmlTemplates.element.valOr,
    "val-not":xmlTemplates.element.valNot,
    "val-and":xmlTemplates.element.valAnd,
    "matches":xmlTemplates.element.matches,
    "number-range":xmlTemplates.element.numberRange,
    "number-integer":xmlTemplates.element.numberInteger,
    "number-decimal":xmlTemplates.element.numberDecimal,
    "text-length":xmlTemplates.element.textLength,
    "text-email":xmlTemplates.element.textEmail,
    "text-url":xmlTemplates.element.textUrl,
    "text-letters":xmlTemplates.element.textLetters,
    "text-digits":xmlTemplates.element.textDigits,
    "time-range":xmlTemplates.element.timeRange,
    "credit-card":xmlTemplates.element.creditCard,
    "secas":xmlTemplates.element.secas,
    "seca":xmlTemplates.element.seca,
    //=================Common Elemetns==========//
    "description":xmlTemplates.element.description,
    //================= action elements=================//
    "actions": xmlTemplates.element.actions,
    "message":xmlTemplates.element.message,
    "service-call": xmlTemplates.element.serviceCall,
    "check-errors":xmlTemplates.element.checkErrors,
    "condition": xmlTemplates.element.condition,
    "set": xmlTemplates.element.set,
    "entity-find-one":xmlTemplates.element.entityFindOne,
    "entity-find":xmlTemplates.element.entityFind,
    "if": xmlTemplates.element.if,
    "else": xmlTemplates.element.else,
    "service-group": xmlTemplates.element.serviceGroup,
    "service-invoke": xmlTemplates.element.serviceInvoke,
    "script": xmlTemplates.element.script,
    "order-map-list": xmlTemplates.element.orderMapList,
    "filter-map-list": xmlTemplates.element.filterMapList,
    "date-filter": xmlTemplates.element.dateFilter,
    "entity-data":xmlTemplates.element.entityData,
    "default-parameters":xmlTemplates.element.defaultParameters,
    "search-from-inputs":xmlTemplates.element.searchFromInputs,
    "econditions":xmlTemplates.element.econditions,
    "having-econditions":xmlTemplates.element.havingEconditions,
    "econdition":xmlTemplates.element.econdition,
    "econdition-object":xmlTemplates.element.econditionObject,
    "select-field":xmlTemplates.element.selectField,
    "order-by":xmlTemplates.element.orderBy,
    "limit-range":xmlTemplates.element.limitRange,
    "limit-view":xmlTemplates.element.limitView,
    "use-iterator":xmlTemplates.element.useIterator,
    "field-map":xmlTemplates.element.fieldMap,
    "entity-find-count":xmlTemplates.element.entityFindCount,
    "entity-find-related-one":xmlTemplates.element.entityFindRelatedOne,
    "entity-find-related":xmlTemplates.element.entityFindRelated,
    "entity-make-value":xmlTemplates.element.entityMakeValue,
    "entity-create":xmlTemplates.element.entityCreate,
    "entity-update":xmlTemplates.element.entityUpdate,
    "entity-delete":xmlTemplates.element.entityDelete,
    "entity-delete-related":xmlTemplates.element.entityDeleteRelated,
    "entity-delete-by-condition":xmlTemplates.element.entityDeleteByCondition,
    "entity-set":xmlTemplates.element.entitySet,
    "entity-sequenced-id-primary":xmlTemplates.element.entitySequencedIdPrimary,
    "entity-sequenced-id-secondary":xmlTemplates.element.entitySequencedIdSecondary,
    "break":xmlTemplates.element.break,
    "continue":xmlTemplates.element.continue,
    "iterate":xmlTemplates.element.iterate,
    "return":xmlTemplates.element.return,
    "assert":xmlTemplates.element.assert,
    "while":xmlTemplates.element.while,
    "then":xmlTemplates.element.then,
    "else-if":xmlTemplates.element.elseIf,
    "or":xmlTemplates.element.or,
    "and":xmlTemplates.element.and,
    "not":xmlTemplates.element.not,
    "compare":xmlTemplates.element.compare,
    "expression":xmlTemplates.element.expression,
    "log":xmlTemplates.element.log,

};
var serviceXmlSpec = {
    onchange: function () {
        //console.log("Ah been chaaanged!");
    },
    validate: function (jsElement) {
        if (typeof(jsElement) == "string") jsElement = Xonomy.xml2js(jsElement);
        var valid = true;
        var elementSpec = this.elements[jsElement.name];
        if (elementSpec.validate) {
            elementSpec.validate(jsElement); //validate the element
        }
        for (var iAttribute = 0; iAttribute < jsElement.attributes.length; iAttribute++) {
            var jsAttribute = jsElement.attributes[iAttribute];
            var attributeSpec = elementSpec.attributes[jsAttribute.name];
            if (attributeSpec.validate) {
                if (!attributeSpec.validate(jsAttribute)) valid = false; //validate the attribute
            }
        }
        for (var iChild = 0; iChild < jsElement.children.length; iChild++) {
            if (jsElement.children[iChild].type == "element") {
                var jsChild = jsElement.children[iChild];
                if (!this.validate(jsChild)) valid = false; //recurse to the child element
            }
        }
        return valid;
    },
    elements: serviceElements
};
