(function () {

    var artifactType = "decision";

    var CLASS_NAMES = {
        drd: 'dmn-icon-lasso-tool',
        decisionTable: 'dmn-icon-decision-table',
        literalExpression: 'dmn-icon-literal-expression'
    };
    var $container = $('.editor-container');
    var $tabs = $('.editor-tabs');
    var dmnModeler = new DmnJS({
        container: $container,
        height: 500,
        width: '100%',
        keyboard: {
            bindTo: window
        }
    });
    $tabs.delegate('.tab', 'click', function(e) {
        var viewIdx = parseInt(this.getAttribute('data-id'), 10);
        var view = dmnModeler.getViews()[viewIdx];
        dmnModeler.open(view);
    });
    dmnModeler.on('views.changed', function(event) {
        var { views, activeView } = event;
        // clear tabs
        $tabs.empty();
        views.forEach(function(v, idx) {
            const className = CLASS_NAMES[v.type];
            var tab = $(`
        <div class="tab ${ v === activeView ? 'active' : ''}" data-id="${idx}">
          <span class="${ className }"></span>
          ${v.element.name || v.element.id}
        </div>
      `);
            $tabs.append(tab);
        });
    });

    function setEditor(xml) {
        dmnModeler.importXML(xml, function(err) {
            if (err) {
                return console.error('could not import DMN 1.1 diagram', err);
            }
            var activeEditor = dmnModeler.getActiveViewer();
            // access active editor components
            var canvas = activeEditor.get('canvas');
            // zoom to fit full viewport
            canvas.zoom('fit-viewport');
        });
    }
    function saveFile() {
        if(!urlParams.get("targetHash")){
            alert('ذخیره مستقیم فایل امکان پذیر نیست. شما فایل را از تب فایل ها انتخاب نکرده اید. این فایل دانلود می شود.');
            downloadFile();clearDraft();
            return;
        }
        dmnModeler.saveXML({ format: true }, function(err, xml) {
            if (err) {
                return console.error('could not save DMN 1.1 diagram', err);
            }
            var postData = {};
            postData.content = xml;
            postData.targetHash = urlParams.get("targetHash");
        postData.targetPhash = urlParams.get("targetPhash");
        postData.resourceRoot = urlParams.get("resourceRoot");
            postData.moquiSessionToken = document.getElementById("saveFormSessionToken").value;
            var host = window.location.origin;
            var url = host +"/apps/tds/saveXmlFile";
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200){
                    if(this.responseText=="true"){
                        swal("تراکنش موفق!", "فایل با موفقیت ذخیره شد.", "success",{buttons:{
                                ok: "قبول، ادامه ویرایش",
                                okRedirect: {
                                    text: "قبول، برگشت به فایل ها",
                                    value: "files"
                                }
                            }}).then(function (value) {
                            switch (value) {
                                case "files":
                                    window.location.replace(window.location.origin+"/apps/tds/elFinder?resourceRoot="+urlParams.get("resourceRoot")+"#elf_"+urlParams.get("targetPhash"));
                                    break;
                                default:
                                    break;
                            }
                        });
                    }
                }
            };
            xhttp.open("POST", url, false);
            xhttp.withCredentials = true;
            xhttp.setRequestHeader('Content-Type','application/json');
            xhttp.send(JSON.stringify(postData));
        });
    }
    function downloadFile() {
        dmnModeler.saveXML({ format: true }, function(err, xml) {
            if (err) {
                return console.error('could not save DMN 1.1 diagram', err);
            }
            var element = document.createElement('a');
            element.setAttribute('href', 'data:application/dmn11-xml;charset=UTF-8,' + encodeURIComponent(xml));
            element.setAttribute('download', "export.dmn");
            element.style.display = 'none';
            document.body.appendChild(element);
            element.click();
            document.body.removeChild(element);
        });
    }
    function initiateXml() { xml = xmlCommonFuncs.loadFile();
         
        var initXmlElement= document.getElementById('xmlInit');
        if(xml.length < 5){
            xml = $(initXmlElement).text();
        }
    }
    var xml = "";
    initiateXml();
    setEditor(xml);

    function readFile(file) {
        if (!file) {return;}
        var reader = new FileReader();
        reader.onload = function (e) {
            var contents = e.target.result;
            xml = contents;
            setEditor(xml);
        };
        reader.readAsText(file);
    }
    document.getElementById('import').addEventListener('click', function () {
        document.getElementById('importFile').click();
    });
    document.getElementById('importFile').addEventListener('change', function () {
        readFile(this.files[0]);
    });
    document.getElementById('download').addEventListener('click', function () {
        downloadFile();clearDraft();
    });
    document.getElementById('save').addEventListener('click', function () {
        saveFile();clearDraft();
    });


    var draftStorageKey = "tds_rec_decision";
    function startSavingDraft(){
        var oldObjStr;
        dmnModeler.saveXML({ format: true }, function(err, xml) {oldObjStr=xml;});
        setInterval(function () {
            var newObjStr;
            dmnModeler.saveXML({ format: true }, function(err, xml) {newObjStr=xml;});
            if(oldObjStr.length != newObjStr.length) {
                localStorage.setItem(draftStorageKey,newObjStr);
                console.log("INFO: saved in draft")
            }
        },10000);
    }
    function loadDraft() {
        if(localStorage.hasOwnProperty(draftStorageKey)&&localStorage.getItem(draftStorageKey)!=""){
            swal("بازیابی فایل!", "شما تغییرات ذخیره نشده دارید", "warning",{buttons:{
                    dontRecovery: "عدم بازیابی و ادامه.",
                    recovery : {
                        text: "بازیابی فایل ذخیره نشده.",
                        value: "recovery"
                    }
                }}).then(function (value) {
                switch (value) {
                    case "recovery":
                        setEditor(localStorage.getItem(draftStorageKey));
                        break;
                    default:
                        break;
                }
                clearDraft();
                startSavingDraft();
            });
        }else{
            setTimeout(function () {
                startSavingDraft();
            },5000);
        }
    }
    function clearDraft() {
        localStorage.setItem(draftStorageKey,"");
    }
    loadDraft();
}());
