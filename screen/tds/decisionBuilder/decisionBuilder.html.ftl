<input type="text" hidden value="${ec.web.sessionToken}" id="saveFormSessionToken">
<link rel="stylesheet" href="/trnstatic/3rd/taraan/modeler/dmnModeler/diagram-js.css">
<link rel="stylesheet" href="/trnstatic/3rd/taraan/modeler/dmnModeler/dmn-js-shared.css">
<link rel="stylesheet" href="/trnstatic/3rd/taraan/modeler/dmnModeler/dmn-js-drd.css">
<link rel="stylesheet" href="/trnstatic/3rd/taraan/modeler/dmnModeler/dmn-js-decision-table.css">
<link rel="stylesheet" href="/trnstatic/3rd/taraan/modeler/dmnModeler/dmn-js-decision-table-controls.css">
<link rel="stylesheet" href="/trnstatic/3rd/taraan/modeler/dmnModeler/dmn-js-literal-expression.css">
<link rel="stylesheet" href="/trnstatic/3rd/taraan/modeler/dmnModeler/css/dmn.css">
<script src="/trnstatic/3rd/taraan/modeler/dmnModeler/dmn-modeler.production.min.js"></script>

<style>
    .dmn-js-parent {
        border: solid 1px #ccc;
    }

    .editor-tabs .tab {
        display: block;
        white-space: nowrap;
        background: white;
        padding: 5px;
        margin: -1px 2px 2px 2px;
        border: solid 1px #CCC;
        border-radius: 0 0 2px 2px;
        padding: 8px;
        font-family: 'Arial', sans-serif;
        font-weight: bold;
        cursor: default;
        font-size: 14px;
        color: #444;
        flex: 0 0 1%;
    }

    .editor-tabs {
        display: flex;
        flex-direction: row;
        position: relative;
    }

    .editor-tabs .tab:first-child {
        margin-left: 5px;
    }

    .editor-tabs .tab.active {
        border-top-color: white;
    }

    .editor-tabs .tab.active,
    .editor-tabs .tab:hover {
        border-bottom: solid 3px #52b415;
        margin-bottom: 0;
    }
</style>
<div class="test-container">
    <div class="editor-parent">
        <div class="editor-container"></div>
        <div class="editor-tabs"></div>
    </div>
</div>
<script src="./common/xmlfunctions.js" type="text/javascript"></script>
<script src="./decisionBuilder/main.js" type="text/javascript"></script>