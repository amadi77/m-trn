<style>
    .formbuilder,.formio-dialog  {
        direction: ltr;
        /*float: left;*/
    }
    .formbuilder .col-md-1, .formbuilder .col-md-2, .formbuilder .col-md-3, .formbuilder .col-md-4, .formbuilder .col-md-5, .formbuilder .col-md-6, .formbuilder .col-md-7, .formbuilder .col-md-8, .formbuilder .col-md-9, .formbuilder .col-md-10, .formbuilder .col-md-11, .formbuilder .col-md-12, .formbuilder .col-sm-1, .formbuilder .col-sm-2, .formbuilder .col-sm-3, .formbuilder .col-sm-4, .formbuilder .col-sm-5, .formbuilder .col-sm-6, .formbuilder .col-sm-7, .formbuilder .col-sm-8, .formbuilder .col-sm-9, .formbuilder .col-sm-10, .formbuilder .col-sm-11, .formbuilder .col-sm-12, .formarea.drag-container .col-md-1, .formarea.drag-container .col-md-2, .formarea.drag-container .col-md-3, .formarea.drag-container .col-md-4, .formarea.drag-container .col-md-5, .formarea.drag-container .col-md-6, .formarea.drag-container .col-md-7, .formarea.drag-container .col-md-8, .formarea.drag-container .col-md-9, .formarea.drag-container .col-md-10, .formarea.drag-container .col-md-11, .formarea.drag-container .col-md-12, .formarea.drag-container .col-sm-1, .formarea.drag-container .col-sm-2, .formarea.drag-container .col-sm-3, .formarea.drag-container .col-sm-4, .formarea.drag-container .col-sm-5, .formarea.drag-container .col-sm-6, .formarea.drag-container .col-sm-7, .formarea.drag-container .col-sm-8, .formarea.drag-container .col-sm-9, .formarea.drag-container .col-sm-10, .formarea.drag-container .col-sm-11, .formarea.drag-container .col-sm-12, .formio-dialog .col-md-1, .formio-dialog .col-md-2, .formio-dialog .col-md-3, .formio-dialog .col-md-4, .formio-dialog .col-md-5, .formio-dialog .col-md-6, .formio-dialog .col-md-7, .formio-dialog .col-md-8, .formio-dialog .col-md-9, .formio-dialog .col-md-10, .formio-dialog .col-md-11, .formio-dialog .col-md-12, .formio-dialog .col-sm-1, .formio-dialog .col-sm-2, .formio-dialog .col-sm-3, .formio-dialog .col-sm-4, .formio-dialog .col-sm-5, .formio-dialog .col-sm-6, .formio-dialog .col-sm-7, .formio-dialog .col-sm-8, .formio-dialog .col-sm-9, .formio-dialog .col-sm-10, .formio-dialog .col-sm-11, .formio-dialog .col-sm-12 {
        direction: ltr;
        float: left;
    }
    @media (min-width: 768px) {
        .formbuilder .col-md-1, .formbuilder .col-md-2, .formbuilder .col-md-3, .formbuilder .col-md-4, .formbuilder .col-md-5, .formbuilder .col-md-6, .formbuilder .col-md-7, .formbuilder .col-md-8, .formbuilder .col-md-9, .formbuilder .col-md-10, .formbuilder .col-md-11, .formbuilder .col-md-12, .formbuilder .col-sm-1, .formbuilder .col-sm-2, .formbuilder .col-sm-3, .formbuilder .col-sm-4, .formbuilder .col-sm-5, .formbuilder .col-sm-6, .formbuilder .col-sm-7, .formbuilder .col-sm-8, .formbuilder .col-sm-9, .formbuilder .col-sm-10, .formbuilder .col-sm-11, .formbuilder .col-sm-12, .formarea.drag-container .col-md-1, .formarea.drag-container .col-md-2, .formarea.drag-container .col-md-3, .formarea.drag-container .col-md-4, .formarea.drag-container .col-md-5, .formarea.drag-container .col-md-6, .formarea.drag-container .col-md-7, .formarea.drag-container .col-md-8, .formarea.drag-container .col-md-9, .formarea.drag-container .col-md-10, .formarea.drag-container .col-md-11, .formarea.drag-container .col-md-12, .formarea.drag-container .col-sm-1, .formarea.drag-container .col-sm-2, .formarea.drag-container .col-sm-3, .formarea.drag-container .col-sm-4, .formarea.drag-container .col-sm-5, .formarea.drag-container .col-sm-6, .formarea.drag-container .col-sm-7, .formarea.drag-container .col-sm-8, .formarea.drag-container .col-sm-9, .formarea.drag-container .col-sm-10, .formarea.drag-container .col-sm-11, .formarea.drag-container .col-sm-12, .formio-dialog .col-md-1, .formio-dialog .col-md-2, .formio-dialog .col-md-3, .formio-dialog .col-md-4, .formio-dialog .col-md-5, .formio-dialog .col-md-6, .formio-dialog .col-md-7, .formio-dialog .col-md-8, .formio-dialog .col-md-9, .formio-dialog .col-md-10, .formio-dialog .col-md-11, .formio-dialog .col-md-12, .formio-dialog .col-sm-1, .formio-dialog .col-sm-2, .formio-dialog .col-sm-3, .formio-dialog .col-sm-4, .formio-dialog .col-sm-5, .formio-dialog .col-sm-6, .formio-dialog .col-sm-7, .formio-dialog .col-sm-8, .formio-dialog .col-sm-9, .formio-dialog .col-sm-10, .formio-dialog .col-sm-11, .formio-dialog .col-sm-12 {
            direction: ltr;
            float: left;
        }
    }
    .formarea.drag-container{
        direction: rtl;
    }
    .formcomponents{
        float: right!important;
    }
    .formio-dialog .radio label,.formio-dialog .checkbox label {
        padding-left: initial;
        padding-left: 20px;
    }
    .formio-dialog .radio input[type="radio"],.formio-dialog .radio-inline input[type="radio"],.formio-dialog .checkbox input[type="checkbox"],.formio-dialog .checkbox-inline input[type="checkbox"] {
        margin-right: initial;
        margin-left: -20px;
    }
    pre{
        white-space: pre;
    }
</style>
<div class="container-fluid" style="margin-top: 10px;">
    <div class="row">
        <div class="col-sm-12 ">
            <div id="builder" class="formio-form row formbuilder"></div>
        </div>
        <div class="col-sm-12 well" >
            <div class="export-json col-sm-3 borderl">
                <h4>ذخیره:</h4>
                <div class="btn-group" style="direction:ltr; ">
                    <div class="btn btn-sm btn-primary" id="dlEntity">موجودیت</div>
                </div>
            </div>
            <div class="col-sm-3 borderl">
                <h4>عملیات:</h4>
                <div class="btn-group" style="direction:ltr; ">
                    <div class="btn btn-sm btn-primary formBuilderHelperBtn" id="requireForm" style="float: left;">require</div>
                    <div class="btn btn-sm btn-primary formBuilderHelperBtn" id="disableForm">disable</div>
                </div>
                <div class="btn-group" style="direction:ltr; ">
                    <div class="btn btn-sm btn-primary formBuilderHelperBtn" id="toggleEmbedForm">toggleEmbedForm</div>
                    <div class="btn btn-sm btn-primary formBuilderHelperBtn" id="ToggleClearHidden">ToggleClearHidden</div>
                </div>
            </div>
            <div class="col-sm-3 borderl">
                <h4>نوع:</h4>

                <h6 class="text-center text-muted"><select class="form-control" id="form-select"  style="display: inline-block; width: 150px;"><option value="form">Form</option><option value="wizard">Wizard</option><option value="pdf">PDF</option></select></h6>
            </div>
            <div class="col-sm-3 borderl">
                <h4>نگاشت موجودیت:</h4>
                <h6 class="text-center text-muted">
                    <div class="form-group" style="">
                        <label class="control-label" style="" for="formMainEntity">موجودیت اصلی</label>
                        <input type="text" id="formMainEntity"  class="form-control" style="direction: ltr;text-align: left;" placeholder="com.trn.sampleEntity">
                        <label class="control-label" style="" for="formMapperEnabled">فعالسازی نگاشت موجودیت</label>
                        <input type="checkbox" id="formMapperEnabled" class="form-control" >
                    </div>
                </h6>
            </div>
        </div>
    </div>
    <h3 class="text-center text-muted">پیش نمایش</h3>
    <div class="row ">
        <div class="col-sm-10 col-sm-offset-1">
            <div id="formio" class="formio-form formio-trn-rtl"></div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <h3 class="text-center text-muted">داده</h3>
            <div class="well jsonviewer">
                <button class="btn" onclick="document.getElementById('subjsonINPUT').select();  document.execCommand('copy');" style="position: absolute;">copy</button>
                <pre id="subjson" style="min-height: 150px; max-height: 600px;overflow-x: scroll;"></pre>
                <input type="text" id="subjsonINPUT" style="width: 10px;">
            </div>
        </div>
        <div class="col-sm-6">
            <h3 class="text-center text-muted">ساختار</h3>
            <div class="well jsonviewer">
                <button class="btn" onclick="document.getElementById('jsonINPUT').select();  document.execCommand('copy');" style="position: absolute;">copy</button>
                <pre id="json" style="min-height: 150px; max-height: 600px;overflow-x: scroll;" ></pre>
                <input type="text" id="jsonINPUT" style="width: 10px">
            </div>
        </div>

        <div class="clearfix"></div>
    </div>
    <script>
        <#include "component://trn/template/screen-macro/formRendererContrib.js" parse=false encoding="UTF-8">
    </script>
    <script>
        <#include "component://trn/template/screen-macro/iterateEmbedForm.js" parse=false encoding="UTF-8">
    </script>
    <script src="./common/xmlfunctions.js" type="text/javascript"></script>
    <script src="./formBuilder/formToEntity.js" type="text/javascript"></script>
    <script src="./formBuilder/main.js" type="text/javascript"></script>

</div>
