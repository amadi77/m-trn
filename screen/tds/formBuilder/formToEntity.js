(function () {


    /**
     * @constructor
     */
    this.FormToEntity = function () {
        this._scope = {};
    };

    /**
     * PUBLIC
     * set the form components into object
     * @param formComponents
     */
    FormToEntity.prototype.setFormComponents = function (formComponents) {
        this._scope.formComponents = formComponents;
    };

    /**
     * PUBLIC
     * download the exported entity
     */
    FormToEntity.prototype.download = function () {
        downloadEntity.call(this);
    };

    /**
     * PRIVATE
     * @param lastEntityName
     * @param thisKey
     * @returns {boolean}
     */
    function isInThisEntity(lastEntityName, thisKey) {
        var res = false;
        for (var i = 0; i < Object.keys(this._scope.flattenComps).length; i++) {
            var thisComp = Object.keys(this._scope.flattenComps)[i];
            thisComp.split(".").length;
            if (thisComp.split(".")[thisComp.split(".").length - 1] === thisKey) {
                if (thisComp.split(".")[thisComp.split(".").length - 2] === lastEntityName) {
                    res = true;
                    break;
                }
            }
        }
        return res;
    }

    /**
     * PRIVATE
     * @param formType
     * @returns {string}
     */
    function getType(formType) {
        // not implemented but in entity
        // binary-very-long
        // text-very-long
        // text-indicator
        // currency-precise
        // number-float
        // number-decimal
        // date-time
        // time
        // date
        // id-long
        // id

        // not implement but in form
        // file
        // location
        // signiture
        // jalaali
        // province
        // city
        // hidden
        // datamap
        // day
        // date-time

        var map = {
            number: "number-integer",
            currency: "currency-amount",
            time: "text-short",
            select: "text-short", // if be multiple, if has relation one/many/enum/status
            phoneNumber: "text-short",
            address: "text-short",
            tags: "text-short",//if array, if has relation
            checkbox: "text-short",
            selectboxes: "text-short", // ?
            radio: "text-short", // maybe should be enum/status
            password: "text-short",
            textfield: "text-medium",
            textarea: "text-long",
            container: "id"
        };
        return (map[formType] ? map[formType] : "?");
    }

    /**
     * PRIVATE
     * @param comps
     * @param res
     * @param lastEntityName
     * @param justLookForFields
     * @param justLookForRelations
     */
    function exportEntity(comps, res, lastEntityName, justLookForFields, justLookForRelations) {
        FormioUtils.eachComponent(comps, function (c) {
            if (c.input) {
                if (c.type === "container" && !justLookForFields && !justLookForRelations) {
                    var newEntity = {};
                    lastEntityName = c.key;
                    newEntity._attributes = {"entity-name": c.key, "package": "?"};
                    newEntity.field = [];
                    newEntity.field.push({_attributes: {name: c.key + "Id", type: "id", "is-pk": "true"}});
                    exportEntity(c.components, newEntity.field, lastEntityName, true, false);
                    newEntity.relationship = [];
                    exportEntity(c.components, newEntity.relationship, lastEntityName, true, true);
                    res.push(newEntity);
                } else if (c.type === "container" && justLookForFields && justLookForRelations && isInThisEntity(lastEntityName, c.key)) {
                    var newRelation = {};
                    newRelation._attributes = {title: c.key, related: "?." + c.key, type: "one"};
                    newRelation["key-map"] = {_attributes: {"field-name": c.key + "Id"}};
                    res.push(newRelation);
                } else if (c.type === "container" && justLookForFields && !justLookForRelations && isInThisEntity(lastEntityName, c.key)) {
                    var newField = {};
                    newField._attributes = {name: c.key + "Id", type: getType(c.type)};
                    res.push(newField);

                } else if (c.type !== "container" && justLookForFields && !justLookForRelations && isInThisEntity(lastEntityName, c.key)) {
                    var newFieldRel = {};
                    newFieldRel._attributes = {name: c.key, type: getType(c.type)};
                    res.push(newFieldRel);
                }
            }
        }, true);
    }

    /**
     * PRIVATE
     */
    function downloadEntity() {
        var res = {
            "_declaration":
                {
                    "_attributes": {
                        "version": "1.0",
                        "encoding": "utf-8"
                    }
                },
            "entities": {
                "_attributes": {
                    "xsi:noNamespaceSchemaLocation": "http://moqui.org/xsd/entity-definition-2.1.xsd",
                    "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance"
                },
                "entity": []
            }
        };
        this._scope.flattenComps = FormioUtils.flattenComponents(this._scope.formComponents, true);

        exportEntity(this._scope.formComponents, res.entities.entity, false, false, false);
        this._scope.entityRes = js2xml(res, {compact: true, ignoreComment: true, spaces: 4});

        var element = document.createElement('a');
        element.setAttribute('href', 'data:application/xml;charset=utf-8,' + encodeURIComponent(this._scope.entityRes));
        element.setAttribute('download', "entities.xml");
        element.style.display = 'none';
        document.body.appendChild(element);
        element.click();
        document.body.removeChild(element);
    }
}());
