var wsHolder = {};

(function () {
    var urlParams = new URL(window.location.href).searchParams;

    var TaraanScope = TaraanScope || {};
    TaraanScope.DEFAULT_FILE_NAME = TaraanScope.fileName = "form.json";
    TaraanScope.formioJson = {
        display: 'form',
        components: []
    };
    var jsonElement = document.getElementById('json');
    var formElement = document.getElementById('formio');

    var subJSON = document.getElementById('subjson');
    var subjsonINPUT = document.getElementById('subjsonINPUT');
    var builder = null;
    var loadTrnPopertiesFormioJson = function () {
      if(!TaraanScope.formioJson.trnProperties || TaraanScope.formioJson.trnProperties==undefined){
          TaraanScope.formioJson.trnProperties = {};
          if(!TaraanScope.formioJson.trnProperties.rootMapper || TaraanScope.formioJson.trnProperties.rootMapper==undefined){
              TaraanScope.formioJson.trnProperties.rootMapper={};
          }
      }
        if(TaraanScope.formioJson.trnProperties.rootMapper.entity && TaraanScope.formioJson.trnProperties.rootMapper.entity!=undefined) {
            $("#formMainEntity").val(TaraanScope.formioJson.trnProperties.rootMapper.entity);
        }
        if(TaraanScope.formioJson.trnProperties.rootMapper.mapperEnabled==true) {
            $("#formMapperEnabled").prop('checked', true)
        }
    };
    var setTrnPopertiesFormioJson = function () {
      if(!TaraanScope.formioJson.trnProperties || TaraanScope.formioJson.trnProperties==undefined){
          TaraanScope.formioJson.trnProperties = {};
          if(!TaraanScope.formioJson.trnProperties.rootMapper || TaraanScope.formioJson.trnProperties.rootMapper==undefined){
              TaraanScope.formioJson.trnProperties.rootMapper={};
          }
      }
        TaraanScope.formioJson.trnProperties.rootMapper.entity = $("#formMainEntity").val();
        TaraanScope.formioJson.trnProperties.rootMapper.mapperEnabled=true;
    };
    var setBuilder = function () {
        var erpComponents= {
            title: 'ERP',
            weight: 4,
            components: {
                enums: {
                    title: 'اطلاعات پایه-enums',
                    key: 'enums',
                    icon: 'fa fa-terminal',
                    schema: {
                        "input": true,
                        "tableView": true,
                        "label": "enumId",
                        "key": "enumId",
                        "placeholder": "",
                        "data": {
                            "values": [
                                {
                                    "value": "",
                                    "label": ""
                                }
                            ],
                            "json": "",
                            "url": "",
                            "resource": "",
                            "custom": "var token = null;\n" +
                            "var orderByField = null;\n" +
                            "var enumTypeId = null;\n" +
                            "var pageSize = null;\n" +
                            "var host = window.location.origin+\"/rest/s1/\";\n" +
                            "var url = host + \"moqui/basic/enums?pageNoLimit=true\";\n" +
                            "if(pageSize)url +=\"&pageSize=\"+pageSize;\n" +
                            "if (enumTypeId) url += \"&enumTypeId=\" + enumTypeId;\n" +
                            "if (orderByField) url += \"&orderByField=\" + orderByField;\n" +
                            "var xhttp = new XMLHttpRequest();\n" +
                            "xhttp.onreadystatechange = function () { if (this.readyState == 4 && this.status == 200) values = JSON.parse(xhttp.responseText);};\n" +
                            "xhttp.open(\"GET\", url, false);\n" +
                            "if (token) {xhttp.setRequestHeader('Authorization', token);} else {xhttp.withCredentials = true;}\n" +
                            "xhttp.send();"
                        },
                        "dataSrc": "custom",
                        "valueProperty": "enumId",
                        "refreshOn": "",
                        "template": "<span>{{ item.description }}</span>",
                        "persistent": true,
                        "clearOnHide": false,
                        "validate": {
                            "required": true
                        },
                        "type": "select"
                    }
                },
                worksEfforts: {
                    title: 'workEfforts',
                    key: 'workEfforts',
                    icon: 'fa fa-terminal',
                    schema: {
                        "input": true,
                        "tableView": true,
                        "label": "workEffortId",
                        "key": "workEffortId",
                        "placeholder": "",
                        "data": {
                            "values": [
                                {
                                    "value": "",
                                    "label": "",
                                }
                            ],
                            "json": "",
                            "url": "",
                            "resource": "",
                            "custom": "var workEffortTypeEnumId = data.workEffortTypeEnumId;\n" +
                            "var rootWorkEffortId = data.rootWorkEffortId;\n" +
                            "var parentWorkEffortId = data.parentWorkEffortId;\n" +
                            "var token = null;var host = window.location.origin+\"/rest/s1/\";\n" +
                            "var url = host + \"mantle/workEfforts?pageSize=10000\";\n" +
                            "if(workEffortTypeEnumId) url+='&workEffortTypeEnumId='+workEffortTypeEnumId;\n" +
                            "if(rootWorkEffortId) url+='&rootWorkEffortId='+rootWorkEffortId;\n" +
                            "if(parentWorkEffortId) url+='&parentWorkEffortId='+parentWorkEffortId;\n" +
                            "var xhttp = new XMLHttpRequest();\n" +
                            "xhttp.onreadystatechange = function () { if (this.readyState == 4 && this.status == 200) values = JSON.parse(xhttp.responseText)};\n" +
                            "xhttp.open(\"GET\", url, false);\n" +
                            "if (token) {xhttp.setRequestHeader('Authorization', token);} else {xhttp.withCredentials = true;}\n" +
                            "xhttp.send();"
                        },
                        "dataSrc": "custom",
                        "valueProperty": "workEffortId",
                        "refreshOn": "",
                        "template": "<span>{{ item.workEffortName }}</span>",
                        "persistent": true,
                        "clearOnHide": false,
                        "validate": {
                            "required": true
                        },
                        "type": "select"
                    }
                },
                statuses: {
                    title: 'وضعیت ها-statuses',
                    key: 'statuses',
                    icon: 'fa fa-terminal',
                    schema: {
                        "input": true,
                        "tableView": true,
                        "label": "statusId",
                        "key": "statusId",
                        "placeholder": "",
                        "data": {
                            "values": [
                                {
                                    "value": "",
                                    "label": "",
                                }
                            ],
                            "json": "",
                            "url": "",
                            "resource": "",
                            "custom": "var token = null;\n" +
                            "var orderByField = null;\n" +
                            "var statusTypeId = null;\n" +
                            "var statusId = null;\n" +
                            "var pageSize = null;\n" +
                            "var host = window.location.origin+\"/rest/s1/\";\n" +
                            "var url = host + ( (!statusId) ?\"moqui/basic/statuses?pageNoLimit=true\":\"moqui/basic/\"+statusId+\"?pageNoLimit=true\");\n" +
                            "if(pageSize)url +=\"&pageSize=\"+pageSize;\n" +
                            "if (statusTypeId) url += \"&statusTypeId=\" + statusTypeId;\n" +
                            "if (orderByField) url += \"&orderByField=\" + orderByField;\n" +
                            "var xhttp = new XMLHttpRequest();\n" +
                            "xhttp.onreadystatechange = function () { if (this.readyState == 4 && this.status == 200) values = JSON.parse(xhttp.responseText);};\n" +
                            "xhttp.open(\"GET\", url, false);\n" +
                            "if (token) {xhttp.setRequestHeader('Authorization', token);} else {xhttp.withCredentials = true;}\n" +
                            "xhttp.send();"
                        },
                        "dataSrc": "custom",
                        "valueProperty": "statusId",
                        "refreshOn": "",
                        "template": "<span>{{ item.description }}</span>",
                        "persistent": true,
                        "clearOnHide": false,
                        "validate": {
                            "required": true
                        },
                        "type": "select"
                    }
                },
                roleType: {
                    title: 'نقش -roleType',
                    key: 'statuses',
                    icon: 'fa fa-terminal',
                    schema: {
                        "input": true,
                        "tableView": true,
                        "label": "roleTypeId",
                        "key": "roleTypeId",
                        "placeholder": "",
                        "data": {
                            "values": [
                                {
                                    "value": "",
                                    "label": "",
                                }
                            ],
                            "json": "",
                            "url": "",
                            "resource": "",
                            "custom": "var parentTypeId = data.parentTypeId;\n" +
                            "var roleGroupEnumId = data.roleGroupEnumId;\n" +
                            "var token = null;var host = window.location.origin+\"/rest/s1/\";\n" +
                            "var url = host + \"mantle/parties/roleTypes?pageSize=10000\";\n" +
                            "if(parentTypeId) url+='&parentTypeId='+parentTypeId;\n" +
                            "if(roleGroupEnumId) url+='&roleGroupEnumId='+roleGroupEnumId;\n" +
                            "var xhttp = new XMLHttpRequest();\n" +
                            "xhttp.onreadystatechange = function () { if (this.readyState == 4 && this.status == 200) values = JSON.parse(xhttp.responseText)};\n" +
                            "xhttp.open(\"GET\", url, false);\n" +
                            "if (token) {xhttp.setRequestHeader('Authorization', token);} else {xhttp.withCredentials = true;}\n" +
                            "xhttp.send();"
                        },
                        "dataSrc": "custom",
                        "valueProperty": "roleTypeId",
                        "refreshOn": "",
                        "template": "<span>{{ item.description }}</span>",
                        "persistent": true,
                        "clearOnHide": false,
                        "validate": {
                            "required": true
                        },
                        "type": "select"
                    }
                },
                positionClasses: {
                    title: 'positionClass',
                    key: 'positionClasses',
                    icon: 'fa fa-terminal',
                    schema: {
                        "input": true,
                        "tableView": true,
                        "label": "emplPositionClassId",
                        "key": "emplPositionClassId",
                        "placeholder": "",
                        "data": {
                            "values": [
                                {
                                    "value": "",
                                    "label": "",
                                }
                            ],
                            "json": "",
                            "url": "",
                            "resource": "",
                            "custom": "// var workEffortTypeEnumId = data.workEffortTypeEnumId;\n" +
                            "var token = null;var host = window.location.origin+\"/rest/s1/\";\n" +
                            "var url = host + \"mantle/hr/positionClass?pageSize=10000\";\n" +
                            "// if(workEffortTypeEnumId) url+='&workEffortTypeEnumId='+workEffortTypeEnumId;\n" +
                            "var xhttp = new XMLHttpRequest();\n" +
                            "xhttp.onreadystatechange = function () { if (this.readyState == 4 && this.status == 200) values = JSON.parse(xhttp.responseText)};\n" +
                            "xhttp.open(\"GET\", url, false);\n" +
                            "if (token) {xhttp.setRequestHeader('Authorization', token);} else {xhttp.withCredentials = true;}\n" +
                            "xhttp.send();"
                        },
                        "dataSrc": "custom",
                        "valueProperty": "emplPositionClassId",
                        "refreshOn": "",
                        "template": "<span>{{ item.title }}</span>",
                        "persistent": true,
                        "clearOnHide": false,
                        "validate": {
                            "required": true
                        },
                        "type": "select"
                    }
                },
                partyClassification: {
                    title: 'classification',
                    key: 'classification',
                    icon: 'fa fa-terminal',
                    schema: {
                        "input": true,
                        "tableView": true,
                        "label": "partyClassificationId",
                        "key": "partyClassificationId",
                        "placeholder": "",
                        "data": {
                            "values": [
                                {
                                    "value": "",
                                    "label": "",
                                }
                            ],
                            "json": "",
                            "url": "",
                            "resource": "",
                            "custom": "var token = null;\n" +
                            "var parentClassificationId = data.parentClassificationId;\n" +
                            "var classificationTypeEnumId = data.classificationTypeEnumId;\n" +
                            "var orderByField = null;\n" +
                            "var pageSize = 200;\n" +
                            "var host = window.location.origin;\n" +
                            "var url = host+ \"/rest/s1/mantle/parties/classification\";\n" +
                            "if (pageSize) url += \"?pageSize=\" + pageSize;\n" +
                            "if (classificationTypeEnumId) url += \"&classificationTypeEnumId=\" + classificationTypeEnumId;\n" +
                            "if (parentClassificationId) url += \"&parentClassificationId=\" + parentClassificationId;\n" +
                            "if (orderByField) url += \"&orderByField=\" + orderByField;\n" +
                            "var xhttp = new XMLHttpRequest();\n" +
                            "xhttp.onreadystatechange = function () { if (this.readyState == 4 && this.status == 200) values = JSON.parse(xhttp.responseText);};\n" +
                            "xhttp.open(\"GET\", url, false);\n" +
                            "if (token) {xhttp.setRequestHeader('Authorization', token);} else {xhttp.withCredentials = true;}\n" +
                            "xhttp.send();"
                        },
                        "dataSrc": "custom",
                        "valueProperty": "partyClassificationId",
                        "refreshOn": "",
                        "template": "<span>{{ item.subject }}</span>",
                        "persistent": true,
                        "clearOnHide": false,
                        "validate": {
                            "required": true
                        },
                        "type": "select"
                    }
                },
                party: {
                    title: 'party',
                    key: 'party',
                    icon: 'fa fa-terminal',
                    schema: {
                        "input": true,
                        "tableView": true,
                        "label": "partyId",
                        "key": "partyId",
                        "placeholder": "",
                        "data": {
                            "values": [
                                {
                                    "value": "",
                                    "label": "",
                                }
                            ],
                            "json": "",
                            "url": "",
                            "resource": "",
                            "custom": "var token = null;\n" +
                            "var partyTypeEnumId = data.partyTypeEnumId;\n" +
                            "var roleTypeId = data.roleTypeId;\n" +
                            "var username = data.username;\n" +
                            "var combinedName = data.combinedName;\n" +
                            "var organizationName = data.organizationName;\n" +
                            "var firstName = data.firstName;\n" +
                            "var lastName = data.lastName;\n" +
                            "var emailAddress = data.emailAddress;\n" +
                            "\n" +
                            "var host = window.location.origin+\"/rest/s1/\";\n" +
                            "var url = host + \"mantle/parties?pageSize=10000\";var xhttp = new XMLHttpRequest();\n" +
                            "if(partyTypeEnumId) url+='&partyTypeEnumId='+partyTypeEnumId;\n" +
                            "if(roleTypeId) url+='&roleTypeId='+roleTypeId;\n" +
                            "if(username) url+='&username='+username;\n" +
                            "if(combinedName) url+='&combinedName='+combinedName;\n" +
                            "if(organizationName) url+='&organizationName='+organizationName;\n" +
                            "if(firstName) url+='&firstName='+firstName;\n" +
                            "if(lastName) url+='&lastName='+lastName;\n" +
                            "if(emailAddress) url+='&emailAddress='+emailAddress;\n" +
                            "xhttp.onreadystatechange = function () { if (this.readyState == 4 && this.status == 200) values = JSON.parse(xhttp.responseText).partyIdNameList};\n" +
                            "xhttp.open(\"GET\", url, false);\n" +
                            "if (token) {xhttp.setRequestHeader('Authorization', token);} else {xhttp.withCredentials = true;}\n" +
                            "xhttp.send();"
                        },
                        "dataSrc": "custom",
                        "valueProperty": "partyId",
                        "refreshOn": "",
                        "template": "<span>{{ item.combinedName }}</span>",
                        "persistent": true,
                        "clearOnHide": false,
                        "validate": {
                            "required": true
                        },
                        "type": "select"
                    }
                },
                facility: {
                    title: 'facility',
                    key: 'facility',
                    icon: 'fa fa-terminal',
                    schema: {
                        "input": true,
                        "tableView": true,
                        "label": "facilityId",
                        "key": "facilityId",
                        "placeholder": "",
                        "data": {
                            "values": [
                                {
                                    "value": "",
                                    "label": "",
                                }
                            ],
                            "json": "",
                            "url": "",
                            "resource": "",
                            "custom": "var token = null;\n" +
                            "var orderByField = \"facilityName\";\n" +
                            "var facilityTypeEnumId = data.facilityTypeEnumId;\n" +
                            "var pageSize = 10000;\n" +
                            "var host = window.location.origin+\"/rest/s1/\";\n" +
                            "var url = host +  \"mantle/facilities?pageNoLimit=true\";\n" +
                            "if(pageSize)url +=\"&pageSize=\"+pageSize;\n" +
                            "if (facilityTypeEnumId) url += \"&facilityTypeEnumId=\" + facilityTypeEnumId;\n" +
                            "if (orderByField) url += \"&orderByField=\" + orderByField;\n" +
                            "var xhttp = new XMLHttpRequest();\n" +
                            "xhttp.onreadystatechange = function () { if (this.readyState == 4 && this.status == 200) values = JSON.parse(xhttp.responseText);};\n" +
                            "xhttp.open(\"GET\", url, false);\n" +
                            "if (token) {xhttp.setRequestHeader('Authorization', token);} else {xhttp.withCredentials = true;}\n" +
                            "xhttp.send();"
                        },
                        "dataSrc": "custom",
                        "valueProperty": "facilityId",
                        "refreshOn": "",
                        "template": "<span>{{ item.facilityName }}</span>",
                        "persistent": true,
                        "clearOnHide": false,
                        "validate": {
                            "required": true
                        },
                        "type": "select"
                    }
                },
                contactMechPurpose: {
                    title: 'contactMechPurpose',
                    key: 'contactMechPurpose',
                    icon: 'fa fa-terminal',
                    schema: {
                        "input": true,
                        "tableView": true,
                        "label": "contactMechPurposeId",
                        "key": "contactMechPurposeId",
                        "placeholder": "",
                        "data": {
                            "values": [
                                {
                                    "value": "",
                                    "label": "",
                                }
                            ],
                            "json": "",
                            "url": "",
                            "resource": "",
                            "custom": "var token = null;\n" +
                            "var orderByField = \"contactMechName\";\n" +
                            "var contactMechTypeEnumId = \"CmtPostalAddress\";\n" +
                            "// var host = data._coreHostRemote + \"/rest/s1/\";\n" +
                            "var host = window.location.origin + \"/rest/s1/\";\n" +
                            "var url = host + \"mantle/parties/contactMechs/purposes?pageNoLimit=true\";\n" +
                            "if (contactMechTypeEnumId) url += \"&contactMechTypeEnumId=\" + contactMechTypeEnumId;\n" +
                            "if (orderByField) url += \"&orderByField=\" + orderByField;\n" +
                            "var xhttp = new XMLHttpRequest();\n" +
                            "xhttp.onreadystatechange = function () { if (this.readyState == 4 && this.status == 200) values = JSON.parse(xhttp.responseText);};\n" +
                            "xhttp.open(\"GET\", url, false);\n" +
                            "if (token) {xhttp.setRequestHeader('Authorization', token);} else {xhttp.withCredentials = true;}\n" +
                            "xhttp.send();\n"
                        },
                        "dataSrc": "custom",
                        "valueProperty": "contactMechPurposeId",
                        "refreshOn": "",
                        "template": "<span>{{ item.description }}</span>",
                        "persistent": true,
                        "clearOnHide": false,
                        "validate": {
                            "required": true
                        },
                        "type": "select"
                    }
                },
                asset: {
                    title: 'asset',
                    key: 'asset',
                    icon: 'fa fa-terminal',
                    schema: {
                        "input": true,
                        "tableView": true,
                        "label": "contactMechPurposeId",
                        "key": "contactMechPurposeId",
                        "placeholder": "",
                        "data": {
                            "values": [
                                {
                                    "value": "",
                                    "label": "",
                                }
                            ],
                            "json": "",
                            "url": "",
                            "resource": "",
                            "custom": "var token = null;\n" +
                            "var orderByField = \"statusId\";\n" +
                            "\n" +
                            "var assetTypeEnumId= data.assetTypeEnumId;\n" +
                            "var facilityId= data.facilityId;\n" +
                            "var pageSize = 10000;\n" +
                            "var host = window.location.origin+\"/rest/s1/\";\n" +
                            "var url = host +  \"mantle/assets?pageNoLimit=true\";\n" +
                            "if(pageSize)url +=\"&pageSize=\"+pageSize;\n" +
                            "if (assetTypeEnumId) url += \"&assetTypeEnumId=\" + assetTypeEnumId;\n" +
                            "if (facilityId) url += \"&facilityId=\" + facilityId;\n" +
                            "if (orderByField) url += \"&orderByField=\" + orderByField;\n" +
                            "var xhttp = new XMLHttpRequest();\n" +
                            "xhttp.onreadystatechange = function () { if (this.readyState == 4 && this.status == 200) values = JSON.parse(xhttp.responseText);};\n" +
                            "xhttp.open(\"GET\", url, false);\n" +
                            "if (token) {xhttp.setRequestHeader('Authorization', token);} else {xhttp.withCredentials = true;}\n" +
                            "xhttp.send();"
                        },
                        "dataSrc": "custom",
                        "valueProperty": "assetId",
                        "refreshOn": "",
                        "template": "<span>{{ item.classEnumId }}</span>",
                        "persistent": true,
                        "clearOnHide": false,
                        "validate": {
                            "required": true
                        },
                        "type": "select"
                    }
                },
            }
        };
        var irComponents = {
            title: 'ملی',
            weight: 3,
            components: {
                jalaali: true,
                t_jalaali: {
                    title: 'ترجمه جلالی',
                    key: 't_jalaali',
                    icon: 'fa fa-terminal',
                    schema: {
                        "input": true,
                        "tableView": true,
                        "key": "t_jalaali",
                        "label": "t_jalaali",
                        "protected": false,
                        "unique": false,
                        "persistent": true,
                        "type": "hidden",
                        "hideLabel": false,
                        "tags": [],
                        "conditional": {
                            "show": "",
                            "when": null,
                            "eq": ""
                        },
                        "properties": {
                            "": ""
                        },
                        "calculateValue": "value = new persianDate(new Date(row.eventDate)).format(\"YYYY/MM/DD HH:mm\");"
                    }
                },
                nationalCode: {
                    title: 'کد ملی',
                    key: 'nationalCode',
                    icon: 'fa fa-terminal',
                    schema: {
                        "input": true,
                        "tableView": true,
                        "inputType": "text",
                        "inputMask": "9999999999",
                        "label": "کد ملی",
                        "key": "nationalCode",
                        "placeholder": "",
                        "prefix": "",
                        "suffix": "",
                        "multiple": false,
                        "defaultValue": "",
                        "protected": false,
                        "unique": false,
                        "persistent": true,
                        "hidden": false,
                        "clearOnHide": true,
                        "validate": {
                            "required": false,
                            "minLength": "",
                            "maxLength": "",
                            "pattern": "",
                            "custom": "var allDigitEqual = [\"0000000000\",\"1111111111\",\"2222222222\",\"3333333333\",\"4444444444\",\"5555555555\",\"6666666666\",\"7777777777\",\"8888888888\",\"9999999999\"];\nif (allDigitEqual.indexOf(input)) {\n    valid = 'کد ملی اشتباه است';\n}\nif (input.length != 10) {\n    valid = 'کد ملی اشتباه است';\n}\nif(input.length == 10 && !allDigitEqual.indexOf(input)) {\n    var chArray = input.split('');\n    var num0 = parseInt(chArray[0].toString())*10;\n    var num2 = parseInt(chArray[1].toString())*9;\n    var num3 = parseInt(chArray[2].toString())*8;\n    var num4 = parseInt(chArray[3].toString())*7;\n    var num5 = parseInt(chArray[4].toString())*6;\n    var num6 = parseInt(chArray[5].toString())*5;\n    var num7 = parseInt(chArray[6].toString())*4;\n    var num8 = parseInt(chArray[7].toString())*3;\n    var num9 = parseInt(chArray[8].toString())*2;\n    var a = parseInt(chArray[9].toString());\n    var b = (((((((num0 + num2) + num3) + num4) + num5) + num6) + num7) + num8) + num9;\n    var c = b%11;\n    valid = (((c < 2) && (a === c)) || ((c >= 2) && ((11 - c) === a))) ? true:'کد ملی اشتباه است';\n}",
                            "customPrivate": false
                        },
                        "conditional": {
                            "show": "",
                            "when": null,
                            "eq": ""
                        },
                        "type": "textfield",
                        "$$hashKey": "object:274",
                        "hideLabel": false,
                        "labelPosition": "top",
                        "tags": [],
                        "properties": {
                            "": ""
                        },
                        "customError": "Cannot read property 'length' of undefined"
                    }
                },
                nationalId: {
                    title: 'شناسه ملی',
                    key: 'nationalId',
                    icon: 'fa fa-terminal',
                    schema: {
                        "input": true,
                        "tableView": true,
                        "inputType": "text",
                        "inputMask": "99999999999",
                        "label": "شناسه ملی",
                        "key": "nationalId",
                        "placeholder": "",
                        "prefix": "",
                        "suffix": "",
                        "multiple": false,
                        "defaultValue": "",
                        "protected": false,
                        "unique": false,
                        "persistent": true,
                        "hidden": false,
                        "clearOnHide": true,
                        "validate": {
                            "required": false,
                            "minLength": "",
                            "maxLength": "",
                            "pattern": "",
                            "custom": "",
                            "customPrivate": false
                        },
                        "conditional": {
                            "show": "",
                            "when": null,
                            "eq": ""
                        },
                        "type": "textfield",
                        "$$hashKey": "object:274",
                        "hideLabel": false,
                        "labelPosition": "top",
                        "tags": [],
                        "properties": {
                            "": ""
                        },
                        "customError": "Cannot read property 'length' of undefined"
                    }
                },
                postalCode: {
                    title: 'کد پستی',
                    key: 'postalCode',
                    icon: 'fa fa-terminal',
                    schema: {
                        "input": true,
                        "tableView": true,
                        "inputType": "text",
                        "inputMask": "99999999999",
                        "label": "کد پستی",
                        "key": "postalCode",
                        "placeholder": "",
                        "prefix": "",
                        "suffix": "",
                        "multiple": false,
                        "defaultValue": "",
                        "protected": false,
                        "unique": false,
                        "persistent": true,
                        "hidden": false,
                        "clearOnHide": true,
                        "validate": {
                            "required": false,
                            "minLength": "",
                            "maxLength": "",
                            "pattern": "",
                            "custom": "",
                            "customPrivate": false
                        },
                        "conditional": {
                            "show": "",
                            "when": null,
                            "eq": ""
                        },
                        "type": "textfield",
                        "$$hashKey": "object:274",
                        "hideLabel": false,
                        "labelPosition": "top",
                        "tags": [],
                        "properties": {
                            "": ""
                        },
                        "customError": "Cannot read property 'length' of undefined"
                    }
                },
                phone: {
                    title: 'تلفن ثابت',
                    key: 'phone',
                    icon: 'fa fa-terminal',
                    schema: {
                        "input": true,
                        "tableView": true,
                        "inputType": "text",
                        "inputMask": "09999999999",
                        "label": "تلفن ثابت",
                        "key": "phone",
                        "placeholder": "",
                        "prefix": "",
                        "suffix": "",
                        "multiple": false,
                        "defaultValue": "",
                        "protected": false,
                        "unique": false,
                        "persistent": true,
                        "hidden": false,
                        "clearOnHide": true,
                        "validate": {
                            "required": false,
                            "minLength": "",
                            "maxLength": "",
                            "pattern": "",
                            "custom": "",
                            "customPrivate": false
                        },
                        "conditional": {
                            "show": "",
                            "when": null,
                            "eq": ""
                        },
                        "type": "textfield",
                        "$$hashKey": "object:274",
                        "hideLabel": false,
                        "labelPosition": "top",
                        "tags": [],
                        "properties": {
                            "": ""
                        },
                        "customError": "Cannot read property 'length' of undefined"
                    }
                },
                mobile: {
                    title: 'تلفن همراه',
                    key: 'mobile',
                    icon: 'fa fa-terminal',
                    schema: {
                        "input": true,
                        "tableView": true,
                        "inputType": "text",
                        "inputMask": "09999999999",
                        "label": "تلفن همراه",
                        "key": "mobile",
                        "placeholder": "",
                        "prefix": "",
                        "suffix": "",
                        "multiple": false,
                        "defaultValue": "",
                        "protected": false,
                        "unique": false,
                        "persistent": true,
                        "hidden": false,
                        "clearOnHide": true,
                        "validate": {
                            "required": false,
                            "minLength": "",
                            "maxLength": "",
                            "pattern": "",
                            "custom": "",
                            "customPrivate": false
                        },
                        "conditional": {
                            "show": "",
                            "when": null,
                            "eq": ""
                        },
                        "type": "textfield",
                        "$$hashKey": "object:274",
                        "hideLabel": false,
                        "labelPosition": "top",
                        "tags": [],
                        "properties": {
                            "": ""
                        },
                        "customError": "Cannot read property 'length' of undefined"
                    }
                },
                country: {
                    title: 'کشور',
                    key: 'country',
                    icon: 'fa fa-terminal',
                    schema: {
                        "input": true,
                        "label": "کشور",
                        "key": "country",
                        "data": {
                            "values": [],
                            "json": "",
                            "url": "",
                            "resource": "",
                            "custom": "var token = null;\n" +
                            "var orderByField = \"geoName\";\n" +
                            "var geoTypeEnumId = \"GEOT_COUNTRY\";\n" +
                            "var geoId = null;\n" +
                            "var geoAssocTypeEnumId = null;\n" +
                            "// var host = data._coreHostRemote + \"/rest/s1/\";\n" +
                            "var host = window.location.origin + \"/rest/s1/\";\n" +
                            "var url = host + ( (!geoId) ? \"moqui/basic/geos?pageNoLimit=true\" : \"moqui/basic/geos/\"+geoId+\"/regions?pageNoLimit=true\");\n" +
                            "if (geoTypeEnumId) url += \"&geoTypeEnumId=\" + geoTypeEnumId;\n" +
                            "if (geoAssocTypeEnumId  && geoId) url += \"&geoAssocTypeEnumId=\" + geoAssocTypeEnumId;\n" +
                            "if (orderByField  && !geoId) url += \"&orderByField=\" + orderByField;\n" +
                            "var xhttp = new XMLHttpRequest();\n" +
                            "xhttp.onreadystatechange = function () { if (this.readyState == 4 && this.status == 200) values = JSON.parse(xhttp.responseText);};\n" +
                            "xhttp.open(\"GET\", url, false);\n" +
                            "if (token) {xhttp.setRequestHeader('Authorization', token);} else {xhttp.withCredentials = true;}\n" +
                            "xhttp.send();"
                        },
                        "dataSrc": "custom",
                        "valueProperty": "geoId",
                        "refreshOn": "province",
                        "authenticate": false,
                        "template": "<span>{{ item.geoName }}</span>",
                        "type": "select"
                    }
                },
                province: {
                    title: 'استان',
                    key: 'province',
                    icon: 'fa fa-terminal',
                    schema: {
                        "input": true,
                        "label": "استان",
                        "key": "province",
                        "data": {
                            "values": [],
                            "json": "",
                            "url": "",
                            "resource": "",
                            "custom": "var token = data.basicToken;\nvar orderByField = \"geoName\";\nvar geoTypeEnumId = \"GEOT_PROVINCE\";\nvar geoId = null;\nvar geoAssocTypeEnumId = null;\nvar host = window.location.origin+'/rest/s1/';\nvar url = host + ( (!geoId) ? \"moqui/basic/geos?pageNoLimit=true\" : \"moqui/basic/geos/\"+geoId+\"/regions?pageNoLimit=true\");\nif (geoTypeEnumId) url += \"&geoTypeEnumId=\" + geoTypeEnumId;\nif (geoAssocTypeEnumId  && geoId) url += \"&geoAssocTypeEnumId=\" + geoAssocTypeEnumId;\nif (orderByField  && !geoId) url += \"&orderByField=\" + orderByField;\nvar xhttp = new XMLHttpRequest();\nxhttp.onreadystatechange = function () { if (this.readyState == 4 && this.status == 200) values = JSON.parse(xhttp.responseText);};\nxhttp.open(\"GET\", url, false);\nif (token) {xhttp.setRequestHeader('Authorization', token);} else {xhttp.withCredentials = true;}\nxhttp.send();"
                        },
                        "dataSrc": "custom",
                        "valueProperty": "geoId",
                        "refreshOn": "",
                        "authenticate": false,
                        "template": "<span>{{ item.geoName }}</span>",
                        "type": "select"
                    }
                },
                city: {
                    title: 'شهرستان',
                    key: 'city',
                    icon: 'fa fa-terminal',
                    schema: {
                        "input": true,
                        "label": "شهرستان",
                        "key": "city",
                        "data": {
                            "values": [],
                            "json": "",
                            "url": "",
                            "resource": "",
                            "custom": "var token = data.basicToken;\nvar orderByField = \"geoName\";\nvar geoTypeEnumId = \"GEOT_CITY\";\nvar geoId = province;\nvar geoAssocTypeEnumId = null;\nvar host = window.location.origin+'/rest/s1/';\nvar url = host + ( (!geoId) ? \"moqui/basic/geos?pageNoLimit=true\" : \"moqui/basic/geos/\"+geoId+\"/regions?pageNoLimit=true\");\nif (geoTypeEnumId) url += \"&geoTypeEnumId=\" + geoTypeEnumId;\nif (geoAssocTypeEnumId  && geoId) url += \"&geoAssocTypeEnumId=\" + geoAssocTypeEnumId;\nif (orderByField  && !geoId) url += \"&orderByField=\" + orderByField;\nvar xhttp = new XMLHttpRequest();\nxhttp.onreadystatechange = function () {\n    if (this.readyState == 4 && this.status == 200)\n        values = JSON.parse(xhttp.responseText).resultList;};\nxhttp.open(\"GET\", url, false);\nif (token) {xhttp.setRequestHeader('Authorization', token);}\nelse {xhttp.withCredentials = true;}\nxhttp.send();"
                        },
                        "dataSrc": "custom",
                        "valueProperty": "geoId",
                        "refreshOn": "province",
                        "authenticate": false,
                        "template": "<span>{{ item.geoName }}</span>",
                        "type": "select"
                    }
                }

            }
        };
        var advancedComponents = {
            title: 'پیشرفته',
            weight: 1,
            components: {
                email: true,
                file: true,
                currency: true,
                embed: {
                    title: 'فرم',
                    key: 'embedForm',
                    icon: 'fa fa-terminal',
                    schema: {
                        "type": "embedForm",
                        "formSrc": "embedForm",
                        "componentKey": "embedForm",
                        "conditional": {
                            "show": "",
                            "when": null,
                            "eq": ""
                        },
                        "key": "embedForm",
                        "protected": false,
                        "disabled": false
                    }
                },
                t_combo: {
                    title: 'ترجمه انتخابی',
                    key: 't_combo',
                    icon: 'fa fa-terminal',
                    schema: {
                        "input": true,
                        "tableView": true,
                        "key": "t_combo",
                        "label": "t_combo",
                        "protected": false,
                        "unique": false,
                        "persistent": true,
                        "type": "hidden",
                        "hideLabel": false,
                        "tags": [],
                        "conditional": {
                            "show": "",
                            "when": null,
                            "eq": ""
                        },
                        "properties": {
                            "": ""
                        },
                        "calculateValue": "var target = instance.info.attr.name;\nvar apiName= \"applicantsTitle\";\ntarget=target.replace(instance.key,apiName);\nvar res = \"\";\n$(\"[name='\"+target+\"'] >option \").each(function(index){\n    res += $(this).children().text();\n    if(index>0){\n        res+=\", \";\n    }\n});\nvalue = res;"
                    }
                },
                enum: {
                    title: 'enumeration',
                    key: 'enum',
                    icon: 'fa fa-terminal',
                    schema: {
                        "label": "enum",
                        "clearOnHide": false,
                        "mask": false,
                        "tableView": true,
                        "alwaysEnabled": false,
                        "type": "select",
                        "input": true,
                        "key": "Enum",
                        "data": {
                            "custom": "values=getEnums(data.basicToken,\"EnumTypeId\")",
                            "values": [],
                            "json": "",
                            "url": "",
                            "resource": ""
                        },
                        "valueProperty": "enumId",
                        "defaultValue": "",
                        "tags": [],
                        "conditional": {
                            "show": "",
                            "when": "",
                            "json": "",
                            "eq": ""
                        },
                        "properties": {
                            "": ""
                        },
                        "lockKey": true,
                        "dataSrc": "custom",
                        "encrypted": false,
                        "template": "<span>{{ item.description }}</span>",
                        "validate": {
                            "required": true,
                            "select": false,
                            "customMessage": "",
                            "json": "",
                            "custom": "",
                            "customPrivate": false
                        },
                        "$$hashKey": "object:750",
                        "selectThreshold": 0.3,
                        "customConditional": "",
                        "logic": [],
                        "reorder": false,
                        "lazyLoad": false,
                        "selectValues": "",
                        "disableLimit": false,
                        "sort": "",
                        "reference": false,
                        "placeholder": "",
                        "prefix": "",
                        "customClass": "",
                        "suffix": "",
                        "multiple": false,
                        "protected": false,
                        "unique": false,
                        "persistent": true,
                        "hidden": false,
                        "dataGridLabel": false,
                        "labelPosition": "top",
                        "labelWidth": 30,
                        "labelMargin": 3,
                        "description": "",
                        "errorLabel": "",
                        "tooltip": "",
                        "hideLabel": false,
                        "tabindex": "",
                        "disabled": false,
                        "autofocus": false,
                        "dbIndex": false,
                        "customDefaultValue": "",
                        "calculateValue": "",
                        "allowCalculateOverride": false,
                        "widget": null,
                        "refreshOn": "",
                        "clearOnRefresh": false,
                        "validateOn": "change",
                        "limit": 100,
                        "filter": "",
                        "searchEnabled": true,
                        "searchField": "",
                        "minSearch": 0,
                        "readOnlyValue": false,
                        "authenticate": false,
                        "selectFields": "",
                        "searchThreshold": 0.3,
                        "fuseOptions": {},
                        "customOptions": {},
                        "id": "en204k5"
                    } },

                btnfiledownload: {
                    title: 'دانلود فایل از فرایند',
                    key: 'pdfDownloadButton',
                    icon: 'fa fa-download',
                    schema: {
                        "input": true,
                        "label": "btnfiledownload",
                        "tableView": false,
                        "key": "btnfiledownload",
                        "size": "md",
                        "leftIcon": "",
                        "rightIcon": "",
                        "block": false,
                        "action": "custom",
                        "disableOnInvalid": false,
                        "theme": "primary",
                        "type": "button",
                        "hideLabel": false,
                        "tags": [],
                        "conditional": {"show": "", "when": null, "eq": ""},
                        "properties": {"": ""},
                        "custom": "var varName = \"pdfOutputVariableName\";\r\nvar taskId= window.location.href.split(\"taskId=\")[1].split(\"&\")[0];\r\nvar url = window.location.origin +\"/bpms/api/engine/engine/default/task/\"+taskId+\"/variables/\"+varName+\"/data\";\r\nif(data.isHistoryView) url = window.location.origin +\"/bpms/api/engine/engine/default/history/variable-instance/\"+data.preValue[varName]+\"/data\";\r\n\r\nif(data.isHistoryView) url = window.location.origin +\"/bpms/api/engine/engine/default/history/variable-instance/\"+data.preValue[varName]+\"/data\";\r\nwindow.open( url,\"_blank\");",
                    }
                },
                tags: true,
                datetime: true,
                day: true,
                modaledit: true,
                url: true,
                custom: true,
                htmlelement: true,
                signature: true,
                survey: true,
                location: true,
                recaptcha: true,
                buttonPdf: {
                    title: 'buttonPdf',
                    key: 'buttonPdf',
                    icon: 'fa fa-book',
                    schema: {
                        "input": true,
                        "label": "buttonPdf",
                        "tableView": false,
                        "key": "buttonPdf",
                        "size": "md",
                        "leftIcon": "",
                        "rightIcon": "",
                        "block": false,
                        "action": "custom",
                        "disableOnInvalid": false,
                        "theme": "primary",
                        "type": "button",
                        "hideLabel": false,
                        "tags": [],
                        "conditional": {"show": "", "when": null, "eq": ""},
                        "properties": {"": ""},
                        "custom": "var token =null;//data._basicToken\n" +
                        "var host =window.location.origin;\n" +
                        "var url = host +\"/rest/s1/trn/common/export/pdf\";\n" +
                        "var data ={}; \n" +
                        "data.reportPath = '/m-ichto/export/productionLicenseSolitary.jasper'; \n" +
                        "data.pdfVariables = {'a':'a'}; \n" +
                        "var xhttp = new XMLHttpRequest();\n" +
                        "xhttp.onreadystatechange = function () {\n" +
                        "    if (this.readyState == 4 && this.status == 200){\n" +
                        "        var hiddenElement = document.createElement('a');\n" +
                        "        hiddenElement.href = 'data:application/pdf;base64,' + JSON.parse(xhttp.responseText).res;\n" +
                        "        hiddenElement.target = '_blank';\n" +
                        "        hiddenElement.download = 'trn-export.pdf';\n" +
                        "        hiddenElement.click();\n" +
                        "    }\n" +
                        "};\n" +
                        "xhttp.open(\"POST\", url, false);\n" +
                        "if (token) {xhttp.setRequestHeader('Authorization', token);} else {xhttp.withCredentials = true;}\n" +
                        "xhttp.setRequestHeader('Authorization','Basic '+token);\n" +
                        "xhttp.setRequestHeader('Content-Type','application/json');\n" +
                        "xhttp.setRequestHeader('Accept','application/json');\n" +
                        "xhttp.send(JSON.stringify(data));"
                    }
                },
                buttonPost: {
                    title: 'buttonPost',
                    key: 'buttonPost',
                    icon: 'fa fa-send',
                    schema: {
                        "input": true,
                        "label": "buttonPost",
                        "tableView": false,
                        "key": "buttonPost",
                        "size": "md",
                        "leftIcon": "",
                        "rightIcon": "",
                        "block": false,
                        "action": "custom",
                        "disableOnInvalid": false,
                        "theme": "primary",
                        "type": "button",
                        "hideLabel": false,
                        "tags": [],
                        "conditional": {"show": "", "when": null, "eq": ""},
                        "properties": {"": ""},
                        "custom": "var token =null;//data._basicToken\n" +
                        "var host =window.location.origin;\n" +
                        "var url = host +\"/rest/s1/trn/common/export/pdf\";\n" +
                        "var xhttp = new XMLHttpRequest();\n" +
                        "xhttp.onreadystatechange = function () {\n" +
                        "    if (this.readyState == 4 && this.status == 200){\n" +
                        "        var hiddenElement = document.createElement('a');\n" +
                        "        hiddenElement.href = 'data:application/pdf;base64,' + JSON.parse(xhttp.responseText).res;\n" +
                        "        hiddenElement.target = '_blank';\n" +
                        "        hiddenElement.download = 'trn-export.pdf';\n" +
                        "        hiddenElement.click();\n" +
                        "    }\n" +
                        "};\n" +
                        "xhttp.open(\"POST\", url, false);\n" +
                        "if (token) {xhttp.setRequestHeader('Authorization', token);} else {xhttp.withCredentials = true;}\n" +
                        "xhttp.setRequestHeader('Authorization','Basic '+token);\n" +
                        "xhttp.setRequestHeader('Content-Type','application/json');\n" +
                        "xhttp.setRequestHeader('Accept','application/json');\n" +
                        "xhttp.send(JSON.stringify(data));"
                    }
                },
                buttonGet: {
                    title: 'buttonGet',
                    key: 'buttonGet',
                    icon: 'fa fa-stop',
                    schema: {
                        "input": true,
                        "label": "buttonGet",
                        "tableView": false,
                        "key": "buttonGet",
                        "size": "md",
                        "leftIcon": "",
                        "rightIcon": "",
                        "block": false,
                        "action": "custom",
                        "disableOnInvalid": false,
                        "theme": "primary",
                        "type": "button",
                        "hideLabel": false,
                        "tags": [],
                        "conditional": {"show": "", "when": null, "eq": ""},
                        "properties": {"": ""},
                        "custom": "var token =null;//data._basicToken\n" +
                        "var host =window.location.origin;\n" +
                        "var url = host + \"/rest/s1/trn/common/export/pdf\";\n" +
                        "var xhttp = new XMLHttpRequest();\n" +
                        "xhttp.onreadystatechange = function () {\n" +
                        "    if (this.readyState == 4 && this.status == 200){\n" +
                        "        var hiddenElement = document.createElement('a');\n" +
                        "        hiddenElement.href = 'data:application/pdf;base64,' + JSON.parse(xhttp.responseText).res;\n" +
                        "        hiddenElement.target = '_blank';\n" +
                        "        hiddenElement.download = 'trn-export.pdf';\n" +
                        "        hiddenElement.click();\n" +
                        "    }\n" +
                        "};\n" +
                        "xhttp.open(\"GET\", url, false);\n" +
                        "if (token) {xhttp.setRequestHeader('Authorization', token);} else {xhttp.withCredentials = true;}\n" +
                        "xhttp.setRequestHeader('Authorization','Basic ' + token);\n" +
                        "xhttp.setRequestHeader('Content-Type','application/json');\n" +
                        "xhttp.setRequestHeader('Accept','application/json');\n" +
                        "xhttp.send();"
                    }
                }

            }
        };
        loadTrnPopertiesFormioJson();
        builder = new Formio.FormBuilder(document.getElementById("builder"), TaraanScope.formioJson, {
            builder: {
                advanced: false,
                customAdvanced: advancedComponents,
                customIR: irComponents,
                customERP: erpComponents
            }
        });
        setDisplay('form');
    };
    var onForm = function (form) {
        form.on('change', function () {
            subJSON.innerHTML = '';
            subJSON.appendChild(document.createTextNode(JSON.stringify(form.submission, null, 4)));
            subjsonINPUT.value = JSON.stringify(form.submission, null, 4);
        });
    };
    var setDisplay = function (display) {
        builder.setDisplay(display).then(function (instance) {
            instance.on('change', function (event) {
                TaraanScope.formioJson = instance.schema;
                jsonElement.innerHTML = '';
                formElement.innerHTML = '';
                jsonElement.appendChild(document.createTextNode(JSON.stringify(TaraanScope.formioJson, null, 4)));
                Formio.createForm(formElement, TaraanScope.formioJson).then(onForm);
            });
            Formio.createForm(formElement, TaraanScope.formioJson).then(onForm);
        });
    };

    // import form func
    function readFile(file) {
        TaraanScope.fileName = file.name;
        if (!file) {return;}
        var reader = new FileReader();
        reader.onload = function (e) {
            var contents = e.target.result;
            TaraanScope.formioJson = JSON.parse(contents);
            setBuilder('form');
        };
        reader.readAsText(file);
    }
    function saveFile() {
        setTrnPopertiesFormioJson();
        if(!urlParams.get("targetHash")){
            alert('ذخیره مستقیم فایل امکان پذیر نیست. شما فایل را از تب فایل ها انتخاب نکرده اید. این فایل دانلود می شود.');
            downloadFile();clearDraft();
            return;
        }
        var postData = {};
        postData.content = TaraanScope.formioJson;
        postData.targetHash = urlParams.get("targetHash");
        postData.targetPhash = urlParams.get("targetPhash");
        postData.resourceRoot = urlParams.get("resourceRoot");
        postData.moquiSessionToken = xmlCommonFuncs.getSession();

        var host = window.location.origin;
        var url = host +"/apps/tds/formBuilder/saveForm";
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200){
                if(this.responseText=="true"){
                    swal("تراکنش موفق!", "فرم با موفقیت ذخیره شد.", "success",{buttons:{
                            ok: "قبول، ادامه ویرایش",
                            okRedirect: {
                                text: "قبول، برگشت به فایل ها",
                                value: "files"
                            }
                        }}).then(function (value) {
                        switch (value) {
                            case "files":
                                clearDraft();
                                window.location.replace(window.location.origin+"/apps/tds/elFinder?resourceRoot="+urlParams.get("resourceRoot")+"#elf_"+urlParams.get("targetPhash"));
                                break;
                            default:
                                break;
                        }
                    });
                }
            }
        };
        xhttp.open("POST", url, false);
        xhttp.withCredentials = true;
        xhttp.setRequestHeader('Content-Type','application/json');
        xhttp.send(JSON.stringify(postData));
    }
    function downloadFile() {
        setTrnPopertiesFormioJson();
        var text =JSON.stringify(TaraanScope.formioJson);
        var element = document.createElement('a');
        element.setAttribute('href', 'data:application/json;charset=utf-8,' + encodeURIComponent(text));
        element.setAttribute('download', TaraanScope.fileName);
        element.style.display = 'none';
        document.body.appendChild(element);
        element.click();
        document.body.removeChild(element);
    }

    // make required & disable & toggleClearHidden & ... helper func
    function iterateEmbed(compName) {
        FormioUtils.eachComponent(TaraanScope.formioJson.components, function (component) {
            if (component.type == "embedForm") {
                if (!component.formSrc) {
                    alert(component.key + " : your embedded form doesn't have formSrc property");
                } else {
                    if (component.formSrc.indexOf("formio:/") != -1) {
                        component.formSrc = compName+"/bpms/forms/"+component.formSrc.split("/").slice(2,component.formSrc.split("/").length).join("/");
                    }
                    if (component.formSrc.substring(0, 12) == "component://") {
                        $.ajax({
                            url: window.location.origin + "/rest/s1/trn/form/jsonString?jsonSrcLocation=" + component.formSrc,
                            type: "get",
                            contentType: "application/json",
                            async: false,
                            timeout: 7000,
                            beforeSend: function (xhr) {
                                xhr.setRequestHeader("Authorization", xmlCommonFuncs.getToken());
                            }
                        }).done(function success(response) {
                            component.type = "fieldset";
                            component.persistent = false;
                            component.input = false;
                            component.hideLabel = false;
                            component.customClass = "fieldset-nostyle";
                            if (component.componentKey) {
                                component.components = [FormioUtils.getComponent(JSON.parse(response.jsonString).components, component.componentKey, true)];
                            } else {
                                component.components = JSON.parse(response.jsonString).components;
                            }
                            iterateEmbed(compName);
                        }).fail(function error(err) {
                            console.log("error getting embedded form "+component.formSrc, err);
                            alert("Internal Error - error getting embedded form " + err);
                        });
                    } else {
                        alert("invalid formSrc in your embedded form component : --> " + component.formSrc + " -- it should start with  formio:/");
                    }
                }
            }
        }, true);
    }
    function toggleEmbedForm() {
        var parentFormPath = xmlCommonFuncs.getPath();
        var compName = "";
        var cmps =JSON.parse(xmlCommonFuncs.getComponentList());
        for(var i =0; i < cmps.length;i++){
                if(parentFormPath.indexOf(cmps[i].folder)!=-1){
                    compName= cmps[i].name;
                    break;
                }
        }
        iterateEmbed(compName);
        alert("success: (WARNING)don't save the form after embed forms are viewed... refresh the page then edit/save the form")
    }
    function toggleClearHidden(formComponents) {
        var i = true;
        var newVal = false;
        FormioUtils.eachComponent(formComponents, function (comp) {
            if (comp.input) {
                if (!i) {
                    newVal = !comp.clearOnHide;
                    i = false;
                }
                comp.clearOnHide = newVal;
            }
        }, true);
        return formComponents;
    }
    function toggleDisabled(formComponents) {
        var i = false;
        var newVal = true;
        FormioUtils.eachComponent(formComponents, function (comp) {
            if (comp.input) {
                if (!i) {
                    newVal = !comp.disabled;
                    i = true;
                }
                comp.disabled = newVal;
            }
        }, true);
        return formComponents;
    }
    function toggleRequired(formComponents) {
        var i = false;
        var newVal = true;
        FormioUtils.eachComponent(formComponents, function (comp) {
            if (comp.input) {
                if (!i) {
                    newVal = !comp.validate.required;
                    i = true;
                }
                if (comp.hasOwnProperty("validate")) {
                    comp.validate.required = newVal;
                }
            }
        }, true);
        return formComponents;
    }
    function loadFile() {
        var host = window.location.origin;
        var url = host +"/apps/tds/formBuilder/loadFile";
        url +=("?resourceRoot="+urlParams.get("resourceRoot"));
        url +=("&targetHash="+urlParams.get("targetHash"));
        url +=("&targetPhash="+urlParams.get("targetPhash"));
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200){
                if(this.responseText.length > 5){
                    TaraanScope.formioJson =JSON.parse(this.responseText);
                }
            }
        };
        xhttp.open("GET", url, false);
        xhttp.withCredentials = true;
        xhttp.send();
    }

    // initiate form when have targetHash from elFinder
    loadFile();
    setBuilder();


    document.getElementById('dlEntity').addEventListener('click', function () {
        var f2entity = new FormToEntity();
        f2entity.setFormComponents(TaraanScope.formioJson.components);
        f2entity.download();
    });
    document.getElementById('requireForm').addEventListener('click', function () {
        toggleRequired(TaraanScope.formioJson.components);
        setBuilder();
    });
    document.getElementById('disableForm').addEventListener('click', function () {
        toggleDisabled(TaraanScope.formioJson.components);
        setBuilder();
    });
    document.getElementById('ToggleClearHidden').addEventListener('click', function () {
        toggleClearHidden(TaraanScope.formioJson.components);
        setBuilder();
    });
    document.getElementById('toggleEmbedForm').addEventListener('click', function () {
        toggleEmbedForm();
        setBuilder();
    });
    document.getElementById('form-select').addEventListener("change", function () {
        setDisplay(this.value);
    });

    document.getElementById('import').addEventListener('click', function () {
        document.getElementById('importFile').click();
    });
    document.getElementById('importFile').addEventListener('change', function () {
        xmlCommonFuncs.readFile(this.files[0],function (txt) {
            xmlCommonFuncs.setEditorXonomy(txt,spec,editor);
        });
    });
    document.getElementById('download').addEventListener('click', function () {
        downloadFile();clearDraft();
    });
    document.getElementById('save').addEventListener('click', function () {
        saveFile();clearDraft();
    });

    var draftStorageKey = "tds_draft_form";
    function startSavingDraft(){
        var oldObjStr = JSON.stringify(TaraanScope.formioJson);
        setInterval(function () {
            var newObjStr = JSON.stringify(TaraanScope.formioJson);
            if(oldObjStr.length != newObjStr.length) {
                localStorage.setItem(draftStorageKey,newObjStr);
                console.log("INFO: saved in draft")
            }
        },10000);
    }
    function loadDraft() {
        if(localStorage.hasOwnProperty(draftStorageKey)&&localStorage.getItem(draftStorageKey)!=""){
            swal("بازیابی فایل!", "شما تغییرات ذخیره نشده دارید", "warning",{buttons:{
                    dontRecovery: "عدم بازیابی و ادامه.",
                    recovery : {
                        text: "بازیابی فایل ذخیره نشده.",
                        value: "recovery"
                    }
                }}).then(function (value) {
                switch (value) {
                    case "recovery":
                        TaraanScope.formioJson = JSON.parse(localStorage.getItem(draftStorageKey));
                        setBuilder();
                        break;
                    default:
                        break;
                }
                clearDraft();
                startSavingDraft();
            });
        }else{
            setTimeout(function () {
                startSavingDraft();
            },5000);
        }
    }
    function clearDraft() {
        localStorage.setItem(draftStorageKey,"");
    }
    loadDraft();
}());

