(function () {
    var urlParams = new URL(window.location.href).searchParams;
    var artifactType = "process";
    var container = $('#js-drop-zone');
    taraanBpmnModeler.on('elementTemplates.errors', function(event) {
        console.log('template load errors', event.errors);
    });
    function setEditor(xml) {
        taraanBpmnModeler.importXML(xml, function (err) {
            if (err) {
                alert(err);
                container.removeClass('with-diagram').addClass('with-error');
                container.find('.error pre').text(err.message);
                console.error(err);
            } else {
                container.removeClass('with-error').addClass('with-diagram');
                var transactionBoundaries = taraanBpmnModeler.get('transactionBoundaries');
                transactionBoundaries.show();
                taraanBpmnModeler.get('minimap').open();
            }
        });
        taraanBpmnModeler.get('elementTemplates').set(trnElementTemplates);
    }
    function saveProcessFile() {
        taraanBpmnModeler.saveXML({format: true}, function (err, xml) {
            xmlCommonFuncs.saveFile(xml,artifactType);
            clearDraft();
        });
    }
    function downloadProcessFile() {
        taraanBpmnModeler.saveXML({format: true}, function (err, xml) {
            xmlCommonFuncs.downloadFile(xml,artifactType);
        });
    }
    function downloadSvgFile() {
        taraanBpmnModeler.saveSVG(function (err, xml) {
            var element = document.createElement('a');
            element.setAttribute('href', 'data:application/bpmn20-xml;charset=UTF-8,' + encodeURIComponent(xml));
            element.setAttribute('download', "export.svg");
            element.style.display = 'none';
            document.body.appendChild(element);
            element.click();
            document.body.removeChild(element);
        });
    }
    function initiateXml() {
        xml = xmlCommonFuncs.loadFile();
        var initXmlElement= document.getElementById('xmlInit');
        if(xml.length < 5){
            xml = $(initXmlElement).text();
        }
    }
    var xml = "";
    initiateXml();
    setEditor(xml);

    document.getElementById('import').addEventListener('click', function () {
        document.getElementById('importFile').click();
    });
    document.getElementById('importFile').addEventListener('change', function () {
        xmlCommonFuncs.readFile(this.files[0],function (txt) {
            setEditor(txt);
        });
    });
    document.getElementById('download').addEventListener('click', function () {
        downloadProcessFile();clearDraft();
    });
    document.getElementById('save').addEventListener('click', function () {
        saveProcessFile();
    });
    document.getElementById('js-download-svg').addEventListener('click', function () {
        downloadSvgFile();
    });

    var draftStorageKey = "tds_draft_process";
    function startSavingDraft(){
        var oldObjStr;
        taraanBpmnModeler.saveXML({ format: true }, function(err, xml) {oldObjStr=xml;});
        setInterval(function () {
            var newObjStr;
            taraanBpmnModeler.saveXML({ format: true }, function(err, xml) {newObjStr=xml;});
            if(oldObjStr.length != newObjStr.length) {
                localStorage.setItem(draftStorageKey,newObjStr);
                console.log("INFO: saved in draft");
                oldObjStr = newObjStr;
            }
        },10000);
    }
    function loadDraft() {
        if(localStorage.hasOwnProperty(draftStorageKey)&&localStorage.getItem(draftStorageKey)!=""){
            swal("بازیابی فایل!", "شما تغییرات ذخیره نشده دارید", "warning",{buttons:{
                    dontRecovery: "عدم بازیابی و ادامه.",
                    recovery : {
                        text: "بازیابی فایل ذخیره نشده.",
                        value: "recovery"
                    }
                }}).then(function (value) {
                switch (value) {
                    case "recovery":
                        setEditor(localStorage.getItem(draftStorageKey));
                        break;
                    default:
                        break;
                }
                clearDraft();
                startSavingDraft();
            });
        }else{
            setTimeout(function () {
                startSavingDraft();
            },5000);
        }
    }
    function clearDraft() {
        localStorage.setItem(draftStorageKey,"");
    }
    loadDraft();
}());
