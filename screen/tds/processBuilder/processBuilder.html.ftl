<input type="text" hidden value="${ec.web.sessionToken}" id="saveFormSessionToken">
<link rel="stylesheet" href="/trnstatic/3rd/taraan/modeler/bpmnModeler/css/diagram-js.css" />
<link rel="stylesheet" href="/trnstatic/3rd/taraan/modeler/bpmnModeler/vendor/bpmn-font/css/bpmn-embedded.css" />
<link rel="stylesheet" href="/trnstatic/3rd/taraan/modeler/bpmnModeler/vendor/comment/comments.css" />
<link rel="stylesheet" href="/trnstatic/3rd/taraan/modeler/bpmnModeler/comment/comment-style.css" />
<link rel="stylesheet" href="/trnstatic/3rd/taraan/modeler/bpmnModeler/vendor/diagram-js-minimap/assets/diagram-js-minimap.css" />
<link rel="stylesheet" href="/trnstatic/3rd/taraan/modeler/bpmnModeler/vendor/bpmn-js-token-simulation/assets/bpmn-js-token-simulation" />
<link rel="stylesheet" href="/trnstatic/3rd/taraan/modeler/bpmnModeler/css/app.css" />
<div class="content with-diagram" id="js-drop-zone" style="height: 600px;">
    <div class="message error">
        <div class="note">
            <p>Ooops, we could not display the BPMN 2.0 diagram.</p>
            <div class="details">
                <span>Import Error Details</span>
                <pre></pre>
            </div>
        </div>
    </div>
     <div class="canvas" id="js-canvas"></div>
     <div class="properties-panel-parent trnPropHide" id="js-properties-panel"></div>
</div>
<script src="/trnstatic/3rd/taraan/modeler/bpmnModeler/index.js" type="text/javascript"></script>
<script src="./common/xmlfunctions.js" type="text/javascript"></script>
<script src="./processBuilder/elementTemplates.js" type="text/javascript"></script>
<script src="./processBuilder/main.js" type="text/javascript"></script>