var trnElementTemplates = [
    {
        "name": "WebService",
        "id": "com.trn.tds.WebserviceTask",
        "appliesTo": [
            "bpmn:ServiceTask"
        ],
        "properties": [],
        "scopes": {
            "camunda:Connector": {
                "properties": [
                    {
                        "label": "ConnectorId",
                        "type": "Dropdown",
                        "choices": [
                            { "name": "http-connector", "value": "http-connector" },
                            { "name": "soap-connector", "value": "soap-connector" }
                        ],
                        "value": "http-connector",
                        "description": "camunda connectors like http-connector or soap-connector",
                        "binding": {
                            "type": "property",
                            "name": "connectorId"
                        },
                        "constraints": {
                            "notEmpty": true
                        }
                    },
                    {
                        "label": "Method",
                        "type": "Dropdown",
                        "value": "GET",
                        "choices": [
                            { "name": "GET", "value": "GET" },
                            { "name": "POST", "value": "POST" },
                            { "name": "PUT", "value": "PUT" },
                            { "name": "DELETE", "value": "DELETE" }
                        ],
                        "binding": {
                            "type": "camunda:inputParameter",
                            "name": "method"
                        },
                        "constraints": {
                            "notEmpty": true
                        }
                    },
                    {
                        "label": "URL",
                        "type": "Text",
                        "description": "url format is groovy ,put url inside single quote and parameter outside it.",
                        "value": "_coreHostLocal+'/rest/s1/test?parameter='+parameter\n\n\n",
                        "binding": {
                            "type": "camunda:inputParameter",
                            "name": "url",
                            "scriptFormat": "groovy"
                        },
                        "constraints": {
                            "notEmpty": true
                        }
                    },
                    {
                        "label": "Headers",
                        "description": "http headers for web service,add your header to headers map",
                        "value": "def headers = [:]\nheaders.put('Authorization',basicToken)\nheaders.put('Content-Type','application/json')\nheaders.put('accept','application/json')\n headers ",
                        "type": "Text",
                        "binding": {
                            "type": "camunda:inputParameter",
                            "name": "headers",
                            "scriptFormat": "groovy"
                        },
                        "constraints": {
                            "notEmpty": true
                        }
                    },
                    {
                        "label": "Payload",
                        "description": "fill payload if method is POST , you can use <a href=\"https://groovy.apache.org/\">groovy templates</a> here",
                        "value": "def mp = [:]\nmp.someValue = someValue;\nS(mp, org.camunda.spin.DataFormats.json()).toString()\n\n",
                        "type": "Text",
                        "binding": {
                            "type": "camunda:inputParameter",
                            "name": "payload",
                            "scriptFormat": "groovy"
                        },
                        "constraints": {
                            "notEmpty": false
                        }
                    },
                    {
                        "label": "Response",
                        "description": "response object ",
                        "type": "Text",
                        "value": "_ws",
                        "editable":true,
                        "binding": {
                            "type": "camunda:outputParameter",
                            "name": "_ws",
                            "source": "if(statusCode == 200) {\nif(S(response, org.camunda.spin.DataFormats.json()).isArray()) {\nS(response, org.camunda.spin.DataFormats.json()).mapTo('java.util.List');}\nelse if(S(response, org.camunda.spin.DataFormats.json()).isString()) {\nS(response, org.camunda.spin.DataFormats.json());\n}else if(S(response, org.camunda.spin.DataFormats.json()).isBoolean()) {\nS(response, org.camunda.spin.DataFormats.json().mapTo('java.lang.Boolean'));\n}else if(S(response, org.camunda.spin.DataFormats.json()).isNumber()) {\nS(response, org.camunda.spin.DataFormats.json().mapTo('java.lang.Long'));\n}else {\nS(response, org.camunda.spin.DataFormats.json()).mapTo('java.util.HashMap');};\n}else {\nthrow new Exception(response.toString())\n} ",
                            "scriptFormat": "groovy"
                        },
                        "constraints": {
                            "notEmpty": true
                        }
                    }
                ]
            }
        }
    },

    {
        "name": "Entity",
        "id": "com.trn.tds.EntityTask",
        "appliesTo": [
            "bpmn:ServiceTask"
        ],
        "properties": [],
        "scopes": {
            "camunda:Connector": {
                "properties": [
                    {
                        "label": "ConnectorId",
                        "type": "String",
                        "value": "http-connector",
                        "editable": false,
                        "binding": {
                            "type": "property",
                            "name": "connectorId"
                        },
                        "constraints": {
                            "notEmpty": true
                        }
                    },
                    {
                        "label": "Method",
                        "type": "String",
                        "value": "POST",
                        "editable":false,
                        "binding": {
                            "type": "camunda:inputParameter",
                            "name": "method"
                        },
                        "constraints": {
                            "notEmpty": true
                        }
                    },
                    {
                        "label": "URL",
                        "type": "String",
                        "editable": false,
                        "value": "_coreHostLocal+'/rest/s1/trn/common/crud'",
                        "binding": {
                            "type": "camunda:inputParameter",
                            "name": "url",
                            "scriptFormat": "groovy"
                        },
                        "constraints": {
                            "notEmpty": true
                        }
                    },
                    {
                        "label": "Headers",
                        "description": "http headers for web service,add your header to headers map",
                        "value": "def headers = [:]\nheaders.put('Authorization',basicToken)\nheaders.put('Content-Type','application/json')\n headers ",
                        "type": "Text",
                        "binding": {
                            "type": "camunda:inputParameter",
                            "name": "headers",
                            "scriptFormat": "groovy"
                        },
                        "constraints": {
                            "notEmpty": true
                        }
                    },
                    {
                        "label": "EntityOperationData",
                        "description": "add variables to post",
                        "value": "def mp = [:]\nmp.entityName = 'mantle.party.Party'\nmp.masterName='its optional'\nmp.operationType = 'CREATE/UPDATE/STORE/DELETE/ONE/LIST/COUNT'\nmp.postData = postDataVariableMap;\nS(mp, org.camunda.spin.DataFormats.json()).toString()\n",
                        "type": "Text",
                        "binding": {
                            "type": "camunda:inputParameter",
                            "name": "payload",
                            "scriptFormat": "groovy"
                        },
                        "constraints": {
                            "notEmpty": false
                        }
                    },
                    {
                        "label": "Response",
                        "description": "response object ",
                        "type": "String",
                        "value": "_ws",
                        "editable":true,
                        "binding": {
                            "type": "camunda:outputParameter",
                            "name": "_ws",
                            "source": "if(statusCode == 200) {\nS(response, org.camunda.spin.DataFormats.json()).mapTo('java.util.HashMap').output;\n}else {\nthrow new Exception(response.toString())\n} ",
                            "scriptFormat": "groovy"
                        },
                        "constraints": {
                            "notEmpty": true
                        }
                    }
                ]
            }
        }
    },

    {
        "name": "PDF",
        "id": "com.trn.tds.pdf.Task",
        "appliesTo": [
            "bpmn:ServiceTask"
        ],
        "properties": [],
        "scopes": {
            "camunda:Connector": {
                "properties": [
                    {
                        "label": "ConnectorId",
                        "type": "Dropdown",
                        "choices": [
                            { "name": "http-connector", "value": "http-connector" },
                            { "name": "soap-connector", "value": "soap-connector" }
                        ],
                        "value": "http-connector",
                        "description": "camunda connectors like http-connector or soap-connector",
                        "binding": {
                            "type": "property",
                            "name": "connectorId"
                        },
                        "constraints": {
                            "notEmpty": true
                        }
                    },
                    {
                        "label": "Method",
                        "type": "String",
                        "value": "POST",
                        "editable":false,
                        "binding": {
                            "type": "camunda:inputParameter",
                            "name": "method"
                        },
                        "constraints": {
                            "notEmpty": true
                        }
                    },
                    {
                        "label": "URL",
                        "type": "String",
                        "description": "url webservice to create pdf",
                        "value": "_coreHostLocal+'/rest/s1/trn/common/export/pdf'\n\n\n",
                        "editable":false,
                        "binding": {
                            "type": "camunda:inputParameter",
                            "name": "url",
                            "scriptFormat": "groovy"
                        },
                        "constraints": {
                            "notEmpty": true
                        }
                    },
                    {
                        "label": "Headers",
                        "description": "http headers for web service,add your header to headers map",
                        "value": "def headers = [:]\nheaders.put('Authorization',basicToken)\nheaders.put('Content-Type','application/json')\n headers ",
                        "type": "Text",
                        "binding": {
                            "type": "camunda:inputParameter",
                            "name": "headers",
                            "scriptFormat": "groovy"
                        },
                        "constraints": {
                            "notEmpty": true
                        }
                    },
                    {
                        "label": "PdfInputData",
                        "description": "add variables to post",
                        "value": "def mp = [:]\nmp.reportPath = '/m-mcls/export/mclsEmployee.jasper'\nmp.pdfVariables = reportVariables;\nS(mp, org.camunda.spin.DataFormats.json()).toString()\n",
                        "type": "Text",
                        "binding": {
                            "type": "camunda:inputParameter",
                            "name": "payload",
                            "scriptFormat": "groovy"
                        },
                        "constraints": {
                            "notEmpty": false
                        }
                    },
                    {
                        "label": "Response",
                        "description": "response object ",
                        "type": "String",
                        "value": "_ws",
                        "binding": {
                            "type": "camunda:outputParameter",
                            "source": "import org.camunda.bpm.engine.variable.value.FileValue\nimport org.camunda.bpm.engine.variable.Variables\nimport sun.misc.BASE64Decoder\nString filename = 'file.pdf';\nbyte[] decodedBytes = null;\nif(statusCode == 200) {\nBASE64Decoder decoder = new BASE64Decoder();\ntry {decodedBytes = decoder.decodeBuffer(S(response, org.camunda.spin.DataFormats.json()).mapTo('java.util.HashMap').res);\n} catch (IOException e) {\ne.printStackTrace();}\nFileValue typedFileValue = Variables.fileValue(filename).file(decodedBytes).mimeType('text/plain').encoding('UTF-8').create()\ntypedFileValue}else {\nthrow new Exception(response.toString())} ",
                            "scriptFormat": "groovy"
                        },
                        "constraints": {
                            "notEmpty": true
                        }
                    }
                ]
            }
        }
    },

    {
        "name": "SMS",
        "id": "com.trn.tds.sms.Task",
        "appliesTo": [
            "bpmn:ServiceTask"
        ],
        "properties": [],
        "scopes": {
            "camunda:Connector": {
                "properties": [
                    {
                        "label": "ConnectorId",
                        "type": "Dropdown",
                        "choices": [
                            { "name": "http-connector", "value": "http-connector" },
                            { "name": "soap-connector", "value": "soap-connector" }
                        ],
                        "value": "http-connector",
                        "description": "camunda connectors like http-connector or soap-connector",
                        "binding": {
                            "type": "property",
                            "name": "connectorId"
                        },
                        "constraints": {
                            "notEmpty": true
                        }
                    },
                    {
                        "label": "Method",
                        "type": "String",
                        "value": "POST",
                        "editable":false,
                        "binding": {
                            "type": "camunda:inputParameter",
                            "name": "method"
                        },
                        "constraints": {
                            "notEmpty": true
                        }
                    },
                    {
                        "label": "URL",
                        "type": "Text",
                        "description": "sms url format is groovy ,",
                        "value": "_coreHostLocal+'/rest/s1/test'\n\n\n",
                        "binding": {
                            "type": "camunda:inputParameter",
                            "name": "url",
                            "scriptFormat": "groovy"
                        },
                        "constraints": {
                            "notEmpty": true
                        }
                    },
                    {
                        "label": "Headers",
                        "description": "http headers for web service,add your header to headers map",
                        "value": "def headers = [:]\nheaders.put('Authorization',basicToken)\nheaders.put('Content-Type','application/json')\nheaders.put('accept','application/json')\n headers ",
                        "type": "Text",
                        "binding": {
                            "type": "camunda:inputParameter",
                            "name": "headers",
                            "scriptFormat": "groovy"
                        },
                        "constraints": {
                            "notEmpty": true
                        }
                    },
                    {
                        "label": "SMSInputData",
                        "description": "text is variable ",
                        "value": "def mp = [:]\nmp.username= starter;\nmp.content = '';\nS(mp, org.camunda.spin.DataFormats.json()).toString()",
                        "type": "Text",
                        "binding": {
                            "type": "camunda:inputParameter",
                            "name": "payload",
                            "scriptFormat": "groovy"
                        },
                        "constraints": {
                            "notEmpty": false
                        }
                    },
                    {
                        "label": "Response",
                        "description": "response object ",
                        "type": "String",
                        "value": "_ws",
                        "editable":false,
                        "binding": {
                            "type": "camunda:outputParameter",
                            "name": "_ws",
                            "source": "if(statusCode == 200) {\nresponse.toString();\n}else {\nthrow new Exception(response.toString())\n} ",
                            "scriptFormat": "groovy"
                        },
                        "constraints": {
                            "notEmpty": true
                        }
                    }
                ]
            }
        }
    },

    {
        "name": "Mail",
        "id": "com.trn.tds.MailTask",
        "appliesTo": [
            "bpmn:ServiceTask"
        ],
        "properties": [],
        "scopes": {
            "camunda:Connector": {
                "properties": [
                    {
                        "label": "ConnectorId",
                        "type": "Dropdown",
                        "choices": [
                            { "name": "http-connector", "value": "http-connector" },
                            { "name": "soap-connector", "value": "soap-connector" }
                        ],
                        "value": "http-connector",
                        "description": "camunda connectors like http-connector or soap-connector",
                        "binding": {
                            "type": "property",
                            "name": "connectorId"
                        },
                        "constraints": {
                            "notEmpty": true
                        }
                    },
                    {
                        "label": "Method",
                        "type": "String",
                        "value": "POST",
                        "editable":false,
                        "binding": {
                            "type": "camunda:inputParameter",
                            "name": "method"
                        },
                        "constraints": {
                            "notEmpty": true
                        }
                    },
                    {
                        "label": "URL",
                        "type": "Text",
                        "description": "url format is groovy ,put url inside single quote and parameter outside it.",
                        "value": "_coreHostLocal+'/rest/s1/moqui/email/templates/'+emailTemplateId+'/send'\n\n\n",
                        "binding": {
                            "type": "camunda:inputParameter",
                            "name": "url",
                            "scriptFormat": "groovy"
                        },
                        "constraints": {
                            "notEmpty": true
                        }
                    },
                    {
                        "label": "Headers",
                        "description": "http headers for web service,add your header to headers map",
                        "value": "def headers = [:]\nheaders.put('Authorization',basicToken)\nheaders.put('Content-Type','application/json')\nheaders.put('accept','application/json')\n headers ",
                        "type": "Text",
                        "binding": {
                            "type": "camunda:inputParameter",
                            "name": "headers",
                            "scriptFormat": "groovy"
                        },
                        "constraints": {
                            "notEmpty": true
                        }
                    },
                    {
                        "label": "EmailInputData",
                        "description": "fromAddress and toAddress  must be valid Email. if you have more than one toAddress put in side string and separate with comma",
                        "value": "def mp = [:]\ndef bodyParameters = [:]\ndef emailServer = [:]\nemailServer.smtpHost = smtpHost\nmp.fromAddress= fromEmailAddress;\nmp.fromName= fromName;\nmp.toAddress= toEmailAddress;\nmp.subject = subject;\nmp.bodyParameters = bodyParameters;\nmp.emailServer = emailServer;\nS(mp, org.camunda.spin.DataFormats.json()).toString()",
                        "type": "Text",
                        "binding": {
                            "type": "camunda:inputParameter",
                            "name": "payload",
                            "scriptFormat": "groovy"
                        },
                        "constraints": {
                            "notEmpty": false
                        }
                    },
                    {
                        "label": "Response",
                        "description": "response object ",
                        "type": "String",
                        "value": "_ws",
                        "editable":false,
                        "binding": {
                            "type": "camunda:outputParameter",
                            "name": "_ws",
                            "source": "if(statusCode == 200) {\nresponse.toString();\n}else {\nthrow new Exception(response.toString())\n} ",
                            "scriptFormat": "groovy"
                        },
                        "constraints": {
                            "notEmpty": true
                        }
                    }
                ]
            }
        }
    },

    {
        "name": "CSV",
        "id": "com.trn.tds.CSV",
        "appliesTo": [
            "bpmn:ServiceTask"
        ],
        "properties": [],
        "scopes": {
            "camunda:Connector": {
                "properties": [
                    {
                        "label": "ConnectorId",
                        "type": "Dropdown",
                        "choices": [
                            { "name": "http-connector", "value": "http-connector" },
                            { "name": "soap-connector", "value": "soap-connector" }
                        ],
                        "value": "http-connector",
                        "description": "camunda connectors like http-connector or soap-connector",
                        "binding": {
                            "type": "property",
                            "name": "connectorId"
                        },
                        "constraints": {
                            "notEmpty": true
                        }
                    },
                    {
                        "label": "Method",
                        "type": "String",
                        "value": "POST",
                        "editable":false,
                        "binding": {
                            "type": "camunda:inputParameter",
                            "name": "method"
                        },
                        "constraints": {
                            "notEmpty": true
                        }
                    },
                    {
                        "label": "URL",
                        "type": "Text",
                        "description": "url format is groovy ,put url inside single quote and parameter outside it.",
                        "value": "_coreHostLocal+'/rest/s1/trn/common/csv'\n\n\n",
                        "binding": {
                            "type": "camunda:inputParameter",
                            "name": "url",
                            "scriptFormat": "groovy"
                        },
                        "constraints": {
                            "notEmpty": true
                        }
                    },
                    {
                        "label": "Headers",
                        "description": "http headers for web service,add your header to headers map",
                        "value": "def headers = [:]\nheaders.put('Authorization',basicToken)\nheaders.put('Content-Type','application/json')\nheaders.put('accept','application/json')\n headers ",
                        "type": "Text",
                        "binding": {
                            "type": "camunda:inputParameter",
                            "name": "headers",
                            "scriptFormat": "groovy"
                        },
                        "constraints": {
                            "notEmpty": true
                        }
                    },
                    {
                        "label": "Data",
                        "description": "add variables to post",
                        "value": "def mp = [:]\nmp.someValue = someValue\nS(mp, org.camunda.spin.DataFormats.json()).toString();\n\n",
                        "type": "Text",
                        "binding": {
                            "type": "camunda:inputParameter",
                            "name": "payload",
                            "scriptFormat": "groovy"
                        },
                        "constraints": {
                            "notEmpty": false
                        }
                    },
                    {
                        "label": "Response",
                        "description": "response object ",
                        "type": "String",
                        "value": "_ws",
                        "editable":false,
                        "binding": {
                            "type": "camunda:outputParameter",
                            "name": "_ws",
                            "source": "if(statusCode == 200) {S(response, org.camunda.spin.DataFormats.json()).mapTo('java.util.HashMap').res;\n}else {\nthrow new Exception(response.toString())\n}  ",
                            "scriptFormat": "groovy"
                        },
                        "constraints": {
                            "notEmpty": true
                        }
                    }
                ]
            }
        }
    },

];
