<div id="elfinder"></div>
<link rel="stylesheet"
      href="/trnstatic/3rd/elfinder/css/smoothness/jquery-ui.min.css"
      type="text/css"/>
<link rel="stylesheet"
      href="/trnstatic/3rd/elfinder/css/smoothness/theme.min.css"
      type="text/css"/>
<script src="/trnstatic/3rd/elfinder/js/jquery-ui.min.js"
        type="text/javascript"></script>

<link rel="stylesheet" href="/trnstatic/3rd/elfinder/css/elfinder.min.css"
      type="text/css"/>
<link rel="stylesheet" href="/trnstatic/3rd/elfinder/css/theme.min.css"
      type="text/css"/>
<script src="/trnstatic/3rd/elfinder/js/elfinder.min.js"
        type="text/javascript"></script>
<script src="/trnstatic/3rd/elfinder/i18n/elfinder.fa.js"
        type="text/javascript"></script>

<script src="/trnstatic/3rd/elfinder/js/ace.js" type="text/javascript"></script>
<script src="/trnstatic/3rd/elfinder/js/ext-modelist.js" type="text/javascript"></script>
<script src="/trnstatic/3rd/elfinder/js/ext-settings_menu.js" type="text/javascript"></script>
<script src="/trnstatic/3rd/elfinder/js/ext-language_tools.js" type="text/javascript"></script>
<script>
    (function () {
        $('#elfinder').elfinder({
            url: '${sri.buildUrl('command').url}',
            customData: {moquiSessionToken: '${ec.web.sessionToken}', resourceRoot: '${resourceRoot}'},
            lang: 'fa',
            defaultView: 'grid',
            requestType: 'post',
            rememberLastDir: false,
            height: 700,
            commands: ['open', 'reload', 'home', 'up', 'back', 'forward', 'getfile', 'quicklook',
                'download', 'rm', 'rename', 'mkdir', 'mkfile', 'upload', 'edit', 'info', 'view', 'help',
                'sort', 'duplicate','copy', 'paste', 'search'],
            uiOptions: {
                // toolbar configuration (NOTE: 'quicklook' doesn't seem to work)
                toolbar: [
                    ['back', 'forward'],
                    ['reload', 'home', 'up'],
                    ['mkdir', 'mkfile', 'upload'],
                    ['copy','paste','duplicate'],
                    ['open', 'download'],
                    ['rm', 'rename', 'edit'],
                    ['view','info'],
                    ['search']
                ],
                navbar: {minWidth: 150, maxWidth: 800}
            },
            commandsOptions: {
                edit: {
                    editors: [
                        {
                            // ACE Editor
                            load: function (textarea) {
                                var self = this,
                                        dfrd = $.Deferred(),
                                        cdn = '//cdnjs.cloudflare.com/ajax/libs/ace/1.2.5',
                                        init = function () {
                                            if (typeof ace === 'undefined') {
                                                this.fm.loadScript([
                                                    cdn + '/ace.js',
                                                    cdn + '/ext-modelist.js',
                                                    cdn + '/ext-settings_menu.js',
                                                    cdn + '/ext-language_tools.js'
                                                ], start);
                                            } else {
                                                start();
                                            }
                                        },
                                        start = function () {
                                            var editor, editorBase, mode,
                                                    ta = $(textarea),
                                                    taBase = ta.parent(),
                                                    dialog = taBase.parent(),
                                                    id = textarea.id + '_ace',
                                                    ext = self.file.name.replace(/^.+\.([^.]+)|(.+)$/, '$1$2').toLowerCase(),
                                                    // MIME/mode map
                                                    mimeMode = {
                                                        'text/x-php': 'php',
                                                        'application/x-php': 'php',
                                                        'text/html': 'html',
                                                        'application/xhtml+xml': 'html',
                                                        'text/javascript': 'javascript',
                                                        'application/javascript': 'javascript',
                                                        'text/css': 'css',
                                                        'text/x-c': 'c_cpp',
                                                        'text/x-csrc': 'c_cpp',
                                                        'text/x-chdr': 'c_cpp',
                                                        'text/x-c++': 'c_cpp',
                                                        'text/x-c++src': 'c_cpp',
                                                        'text/x-c++hdr': 'c_cpp',
                                                        'text/x-shellscript': 'sh',
                                                        'application/x-csh': 'sh',
                                                        'text/x-python': 'python',
                                                        'text/x-java': 'java',
                                                        'text/x-java-source': 'java',
                                                        'text/x-groovy': 'groovy',
                                                        'text/x-groovy-source': 'groovy',
                                                        'text/x-ruby': 'ruby',
                                                        'text/x-perl': 'perl',
                                                        'application/x-perl': 'perl',
                                                        'text/x-sql': 'sql',
                                                        'text/xml': 'xml',
                                                        'application/docbook+xml': 'xml',
                                                        'application/xml': 'xml'
                                                    };

                                            // set basePath of ace
                                            ace.config.set('basePath', cdn);

                                            // set base height
                                            taBase.height(taBase.height());

                                            // detect mode
                                            mode = ace.require('ace/ext/modelist').getModeForPath('/' + self.file.name).name;
                                            if (mode === 'text') {
                                                if (mimeMode[self.file.mime]) {
                                                    mode = mimeMode[self.file.mime];
                                                }
                                            }

                                            // show MIME:mode in title bar
                                            taBase.prev().children('.elfinder-dialog-title').append(' (' + self.file.mime + ' : ' + mode.split(/[\/\\]/).pop() + ')');

                                            // TextArea button and Setting button
                                            $('<div class="ui-dialog-buttonset"/>').css('float', 'left')
                                                    .append(
                                                            $('<button>TextArea</button>')
                                                                    .button()
                                                                    .on('click', function () {
                                                                        if (ta.data('ace')) {
                                                                            ta.removeData('ace');
                                                                            editorBase.hide();
                                                                            ta.val(editor.session.getValue()).show().focus();
                                                                            $(this).text('AceEditor');
                                                                        } else {
                                                                            ta.data('ace', true);
                                                                            editorBase.show();
                                                                            editor.setValue(ta.hide().val(), -1);
                                                                            editor.focus();
                                                                            $(this).text('TextArea');
                                                                        }
                                                                    })
                                                    )
                                                    .append(
                                                            $('<button>Ace editor setting</button>')
                                                                    .button({
                                                                        icons: {
                                                                            primary: 'ui-icon-gear',
                                                                            secondary: 'ui-icon-triangle-1-e'
                                                                        },
                                                                        text: false
                                                                    })
                                                                    .on('click', function () {
                                                                        editor.showSettingsMenu();
                                                                    })
                                                    )
                                                    .prependTo(taBase.next());

                                            // Base node of Ace editor
                                            editorBase = $('<div id="' + id + '" style="width:100%; height:100%;"/>').text(ta.val()).insertBefore(ta.hide());

                                            // Ace editor configure
                                            ta.data('ace', true);
                                            editor = ace.edit(id);
                                            ace.require('ace/ext/language_tools');
                                            ace.require('ace/ext/settings_menu').init(editor);
                                            editor.$blockScrolling = Infinity;
                                            editor.setOptions({
                                                theme: 'ace/theme/monokai',
                                                mode: 'ace/mode/' + mode,
                                                fontSize: '14px',
                                                wrap: true,
                                                enableBasicAutocompletion: true,
                                                enableSnippets: true,
                                                enableLiveAutocompletion: false
                                            });
                                            editor.commands.addCommand({
                                                name: "saveFile",
                                                bindKey: {
                                                    win: 'Ctrl-s',
                                                    mac: 'Command-s'
                                                },
                                                exec: function (editor) {
                                                    self.doSave();
                                                }
                                            });
                                            editor.commands.addCommand({
                                                name: "closeEditor",
                                                bindKey: {
                                                    win: 'Ctrl-w|Ctrl-q',
                                                    mac: 'Command-w|Command-q'
                                                },
                                                exec: function (editor) {
                                                    self.doCancel();
                                                }
                                            });

                                            editor.resize();

                                            dfrd.resolve(editor);
                                        };

                                // init & start
                                init();

                                return dfrd;
                            },
                            close: function (textarea, instance) {
                                if (instance) {
                                    instance.destroy();
                                    $(textarea).show();
                                }
                            },
                            save: function (textarea, instance) {
                                instance && $(textarea).data('ace') && (textarea.value = instance.session.getValue());
                            },
                            focus: function (textarea, instance) {
                                instance && $(textarea).data('ace') && instance.focus();
                            },
                            resize: function (textarea, instance, e, data) {
                                instance && instance.resize();
                            }
                        }
                    ]
                }
            },
            handlers: {
                dblclick: function (event, instance) {
                    var selectedFile = instance.file(event.data.file);
                    var selectedFilePath = instance.path(selectedFile.hash);
                    if (selectedFile.mime === "application/json") {
                        event.preventDefault();
                        redirectToFormBuilder(selectedFile);
                    } else if (selectedFile.name.includes(".bpmn")) {
                        event.preventDefault();
                        redirectToProcessBuilder(selectedFile);
                    } else if (selectedFile.name.includes(".jasper")) {
                        event.preventDefault();
                        redirectToReportTester(selectedFile);
                    } else if (selectedFile.name.includes(".dmn")) {
                        event.preventDefault();
                        redirectToDecisionBuilder(selectedFile);
                    } else if (selectedFile.mime === "text/xml") {
                        event.preventDefault();
                        var fileType = getXmlFileType(selectedFile, selectedFilePath);
                        redirectToXmlBuilder(selectedFile, fileType);
                    }
                }
            }

        });

        function redirectToFormBuilder(selectedFile) {
            var theUrl = "/apps/tds/formBuilder";
            theUrl += "?targetHash=";
            theUrl += selectedFile.hash;
            theUrl += "&targetPhash=";
            theUrl += selectedFile.phash;
            theUrl += "&resourceRoot=";
            theUrl += '${resourceRoot}';
            window.location.replace(theUrl);
        }
        function redirectToReportTester(selectedFile) {
            var theUrl = "/apps/tds/reportTester";
            theUrl += "?targetHash=";
            theUrl += selectedFile.hash;
            theUrl += "&targetPhash=";
            theUrl += selectedFile.phash;
            theUrl += "&resourceRoot=";
            theUrl += '${resourceRoot}';
            window.location.replace(theUrl);
        }
        function redirectToProcessBuilder(selectedFile) {
            var theUrl = "/apps/tds/processBuilder";
            theUrl += "?targetHash=";
            theUrl += selectedFile.hash;
            theUrl += "&targetPhash=";
            theUrl += selectedFile.phash;
            theUrl += "&resourceRoot=";
            theUrl += '${resourceRoot}';
            window.location.replace(theUrl);
        }
        function redirectToDecisionBuilder(selectedFile) {
            var theUrl = "/apps/tds/decisionBuilder";
            theUrl += "?targetHash=";
            theUrl += selectedFile.hash;
            theUrl += "&targetPhash=";
            theUrl += selectedFile.phash;
            theUrl += "&resourceRoot=";
            theUrl += '${resourceRoot}';
            window.location.replace(theUrl);
        }

        function redirectToXmlBuilder(selectedFile, fileType) {
            var redirectUrl = "/apps/tds/" + fileType + "Builder";
            redirectUrl += "?targetHash=";
            redirectUrl += selectedFile.hash;
            redirectUrl += "&targetPhash=";
            redirectUrl += selectedFile.phash;
            redirectUrl += "&fileType=";
            redirectUrl += fileType;
            redirectUrl += "&resourceRoot=";
            redirectUrl += '${resourceRoot}';
            window.location.replace(redirectUrl);
        }

        function getXmlFileType(selectedFile, selectedFilePath) {
            var fileType = "xml";
            if (selectedFile.name === "MoquiConf.xml") {
                fileType = "conf";
            } else if (selectedFile.name.indexOf(".rest.xml") !== -1) {
                fileType = "rest";
            } else if (selectedFilePath.indexOf("/screen/") !== -1) {
                fileType = "screen";
            } else if (selectedFilePath.indexOf("/service/") !== -1) {
                fileType = "service";
            } else if (selectedFilePath.indexOf("/entity/") !== -1) {
                fileType = "entity";
            } else if (selectedFilePath.indexOf("/data/") !== -1) {
                fileType = "data";
            }
            return fileType;
        }
    }());
</script>