var xmlCommonFuncs = {};
var urlParams = new URL(window.location.href).searchParams;
xmlCommonFuncs.loadFile = function () {
    var host = window.location.origin;
    var url = host + "/apps/tds/loadFile";
    url += ("?resourceRoot=" + urlParams.get("resourceRoot"));
    url += ("&targetHash=" + urlParams.get("targetHash"));
    url += ("&targetPhash=" + urlParams.get("targetPhash"));
    if(!urlParams.get("targetHash")){
        return '';
    }
    var res = "";
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            if (this.responseText.length > 5) {
                res= this.responseText;
            }
        }
    };
    xhttp.open("GET", url, false);
    xhttp.withCredentials = true;
    xhttp.send();
    return res;
};
xmlCommonFuncs.getSession = function () {
    var host = window.location.origin;
    var url = host + "/apps/tds/moquiSessionToken";
    url += ("?resourceRoot=" + urlParams.get("resourceRoot"));
    url += ("&targetHash=" + urlParams.get("targetHash"));
    url += ("&targetPhash=" + urlParams.get("targetPhash"));
    if(!urlParams.get("targetHash")){
        return '';
    }
    var res = "";
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            if (this.responseText.length > 5) {
                res= this.responseText;
            }
        }
    };
    xhttp.open("GET", url, false);
    xhttp.withCredentials = true;
    xhttp.send();
    return res;
};
xmlCommonFuncs.getToken = function () {
    var host = window.location.origin;
    var url = host + "/apps/tds/getToken";
    url += ("?resourceRoot=" + urlParams.get("resourceRoot"));
    url += ("&targetHash=" + urlParams.get("targetHash"));
    url += ("&targetPhash=" + urlParams.get("targetPhash"));
    if(!urlParams.get("targetHash")){
        return '';
    }
    var res = "";
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            if (this.responseText.length > 5) {
                res= this.responseText;
            }
        }
    };
    xhttp.open("GET", url, false);
    xhttp.withCredentials = true;
    xhttp.send();
    return res;
};
xmlCommonFuncs.getPath = function () {
    var host = window.location.origin;
    var url = host + "/apps/tds/getFilePath";
    url += ("?resourceRoot=" + urlParams.get("resourceRoot"));
    url += ("&targetHash=" + urlParams.get("targetHash"));
    url += ("&targetPhash=" + urlParams.get("targetPhash"));
    if(!urlParams.get("targetHash")){
        return '';
    }
    var res = "";
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            if (this.responseText.length > 5) {
                res= this.responseText;
            }
        }
    };
    xhttp.open("GET", url, false);
    xhttp.withCredentials = true;
    xhttp.send();
    return res;
};
xmlCommonFuncs.getComponentList = function () {
    var host = window.location.origin;
    var url = host + "/apps/tds/componentList";
    url += ("?resourceRoot=" + urlParams.get("resourceRoot"));
    url += ("&targetHash=" + urlParams.get("targetHash"));
    url += ("&targetPhash=" + urlParams.get("targetPhash"));
    if(!urlParams.get("targetHash")){
        return '';
    }
    var res = "";
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            if (this.responseText.length > 5) {
                res= this.responseText;
            }
        }
    };
    xhttp.open("GET", url, false);
    xhttp.withCredentials = true;
    xhttp.send();
    return res;
};
xmlCommonFuncs.saveFile = function (xml,type) {
    if(!urlParams.get("targetHash")){
        alert('ذخیره مستقیم فایل امکان پذیر نیست. شما فایل را از تب فایل ها انتخاب نکرده اید. این فایل دانلود می شود.');
        xmlCommonFuncs.downloadFile(xml,type);
        return;
    }
    var postData = {};
    postData.content = xml;
    postData.targetHash = urlParams.get("targetHash");
    postData.targetPhash = urlParams.get("targetPhash");
    postData.resourceRoot = urlParams.get("resourceRoot");
    postData.moquiSessionToken = xmlCommonFuncs.getSession();
    var host = window.location.origin;
    var url = host +"/apps/tds/saveXmlFile";
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200){
            if(this.responseText=="true"){
                swal("تراکنش موفق!", "فایل با موفقیت ذخیره شد.", "success",{buttons:{
                        ok: "قبول، ادامه ویرایش",
                        okRedirect: {
                            text: "قبول، برگشت به فایل ها",
                            value: "files"
                        }
                    }}).then(function (value) {
                    switch (value) {
                        case "files":
                            xmlCommonFuncs.clearDraftXonomy(type);
                            window.location.replace(window.location.origin+"/apps/tds/elFinder?resourceRoot="+urlParams.get("resourceRoot")+"#elf_"+urlParams.get("targetPhash"));
                            break;
                        default:
                            break;
                    }
                });
            }
        }
    };
    xhttp.open("POST", url, false);
    xhttp.withCredentials = true;
    xhttp.setRequestHeader('Content-Type','application/json');
    xhttp.send(JSON.stringify(postData));
};
xmlCommonFuncs._getDownloadFileType = function (type) {
    if(type=='process'){
        return 'data:application/bpmn20-xml;charset=UTF-8,';
    }else if(type=='decision'){
        return '';
    }else{
        return 'data:application/xml;charset=utf-8,';
    }
};
xmlCommonFuncs._getDownloadFileName = function (type) {
    if(type=='process'){
        return 'export.bpmn';
    }else if(type=='decision'){
        return '';
    }else{
        return 'export.xml';
    }
};
xmlCommonFuncs.downloadFile = function (xml,type) {
    var element = document.createElement('a');
    element.setAttribute('href', xmlCommonFuncs._getDownloadFileType(type) + encodeURIComponent(xml));
    element.setAttribute('download', xmlCommonFuncs._getDownloadFileName(type));
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
};
xmlCommonFuncs.readFile = function (file,callback) {
    if (!file) {return;}
    var reader = new FileReader();
    reader.onload = function (e) {
        var contents = e.target.result;
        callback(contents);
    };
    reader.readAsText(file);
};
xmlCommonFuncs.getXonomyXml = function () {
    return '<?xml version="1.0" encoding="UTF-8"?><!-- Taraan TDS License -->\n'+ Xonomy.harvest();
};
xmlCommonFuncs.startSavingDraftXonomy = function (type){
    var oldObjStr =xmlCommonFuncs.getXonomyXml();
    setInterval(function () {
        var newObjStr =xmlCommonFuncs.getXonomyXml();
        if(oldObjStr.length != newObjStr.length) {
            localStorage.setItem("tds_draft_"+type,newObjStr);
            console.log("INFO: saved in draft");
            oldObjStr =newObjStr;
        }
    },10000);
};
xmlCommonFuncs.loadDraftXonomy= function (type,spec,editor) {
    if(localStorage.hasOwnProperty("tds_draft_"+type)&&localStorage.getItem("tds_draft_"+type)!=""){
        swal("بازیابی فایل!", "شما تغییرات ذخیره نشده دارید", "warning",{buttons:{
                dontRecovery: "عدم بازیابی و ادامه.",
                recovery : {
                    text: "بازیابی فایل ذخیره نشده.",
                    value: "recovery"
                }
            }}).then(function (value) {
            switch (value) {
                case "recovery":
                    xmlCommonFuncs.setEditorXonomy(localStorage.getItem("tds_draft_"+type),spec,editor);
                    break;
                default:
                    break;
            }
            xmlCommonFuncs.clearDraftXonomy(type);
            xmlCommonFuncs.startSavingDraftXonomy(type);
        });
    }else{
        setTimeout(function () {
            xmlCommonFuncs.startSavingDraftXonomy(type);
        },5000);
    }
};
xmlCommonFuncs.clearDraftXonomy = function (type) {
    localStorage.setItem("tds_draft_"+type,"");
};
xmlCommonFuncs.setEditorXonomy = function (xml,spec,editor) {
    Xonomy.setMode('nerd');
    Xonomy.lang = "en";
    Xonomy.render(xml, editor, spec);
};
