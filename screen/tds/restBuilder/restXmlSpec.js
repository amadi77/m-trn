var restXmlInitiate = "";

// Create elements object
var restElements = {
    "resource":xmlTemplates.element.resource,
    "id":xmlTemplates.element.id,
    "method":xmlTemplates.element.method,
    "service":xmlTemplates.element.service,
    "entity":xmlTemplates.element.entityRest,
    "description":xmlTemplates.element.description,

};
var restXmlSpec = {
    onchange: function () {
        //console.log("Ah been chaaanged!");
    },
    validate: function (jsElement) {
        if (typeof(jsElement) == "string") jsElement = Xonomy.xml2js(jsElement);
        var valid = true;
        var elementSpec = this.elements[jsElement.name];
        if (elementSpec.validate) {
            elementSpec.validate(jsElement); //validate the element
        }
        for (var iAttribute = 0; iAttribute < jsElement.attributes.length; iAttribute++) {
            var jsAttribute = jsElement.attributes[iAttribute];
            var attributeSpec = elementSpec.attributes[jsAttribute.name];
            if (attributeSpec.validate) {
                if (!attributeSpec.validate(jsAttribute)) valid = false; //validate the attribute
            }
        }
        for (var iChild = 0; iChild < jsElement.children.length; iChild++) {
            if (jsElement.children[iChild].type == "element") {
                var jsChild = jsElement.children[iChild];
                if (!this.validate(jsChild)) valid = false; //recurse to the child element
            }
        }
        return valid;
    },
    elements: restElements
};
