var screenXmlInitiate = "";

// Create elements object
var screenElements = {
    "widgets": xmlTemplates.element.widget,
    "container": xmlTemplates.element.container,
    "label": xmlTemplates.element.label,
    "link": xmlTemplates.element.link,
    "submit": xmlTemplates.element.submit,
    "reset": xmlTemplates.element.reset,
    "container-box": xmlTemplates.element.containerBox,
    "box-header": xmlTemplates.element.boxHeader,
    "box-toolbar": xmlTemplates.element.boxToolbar,
    "box-body": xmlTemplates.element.boxBody,
    "row-col": xmlTemplates.element.rowCol,
    "form-list": xmlTemplates.element.formList,
    "field": xmlTemplates.element.field,
    "default-field": xmlTemplates.element.defaultField,
    "display": xmlTemplates.element.display,
    "header-field": xmlTemplates.element.headerField,
    "container-row": xmlTemplates.element.containerRow,
    "box-body-nopad": xmlTemplates.element.boxBodyNopad,
    "section": xmlTemplates.element.section,
    "container-dialog": xmlTemplates.element.containerDialog,
    "section-iterate": xmlTemplates.element.sectionIterate,
    "fail-widgets": xmlTemplates.element.failWidgets,
    "transition": xmlTemplates.element.transition,
    "default-response": xmlTemplates.element.defaultResponse,
    "screen": xmlTemplates.element.screen,
    "text-line": xmlTemplates.element.textLine,
    "form-single": xmlTemplates.element.formSingle,
    "field-layout": xmlTemplates.element.fieldLayout,
    "field-ref": xmlTemplates.element.fieldRef,
    "drop-down": xmlTemplates.element.dropDown,
    "list-options": xmlTemplates.element.listOptions,
    "radio": xmlTemplates.element.radio,
    "option": xmlTemplates.element.option,
    "check": xmlTemplates.element.check,
    "text-area": xmlTemplates.element.textArea,
    "subscreens": xmlTemplates.element.subscreens,
    "subscreens-active": xmlTemplates.element.subscreensActive,
    "hidden": xmlTemplates.element.hidden,
    "form-list-column": xmlTemplates.element.formListColumn,
    "display-entity": xmlTemplates.element.displayEntity,
    "section-include": xmlTemplates.element.sectionInclude,
    "field-row-big": xmlTemplates.element.fieldRowBig,
    "conditional-field": xmlTemplates.element.conditionalField,
    "transition-include": xmlTemplates.element.transitionInclude,
    "parameter": xmlTemplates.element.parameter,
    "subscreens-item": xmlTemplates.element.subscreensItem,
    "macro-template": xmlTemplates.element.macroTemplate,
    "web-settings": xmlTemplates.element.webSettings,
    "always-actions": xmlTemplates.element.alwaysActions,
    "pre-actions": xmlTemplates.element.preActions,
    "path-parameter": xmlTemplates.element.pathParameter,
    "conditional-response": xmlTemplates.element.conditionalResponse,
    "error-response": xmlTemplates.element.errorResponse,
    "conditional-default": xmlTemplates.element.conditionalDefault,
    "subscreens-menu": xmlTemplates.element.subscreensMenu,
    "subscreens-panel": xmlTemplates.element.subscreensPanel,
    "container-panel": xmlTemplates.element.containerPanel,
    "panel-header": xmlTemplates.element.panelHeader,
    "panel-left": xmlTemplates.element.panelLeft,
    "panel-center": xmlTemplates.element.panelCenter,
    "panel-right": xmlTemplates.element.panelRight,
    "panel-footer": xmlTemplates.element.panelFooter,
    "dynamic-container": xmlTemplates.element.dynamicContainer,
    "dynamic-dialog": xmlTemplates.element.dynamicDialog,
    "include-screen": xmlTemplates.element.includeScreen,
    "tree": xmlTemplates.element.tree,
    "tree-node": xmlTemplates.element.treeNode,
    "tree-sub-node": xmlTemplates.element.treeSubNode,
    "render-mode": xmlTemplates.element.renderMode,
    "text": xmlTemplates.element.text,
    "widget-templates": xmlTemplates.element.widgetTemplates,
    "widget-template": xmlTemplates.element.widgetTemplate,
    "row-actions": xmlTemplates.element.rowActions,
    "auto-fields-service": xmlTemplates.element.autoFieldsService,
    "auto-fields-entity": xmlTemplates.element.autoFieldsEntity,
    "exclude": xmlTemplates.element.exclude,
    "field-accordion": xmlTemplates.element.fieldAccordion,
    "field-group": xmlTemplates.element.fieldGroup,
    "field-row": xmlTemplates.element.fieldRow,
    "fields-not-referenced": xmlTemplates.element.fieldsNotReferenced,
    "image": xmlTemplates.element.image,
    "editable": xmlTemplates.element.editable,
    "editable-load": xmlTemplates.element.editableLoad,
    "first-row-field": xmlTemplates.element.firstRowField,
    "second-row-field": xmlTemplates.element.secondRowField,
    "last-row-field": xmlTemplates.element.lastRowField,
    "auto-widget-service": xmlTemplates.element.autoWidgetService,
    "auto-widget-entity": xmlTemplates.element.autoWidgetEntity,
    "widget-template-include": xmlTemplates.element.widgetTemplateInclude,
    "date-find": xmlTemplates.element.dateFind,
    "date-period": xmlTemplates.element.datePeriod,
    "date-time": xmlTemplates.element.dateTime,
    "file": xmlTemplates.element.file,
    "ignored": xmlTemplates.element.ignored,
    "password": xmlTemplates.element.password,
    "range-find": xmlTemplates.element.rangeFind,
    "text-find": xmlTemplates.element.textFind,
    "entity-options": xmlTemplates.element.entityOptions,
    "dynamic-options": xmlTemplates.element.dynamicOptions,
    "depends-on": xmlTemplates.element.dependsOn,
    "json-form-single": xmlTemplates.element.jsonFormSingle,
    "hidden-parameters": xmlTemplates.element.hiddenParameters,
    "field-col-row": xmlTemplates.element.fieldColRow,
    "description":xmlTemplates.element.description,
    "emecas":xmlTemplates.element.emecas,
    "emeca":xmlTemplates.element.emeca,
    /////////////////// action elements//////////////////
    "actions": xmlTemplates.element.actions,
    "message":xmlTemplates.element.message,
    "service-call": xmlTemplates.element.serviceCall,
    "check-errors":xmlTemplates.element.checkErrors,
    "condition": xmlTemplates.element.condition,
    "set": xmlTemplates.element.set,
    "entity-find-one":xmlTemplates.element.entityFindOne,
    "entity-find":xmlTemplates.element.entityFind,
    "if": xmlTemplates.element.if,
    "else": xmlTemplates.element.else,
    "service-group": xmlTemplates.element.serviceGroup,
    "service-invoke": xmlTemplates.element.serviceInvoke,
    "script": xmlTemplates.element.script,
    "order-map-list": xmlTemplates.element.orderMapList,
    "filter-map-list": xmlTemplates.element.filterMapList,
    "date-filter": xmlTemplates.element.dateFilter,
    "entity-data":xmlTemplates.element.entityData,
    "default-parameters":xmlTemplates.element.defaultParameters,
    "search-from-inputs":xmlTemplates.element.searchFromInputs,
    "econditions":xmlTemplates.element.econditions,
    "having-econditions":xmlTemplates.element.havingEconditions,
    "econdition":xmlTemplates.element.econdition,
    "econdition-object":xmlTemplates.element.econditionObject,
    "select-field":xmlTemplates.element.selectField,
    "order-by":xmlTemplates.element.orderBy,
    "limit-range":xmlTemplates.element.limitRange,
    "limit-view":xmlTemplates.element.limitView,
    "use-iterator":xmlTemplates.element.useIterator,
    "field-map":xmlTemplates.element.fieldMap,
    "entity-find-count":xmlTemplates.element.entityFindCount,
    "entity-find-related-one":xmlTemplates.element.entityFindRelatedOne,
    "entity-find-related":xmlTemplates.element.entityFindRelated,
    "entity-make-value":xmlTemplates.element.entityMakeValue,
    "entity-create":xmlTemplates.element.entityCreate,
    "entity-update":xmlTemplates.element.entityUpdate,
    "entity-delete":xmlTemplates.element.entityDelete,
    "entity-delete-related":xmlTemplates.element.entityDeleteRelated,
    "entity-delete-by-condition":xmlTemplates.element.entityDeleteByCondition,
    "entity-set":xmlTemplates.element.entitySet,
    "entity-sequenced-id-primary":xmlTemplates.element.entitySequencedIdPrimary,
    "entity-sequenced-id-secondary":xmlTemplates.element.entitySequencedIdSecondary,
    "break":xmlTemplates.element.break,
    "continue":xmlTemplates.element.continue,
    "iterate":xmlTemplates.element.iterate,
    "return":xmlTemplates.element.return,
    "assert":xmlTemplates.element.assert,
    "while":xmlTemplates.element.while,
    "then":xmlTemplates.element.then,
    "else-if":xmlTemplates.element.elseIf,
    "or":xmlTemplates.element.or,
    "and":xmlTemplates.element.and,
    "not":xmlTemplates.element.not,
    "compare":xmlTemplates.element.compare,
    "expression":xmlTemplates.element.expression,
    "log":xmlTemplates.element.log,
    "report-view":xmlTemplates.element.reportView,
    "export-download":xmlTemplates.element.exportDownload,




};
var screenXmlSpec = {
    onchange: function () {
        //console.log("Ah been chaaanged!");
    },
    validate: function (jsElement) {
        if (typeof(jsElement) == "string") jsElement = Xonomy.xml2js(jsElement);
        var valid = true;
        var elementSpec = this.elements[jsElement.name];
        if (elementSpec.validate) {
            elementSpec.validate(jsElement); //validate the element
        }
        for (var iAttribute = 0; iAttribute < jsElement.attributes.length; iAttribute++) {
            var jsAttribute = jsElement.attributes[iAttribute];
            var attributeSpec = elementSpec.attributes[jsAttribute.name];
            if (attributeSpec.validate) {
                if (!attributeSpec.validate(jsAttribute)) valid = false; //validate the attribute
            }
        }
        for (var iChild = 0; iChild < jsElement.children.length; iChild++) {
            if (jsElement.children[iChild].type == "element") {
                var jsChild = jsElement.children[iChild];
                if (!this.validate(jsChild)) valid = false; //recurse to the child element
            }
        }
        return valid;
    },
    elements: screenElements
};
