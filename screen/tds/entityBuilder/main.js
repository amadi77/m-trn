(function () {
    var artifactType = "entity";


    function setSpecByFileType() {
        spec = entityXmlSpec;
        xml = entityXmlInitiate;
    }
    function initiateXml() { xml = xmlCommonFuncs.loadFile();
        xml = xmlCommonFuncs.loadFile();
        var initXmlElement= document.getElementById('xmlInit');
        if(xml.length < 5){
            xml = $(initXmlElement).text();
        }
    }

    var editor = document.getElementById("xmlEditor");
    var xml = "";
    var spec ="";

    setSpecByFileType();
    initiateXml();

    xmlCommonFuncs.setEditorXonomy(xml,spec,editor);


    document.getElementById('import').addEventListener('click', function () {
        document.getElementById('importFile').click();
    });
    document.getElementById('importFile').addEventListener('change', function () {
        xmlCommonFuncs.readFile(this.files[0],function (txt) {
            xmlCommonFuncs.setEditorXonomy(txt,spec,editor);
        });
    });
    document.getElementById('download').addEventListener('click', function () {
        xmlCommonFuncs.downloadFile(xmlCommonFuncs.getXonomyXml(),artifactType);xmlCommonFuncs.clearDraftXonomy(artifactType);
    });
    document.getElementById('save').addEventListener('click', function () {
        xmlCommonFuncs.saveFile(xmlCommonFuncs.getXonomyXml(),artifactType);xmlCommonFuncs.clearDraftXonomy(artifactType);
    });

    xmlCommonFuncs.loadDraftXonomy(artifactType,spec,editor);
}());
