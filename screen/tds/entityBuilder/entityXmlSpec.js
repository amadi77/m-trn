var entityXmlInitiate = "";

// Create elements object
var entityElements = {
    "entities":xmlTemplates.element.entities,
    "entity":xmlTemplates.element.entity,
    "extend-entity":xmlTemplates.element.extendEntity,
    "field":xmlTemplates.element.fieldEntity,
    "relationship":xmlTemplates.element.relationship,
    "key-map":xmlTemplates.element.keyMap,
    "index":xmlTemplates.element.index,
    "index-field":xmlTemplates.element.indexField,
    "seed-data":xmlTemplates.element.seedData,
    "master":xmlTemplates.element.master,
    "detail":xmlTemplates.element.detail,
    "view-entity":xmlTemplates.element.viewEntity,
    "member-entity":xmlTemplates.element.memberEntity,
    "member-relationship":xmlTemplates.element.memberRelationship,
    "alias-all":xmlTemplates.element.aliasAll,
    "alias":xmlTemplates.element.alias,
    "case":xmlTemplates.element.case,
    "complex-alias":xmlTemplates.element.complexAlias,
    "complex-alias-field":xmlTemplates.element.complexAliasField,
    "entity-condition":xmlTemplates.element.entityCondition,
    "date-filter":xmlTemplates.element.dateFilterEntity,
    "econdition":xmlTemplates.element.econditionEntity,
    "econditions":xmlTemplates.element.econditionsEntity,
    "having-econditions":xmlTemplates.element.havingEconditionsEntity,
    "order-by":xmlTemplates.element.orderByEntity,
    "exclude":xmlTemplates.element.excludeEntitySpec,
    "eecas":xmlTemplates.element.eecas,
    "eeca":xmlTemplates.element.eeca,
    "description":xmlTemplates.element.description,

};
var entityXmlSpec = {
    onchange: function () {
        //console.log("Ah been chaaanged!");
    },
    validate: function (jsElement) {
        if (typeof(jsElement) == "string") jsElement = Xonomy.xml2js(jsElement);
        var valid = true;
        var elementSpec = this.elements[jsElement.name];
        if (elementSpec.validate) {
            elementSpec.validate(jsElement); //validate the element
        }
        for (var iAttribute = 0; iAttribute < jsElement.attributes.length; iAttribute++) {
            var jsAttribute = jsElement.attributes[iAttribute];
            var attributeSpec = elementSpec.attributes[jsAttribute.name];
            if (attributeSpec.validate) {
                if (!attributeSpec.validate(jsAttribute)) valid = false; //validate the attribute
            }
        }
        for (var iChild = 0; iChild < jsElement.children.length; iChild++) {
            if (jsElement.children[iChild].type == "element") {
                var jsChild = jsElement.children[iChild];
                if (!this.validate(jsChild)) valid = false; //recurse to the child element
            }
        }
        return valid;
    },
    elements: entityElements
};
