var BaseComponent = Formio.Components.components.textfield;

function JalaaliComponent(component, options, data) {
    BaseComponent.prototype.constructor.call(this, component, options, data);
}

// Perform typical ES5 inheritance
JalaaliComponent.prototype = Object.create(BaseComponent.prototype);
JalaaliComponent.prototype.constructor = JalaaliComponent;

JalaaliComponent.schema = function() {
    return BaseComponent.schema({
        type: 'jalaali',
        format:'YYYY-MM-DD',
        defaultValue:'now',
        saveformat:'YYYY-MM-DD',
    });
};

JalaaliComponent.builderInfo = {
    title: 'jalaali',
    group: 'customIR',
    icon: 'fa fa-calendar-plus-o',
    weight: 70,
    documentation: 'http://ham-sun.com',
    schema: JalaaliComponent.schema()
};
JalaaliComponent.prototype.setValueAt = function(index, value) {
    BaseComponent.prototype.setValueAt.call(this,index,value);
    if(value){
        $(this.element).children('input.trnDatepicker').val(new persianDate(new Date(value)).format(this.component.format));
    }

};
JalaaliComponent.prototype.getFormatedDate = function(unix,saveFormat) {
    let res = unix;
    res = new Date(unix).toISOString();
    if(saveFormat=='unix') {
        res = unix
    }
    return res;
};

JalaaliComponent.prototype.createInput = function(container) {
    let defValue = false;
    if(this.component.defaultValue==undefined || this.component.defaultValue==''){
        this.component.defaultValue=false;
    }
    if(this.component.defaultValue=='now'){
        this.component.defaultValue=new Date().toISOString();
    }
    if(!!this.component.defaultValue){
        defValue = this.component.defaultValue;
    }
    if(!!this.value){
        defValue = this.value;
    }
    const input = BaseComponent.prototype.createInput.call(this,container);
    input.setAttribute("readonly","readonly");
    if(!!defValue)
        input.setAttribute("value",defValue);
    const trnDatepicker = this.ce('input',{class:'form-control trnDatepicker'});
    if(this.component.disabled){
        input.setAttribute("disabled","disabled");
        trnDatepicker.setAttribute("disabled","disabled");
        trnDatepicker.classList.add("disableJalaliDate");
    }else{
        trnDatepicker.classList.remove("disableJalaliDate");
    }
    if(this.component.validate.required){
        input.setAttribute("required","required");
        trnDatepicker.setAttribute("required","required");
    }
    trnDatepicker.setAttribute("readonly","readonly");
    if(!!defValue)
        trnDatepicker.setAttribute("value",defValue);
    container.appendChild(trnDatepicker);
    if(!this.component.format){
        this.component.format = 'YYYY-MM-DD';
    }
    $(trnDatepicker).persianDatepicker({
        format: this.component.format,
        altFieldFormatter: function (unix) {
            this.model.input.elem.parentElement.component.setValue(JalaaliComponent.prototype.getFormatedDate(unix,this.model.input.elem.parentElement.component.saveFormat));
            return unix;
        },
        autoClose: true,
        initialValue : !!defValue,
        altField: "#" +this.id +" > input:first-child",
        toolbox: {
            calendarSwitch: {enabled: true},
            submitButton: {enabled: true}
        },
        timePicker: {enabled: this.component.format.toLowerCase().indexOf('hh:mm') != -1}
    });
    return input;
};

// Use the table component edit form.
JalaaliComponent.editForm = Formio.Components.components.textfield.editForm;

// Register the component to the Formio.Components registry.
Formio.Components.addComponent('jalaali', JalaaliComponent);
