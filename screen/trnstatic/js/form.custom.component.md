in order to add custom component into form renderer you need to do these:
    1- open form-renderer project
    2- run npm install gulp on project directory
    3- run npm install on project directory
    5- go to lib/contrib directory
    6- add your custom component like the other components in that directory
    7- run gulp build
    8-copy the formio.contrib.min.js in dist directory into m-tasklist/taskliststatic/js
