workshopDetails = function (basicToken,shikId) {
    if (shikId.length >= 15) {
        var result;
            var token = basicToken;
            var host = window.location.origin;
            var url = host + "/rest/s1/mcls/party/workshop/workshopDetailByShikId/" + shikId;
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    var res = JSON.parse(xhttp.responseText);
                    if (res.workshop) {
                        result = res.workshop;
                    } else {
                        result =''
                    }
                }
            };
            xhttp.open("GET", url, false);
            if (token) {
                xhttp.setRequestHeader('Authorization', token);
            } else {
                xhttp.withCredentials = true;
            }
            xhttp.send();
        }
        else {
            result = '';
        }
        return result;
}
String.prototype.workshopDetails = function (basicToken,shikId) {
    return workshopDetails(this)
}
