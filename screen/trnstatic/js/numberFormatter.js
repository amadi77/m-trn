var delimiter = " و ", zero = "صفر",
    letters = [["", "یک", "دو", "سه", "چهار", "پنج", "شش", "هفت", "هشت", "نه"], ["ده", "یازده", "دوازده", "سیزده", "چهارده", "پانزده", "شانزده", "هفده", "هجده", "نوزده", "بیست"], ["", "", "بیست", "سی", "چهل", "پنجاه", "شصت", "هفتاد", "هشتاد", "نود"], ["", "یکصد", "دویست", "سیصد", "چهارصد", "پانصد", "ششصد", "هفتصد", "هشتصد", "نهصد"], ["", " هزار", " میلیون", " میلیارد", " بیلیون", " بیلیارد", " تریلیون", " تریلیارد", "کوآدریلیون", " کادریلیارد", " کوینتیلیون", " کوانتینیارد", " سکستیلیون", " سکستیلیارد", " سپتیلیون", "سپتیلیارد", " اکتیلیون", " اکتیلیارد", " نانیلیون", " نانیلیارد", " دسیلیون", " دسیلیارد"]],
    decimalSuffixes = ["", "دهم", "صدم", "هزارم", "ده‌هزارم", "صد‌هزارم", "میلیونوم", "ده‌میلیونوم", "صدمیلیونوم", "میلیاردم", "ده‌میلیاردم", "صد‌‌میلیاردم"],
    prepareNumber = function (e) {
        var t = e;
        "number" == typeof t && (t = t.toString());
        var r = t.length % 3;
        return 1 === r ? t = "00".concat(t) : 2 === r && (t = "0".concat(t)), t.replace(/\d{3}(?=\d)/g, "$&*").split("*")
    }, threeNumbersToLetter = function (e) {
        if (0 === parseInt(e, 0)) return "";
        var t = parseInt(e, 0);
        if (t < 10) return letters[0][t];
        if (t <= 20) return letters[1][t - 10];
        if (t < 100) {
            var r = t % 10, n = (t - r) / 10;
            return r > 0 ? letters[2][n] + delimiter + letters[0][r] : letters[2][n]
        }
        var i = t % 10, s = (t - t % 100) / 100, u = (t - (100 * s + i)) / 10, a = [letters[3][s]], l = 10 * u + i;
        return l > 0 && (l < 10 ? a.push(letters[0][l]) : l <= 20 ? a.push(letters[1][l - 10]) : (a.push(letters[2][u]), i > 0 && a.push(letters[0][i]))), a.join(delimiter)
    }, convertDecimalPart = function (e) {
        return "" === (e = e.replace(/0*$/, "")) ? "" : (e.length > 11 && (e = e.substr(0, 11)), " ممیز " + Num2persian(e) + " " + decimalSuffixes[e.length])
    }, Num2persian = function (e) {
        e = e.replace(/[^0-9.]/g, "");
        if (isNaN(parseFloat(e))) return zero;
        var t = "", r = e, n = e.indexOf(".");
        if (n > -1 && (r = e.substring(0, n), t = e.substring(n + 1, e.length)), r.length > 66) return "خارج از محدوده";
        for (var i = prepareNumber(r), s = [], u = i.length, a = 0; a < u; a += 1) {
            var l = letters[4][u - (a + 1)], o = threeNumbersToLetter(i[a]);
            "" !== o && s.push(o + l)
        }
        return t.length > 0 && (t = convertDecimalPart(t)), s.join(delimiter) + t
    };
String.prototype.toPersianLetter = function () {
    return Num2persian(this)
}, Number.prototype.toPersianLetter = function () {
    return Num2persian(parseFloat(this).toString())
};