checkNationalCode = function (meli_code) {
    if (meli_code.length == 10) {
        if (meli_code == '1111111111' || meli_code == '2222222222' || meli_code == '3333333333' || meli_code == '4444444444' || meli_code == '5555555555' || meli_code == '6666666666' || meli_code == '7777777777' || meli_code == '8888888888' || meli_code == '9999999999') {
            return false;
        } else {
            c = parseInt(meli_code.charAt(9));
            n = parseInt(meli_code.charAt(0)) * 10 + parseInt(meli_code.charAt(1)) * 9 + parseInt(meli_code.charAt(2)) * 8 + parseInt(meli_code.charAt(3)) * 7 + parseInt(meli_code.charAt(4)) * 6 + parseInt(meli_code.charAt(5)) * 5 + parseInt(meli_code.charAt(6)) * 4 + parseInt(meli_code.charAt(7)) * 3 + parseInt(meli_code.charAt(8)) * 2;
            r = n - parseInt(n / 11) * 11;
            if ((r == 0 && r == c) || (r == 1 && c == 1) || (r > 1 && c == 11 - r)) {
                return true;
            }
            else {
                return false;
            }
        }
    }
    else {
        return false
    }
}

validatePostalCode = function (postalCode) {
    if (postalCode == null)
        return false;
    if (postalCode.trim().length != 10)
        return false;
    var code = [];

    for (var i = 0; i < 10; i++)
        code[i] = Number.parseInt(postalCode.substring(i, i + 1));
    var re = 0;
    for (var i = 0; i < 10; i++)
        re = re * Number.parseInt(String.valueOf(code[i]));
    var flag = false;
    for (var i = 1; i < 10; i++)
        if (code[i - 1] != code[i]) flag = true;
    if (!flag)
        return false;
    for (var i = 0; i < 10; i++)
        if (code[i] == 0 || code[i] == 2)
            return false
    if ((Number.parseInt(postalCode)) < 1000000000)
        return false
    return true
}

validateNationalId = function (nationalId) {
    if (nationalId == null)
        return false;
    if (nationalId.trim().length != 11)
        return false;
    if (Number.parseInt(nationalId.substring(3, 9)) == 0)
        return false;
    e = Number.parseInt(nationalId.substring(10, 11));
    f = Number.parseInt(nationalId.substring(9, 10)) + 2;
    var z = [29, 27, 23, 19, 17];
    var s = 0;
    for (var i = 0; i < 10; i++)
        s += (f + Number.parseInt(nationalId.substring(i, i + 1))) * z[i % 5];
    s = s % 11;
    if (s == 10)
        s = 0;
    if (e != s)
        return false;
    return true;
}
validateForeignersId = function (foreignersIdentity) {
    if (foreignersIdentity == null)
        return false;
    if (foreignersIdentity.trim().length < 8)
        return false;
    if (foreignersIdentity.trim().length > 12)
        return false;
    var curVal
    var total = 0
    var everyOther = 1
    var checkDigit = 0
    var togen = foreignersIdentity.substring(0, foreignersIdentity.trim().length - 1);
    var r = togen.split('');
    var chars = r.reverse();
    for (var aChar in chars)
    {
        curVal = Number.parseInt(String.valueOf(aChar));
        if (everyOther == 1)
        {
            everyOther = 0;
            curVal = curVal * 2;
            if (curVal > 9)
                curVal = curVal - 9;
        }
        else
            everyOther = 1;
        total = total + curVal;
    }
    if (total % 10 == 0)
        checkDigit = 0;
    else
        checkDigit = 10 - total % 10;
    var s = foreignersIdentity.substring(foreignersIdentity.trim().length - 1);
    if (checkDigit != Number.parseInt(s))
        return false;
    return true;
}


String.prototype.checkPublicAlgorithm = function () {
    return checkNationalCode(this)
}
String.prototype.checkPublicAlgorithm = function () {
    return validatePostalCode(this)
}
String.prototype.checkPublicAlgorithm = function () {
    return validateNationalId(this)
}
String.prototype.checkPublicAlgorithm = function () {
    return validateForeignersId(this)
}