getEnums = function (basicToken,enumTypeId,parentEnumId,orderByField) {
    var token = basicToken;
    var pageSize = 200;
    var values=[];
    var host = window.location.origin;
    var url = host + "/rest/s1/moqui/basic/enums";
    if (pageSize)
        url += "?pageSize=" + pageSize;
    if (parentEnumId)
        url += "&parentEnumId=" + parentEnumId;
    if (enumTypeId)
        url += "&enumTypeId=" + enumTypeId;
    if (orderByField)
        url += "&orderByField=" + orderByField;
    var
        xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) values = JSON.parse(xhttp.responseText);
    };
    xhttp.open("GET", url, false);
    if (token) {
        xhttp.setRequestHeader('Authorization', token);
    }
    else {
        xhttp.withCredentials = true;
    }
    xhttp.send();
    return values;
}
String.prototype.getEnums = function (basicToken,enumTypeId,parentEnumId,orderByField) {
    return getEnums(this)
}
