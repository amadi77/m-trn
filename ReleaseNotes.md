# Change Log
All notable changes to this component will be documented in this file
## UnRelease

### Fixed
 - Fix double cell type and change to string in ReadExcel
 - Fix style for huge datagrid
 
### Changed
 - Remove importProvinceParty webService and move to mantle-usl component
 - Remove geoService.xml
 - Change geoNameLocal of geoIran.xml 